package com.android.fanqfang.dldregist.widget;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;

import com.android.fanqfang.dldregist.R;

/**
 * Created by fanqfang on 9/8/2017 AD.
 */

public class MyEditNoHLTextView extends android.support.v7.widget.AppCompatEditText{
    public MyEditNoHLTextView(Context context) {
        super(context);
        init();
    }

    public MyEditNoHLTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public MyEditNoHLTextView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    public void init() {
//        setTextSize(getResources().getDimension(R.dimen.font_edit_text));
        Typeface tf = Typeface.createFromAsset(getContext().getAssets(), getResources().getString(R.string.font_en_light));
        setTypeface(tf, 1);
    }
}
