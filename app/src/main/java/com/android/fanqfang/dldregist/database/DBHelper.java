package com.android.fanqfang.dldregist.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import com.android.fanqfang.dldregist.model.AmphurMaster;
import com.android.fanqfang.dldregist.model.AmphurMaster.AmphurEntity;
import com.android.fanqfang.dldregist.model.FarmModel;
import com.android.fanqfang.dldregist.model.FarmModel.FarmEntity;
import com.android.fanqfang.dldregist.model.FarmOwnerMaster;
import com.android.fanqfang.dldregist.model.FarmOwnerMaster.OwnerEntity;
import com.android.fanqfang.dldregist.model.FarmProblemMaster;
import com.android.fanqfang.dldregist.model.FarmProblemMaster.FarmProblemEntity;
import com.android.fanqfang.dldregist.model.FarmerModel;
import com.android.fanqfang.dldregist.model.FarmerModel.FarmerEntity;
import com.android.fanqfang.dldregist.model.FarmerProblemMaster;
import com.android.fanqfang.dldregist.model.FarmerProblemMaster.FarmerProblemEntity;
import com.android.fanqfang.dldregist.model.FarmerTypeMaster;
import com.android.fanqfang.dldregist.model.FarmerTypeMaster.FarmerTypeEntity;
import com.android.fanqfang.dldregist.model.FeedKindMaster;
import com.android.fanqfang.dldregist.model.FeedKindMaster.FeedKindEntity;
import com.android.fanqfang.dldregist.model.FeedModel;
import com.android.fanqfang.dldregist.model.FeedModel.FeedEntity;
import com.android.fanqfang.dldregist.model.HelpMaster;
import com.android.fanqfang.dldregist.model.HelpMaster.HelpEntity;
import com.android.fanqfang.dldregist.model.HoldingLandMaster;
import com.android.fanqfang.dldregist.model.HoldingLandMaster.LandEntity;
import com.android.fanqfang.dldregist.model.LivestockMaster;
import com.android.fanqfang.dldregist.model.LivestockModel;
import com.android.fanqfang.dldregist.model.MainJobMaster;
import com.android.fanqfang.dldregist.model.MainJobMaster.JobEntity;
import com.android.fanqfang.dldregist.model.PrefixMaster;
import com.android.fanqfang.dldregist.model.PrefixMaster.PrefixEntity;
import com.android.fanqfang.dldregist.model.ProvinceMaster;
import com.android.fanqfang.dldregist.model.ProvinceMaster.ProvinceEntity;
import com.android.fanqfang.dldregist.model.SecondJobMaster;
import com.android.fanqfang.dldregist.model.SecondJobMaster.SecondJobEntity;
import com.android.fanqfang.dldregist.model.StandardMaster;
import com.android.fanqfang.dldregist.model.StandardMaster.StandardEntity;
import com.android.fanqfang.dldregist.model.SupportStandardMaster;
import com.android.fanqfang.dldregist.model.SupportStandardMaster.SupportEntity;
import com.android.fanqfang.dldregist.model.TambolMaster;
import com.android.fanqfang.dldregist.model.TambolMaster.TambolEntity;
import com.android.fanqfang.dldregist.model.VillageMaster;
import com.android.fanqfang.dldregist.model.VillageMaster.VillageEntity;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by fanqfang on 9/26/2017 AD.
 */

public class DBHelper extends SQLiteOpenHelper {

    private final String TAG = getClass().getSimpleName();

    private SQLiteDatabase sqLiteDatabase;

    private static final String CREATE_FARMER_TABLE = String.format("CREATE TABLE %s" +
                    "(%s INTEGER PRIMARY KEY AUTOINCREMENT, %s INTEGER, %s INTEGER, %s TEXT, %s TEXT, %s TEXT, %s TEXT, %s INTEGER, %s TEXT, %s TEXT, %s TEXT," +
                    "%s INTEGER, %s TEXT, %s TEXT, %s INTEGER, %s TEXT, %s TEXT, %s TEXT, %s INTEGER, %s INTEGER, %s INTEGER, %s INTEGER, %s TEXT, %s INTEGER," +
                    "%s INTEGER, %s INTEGER, %s INTEGER, %s TEXT, %s TEXT, %s TEXT, %s INTEGER, %s INTEGER, %s TEXT, %s TEXT, %s INTEGER, %s TEXT, %s INTEGER, %s INTEGER," +
                    "%s TEXT, %s TEXT, %s INTEGER, %s TEXT, %s TEXT, %s INTEGER, %s TEXT, %s TEXT, %s TEXT, %s TEXT, %s INTEGER) ",

            FarmerModel.TABLE,
            FarmerModel.Column.ID,
            FarmerModel.Column.FARMER_ID, //1
            FarmerModel.Column.AMPHUR_ID,
            FarmerModel.Column.BIRTHDAY,
            FarmerModel.Column.COMPANY_TYPE_ID,
            FarmerModel.Column.CREATE_BY,
            FarmerModel.Column.CREATE_DATE,
            FarmerModel.Column.DEBTAMOUNT,
            FarmerModel.Column.DEBTSAMOUNT_IN,
            FarmerModel.Column.DEBTSAMOUNT_OUT,
            FarmerModel.Column.EMAIL, //10
            FarmerModel.Column.FARMER_GROUP_ID,
            FarmerModel.Column.FARMER_PROBLEM_ID,
            FarmerModel.Column.FARMER_PROBLEM_OTHER,
            FarmerModel.Column.FARMER_STATUS_ID,
            FarmerModel.Column.FARMER_TYPE_ID,
            FarmerModel.Column.FIRST_NAME,
            FarmerModel.Column.GENDER_ID,
            FarmerModel.Column.GOVERNMENT_HELP_ID,
            FarmerModel.Column.HOLDING_EQUIPMENT,
            FarmerModel.Column.HOLDING_LAND_ID, //20
            FarmerModel.Column.HOME_ID,
            FarmerModel.Column.HOME_NO,
            FarmerModel.Column.IMAGE_ID,
            FarmerModel.Column.IS_AGENCY,
            FarmerModel.Column.IS_Cancel,
            FarmerModel.Column.IS_DEAD,
            FarmerModel.Column.LASTNAME,
            FarmerModel.Column.LATITUDE,
            FarmerModel.Column.LONGITUDE,
            FarmerModel.Column.MAIN_JOB_ID, //30
            FarmerModel.Column.MARITAL_STATUS_ID,
            FarmerModel.Column.MOBILE,
            FarmerModel.Column.PHONE,
            FarmerModel.Column.PID,
            FarmerModel.Column.POSTCODE,
            FarmerModel.Column.PREFIX_ID,
            FarmerModel.Column.PROVINCE_ID,
            FarmerModel.Column.REVENUE,
            FarmerModel.Column.ROAD,
            FarmerModel.Column.SECOND_JOB_ID, //40
            FarmerModel.Column.SOI,
            FarmerModel.Column.STATUS_ID,
            FarmerModel.Column.TAMBOL_ID,
            FarmerModel.Column.TAX_ID,
            FarmerModel.Column.UPDATE_BY,
            FarmerModel.Column.UPDATE_DATE,
            FarmerModel.Column.VILLAGE,
            FarmerModel.Column.VILLAGE_ID);

    private static final String CREATE_FARM_TABLE = String.format("CREATE TABLE %s" +
                    "(%s INTEGER PRIMARY KEY AUTOINCREMENT,%s INTEGER, %s INTEGER, %s INTEGER, %s TEXT, %s TEXT, %s TEXT, %s TEXT, %s TEXT, %s TEXT, %s TEXT, %s TEXT, %s TEXT, %s TEXT, %s TEXT, %s TEXT," +
                    "%s INTEGER, %s TEXT, %s INTEGER, %s TEXT, %s TEXT, %s TEXT, %s TEXT, %s TEXT, %s TEXT ,%s INTEGER, %s INTEGER, %s TEXT, %s TEXT, %s INTEGER, %s INTEGER, %s INTEGER, %s INTEGER, %s TEXT," +
                    "%s TEXT, %s TEXT, %s INTEGER, %s TEXT)",
            FarmModel.TABLE,
            FarmModel.Column.ID,
            FarmModel.Column.FARM_ID, //1
            FarmModel.Column.AMPHUR_ID,
            FarmModel.Column.ANIMAL_WORTH,
            FarmModel.Column.AREA_RAI,
            FarmModel.Column.AREA_NGAN,
            FarmModel.Column.AREA_TYPE_ID,
            FarmModel.Column.CANCEL_BY,
            FarmModel.Column.CANCEL_DATE,
            FarmModel.Column.CREATE_BY,
            FarmModel.Column.CREATE_DATE, //10
            FarmModel.Column.EMAIL,
            FarmModel.Column.FARM_NAME,
            FarmModel.Column.FARM_OWNER_TYPE_ID,
            FarmModel.Column.FARM_PROBLEM_DESC,
            FarmModel.Column.FARM_STANDARD_NUMBER,
            FarmModel.Column.FARMER_ID,
            FarmModel.Column.HOME_NO,
            FarmModel.Column.IS_CANCEL,
            FarmModel.Column.LATITUDE,
            FarmModel.Column.LONGITUDE, //20
            FarmModel.Column.MOBILE,
            FarmModel.Column.ORDER_INDEX,
            FarmModel.Column.PHONE,
            FarmModel.Column.POSTCODE,
            FarmModel.Column.PROVINCE_ID,
            FarmModel.Column.REVENUE_OF_LIVESTOCK,
            FarmModel.Column.ROAD,
            FarmModel.Column.SOI,
            FarmModel.Column.STANDARD_ID,
            FarmModel.Column.STATUS_ID, //30
            FarmModel.Column.SUPPORT_STANDARD_ID,
            FarmModel.Column.TAMBOL_ID,
            FarmModel.Column.UPDATE_BY,
            FarmModel.Column.UPDATE_DATE,
            FarmModel.Column.VILLAGE,
            FarmModel.Column.VILLAGE_ID,
            FarmModel.Column.WORKER_ID);

    private static final String CREATE_PREFIX_TABLE = String.format("CREATE TABLE %s" +
                    "(%s INTEGER PRIMARY KEY AUTOINCREMENT, %s INTEGER, %s INTEGER, %s TEXT,%s TEXT)",
            PrefixMaster.TABLE,
            PrefixMaster.Column.ID,
            PrefixMaster.Column.PREFIX_ID,
            PrefixMaster.Column.ORDER_INDEX,
            PrefixMaster.Column.PREFIX_NAME_TH,
            PrefixMaster.Column.PREFIX_NAME_EN);

    private static final String CREATE_PROVINCE_TABLE = String.format("CREATE TABLE %s" +
                    "(%s INTEGER PRIMARY KEY AUTOINCREMENT, %s INTEGER, %s INTEGER, %s INTEGER, %s REAL, %s REAL, %s INTEGER, %s TEXT, %s TEXT, %s INTEGER, %s INTEGER)",
            ProvinceMaster.TABLE,
            ProvinceMaster.Column.ID,
            ProvinceMaster.Column.PROVINCE_ID,
            ProvinceMaster.Column.COUNTRYRANCH_ID,
            ProvinceMaster.Column.GEO_ID,
            ProvinceMaster.Column.LATITUDE,
            ProvinceMaster.Column.LONGITUDE,
            ProvinceMaster.Column.ORDER_INDEX,
            ProvinceMaster.Column.PROVINCE_NAME_EN,
            ProvinceMaster.Column.PROVINCE_NAME_TH,
            ProvinceMaster.Column.PROVINCE_CODE,
            ProvinceMaster.Column.STATUS_ID);

    private static final String CREATE_AMPHUR_TABLE = String.format("CREATE TABLE %s" +
                    "(%s INTEGER PRIMARY KEY AUTOINCREMENT, %s INTEGER, %s INTEGER, %s TEXT, %s TEXT, %s REAL, %s REAL, %s INTEGER, %s INTEGER, %s INTEGER)",
            AmphurMaster.TABLE,
            AmphurMaster.Column.ID,
            AmphurMaster.Column.AMPHUR_ID,
            AmphurMaster.Column.AMPHUR_CODE,
            AmphurMaster.Column.AMPHUR_NAME_EN,
            AmphurMaster.Column.AMPHUR_NAME_TH,
            AmphurMaster.Column.LATITUDE,
            AmphurMaster.Column.LONGITUDE,
            AmphurMaster.Column.ORDER_INDEX,
            AmphurMaster.Column.PROVINCE_ID,
            AmphurMaster.Column.STATUS_ID);

    private static final String CREATE_TAMBOL_TABLE = String.format("CREATE TABLE %s" +
                    "(%s INTEGER PRIMARY KEY AUTOINCREMENT, %s INTEGER,%s INTEGER, %s REAL, %s REAL, %s INTEGER, %s INTEGER, %s INTEGER, %s STRING, %s STRING)",
            TambolMaster.TABLE,
            TambolMaster.Column.ID,
            TambolMaster.Column.TAMBOL_ID,
            TambolMaster.Column.AMPHUR_ID,
            TambolMaster.Column.LATITUDE,
            TambolMaster.Column.LONGITUDE,
            TambolMaster.Column.ORDER_INDEX,
            TambolMaster.Column.STATUS_ID,
            TambolMaster.Column.TAMBOL_CODE,
            TambolMaster.Column.TAMBOL_NAME_EN,
            TambolMaster.Column.TAMBOL_NAME_TH);

    private static final String CREATE_VILLAGE_TABLE = String.format("CREATE TABLE %s" +
                    "(%s INTEGER PRIMARY KEY AUTOINCREMENT, %s INTEGER, %s INTEGER, %s INTEGER, %s INTEGER,%s INTEGER, %s INTEGER, %s TEXT)",
            VillageMaster.TABLE,
            VillageMaster.Column.ID,
            VillageMaster.Column.VILLAGE_ID,
            VillageMaster.Column.MOO,
            VillageMaster.Column.ORDER_INDEX,
            VillageMaster.Column.STATUS_ID,
            VillageMaster.Column.TAMBOL_ID,
            VillageMaster.Column.VILLAGE_CODE,
            VillageMaster.Column.VILLAGE_NAME);

    private static final String CREATE_FARMER_PROBLEM_TABLE = String.format("CREATE TABLE %s" +
                    "(%s INTEGER PRIMARY KEY AUTOINCREMENT,%s INTEGER, %s TEXT, %s TEXT, %s INTEGER, %s INTEGER)",
            FarmerProblemMaster.TABLE,
            FarmerProblemMaster.Column.ID,
            FarmerProblemMaster.Column.FARMER_PROBLEM_ID,
            FarmerProblemMaster.Column.FARMER_PROBLEM_DESC,
            FarmerProblemMaster.Column.FARMER_PROBLEM_NAME,
            FarmerProblemMaster.Column.ORDER_INDEX,
            FarmerProblemMaster.Column.STATUS_ID);

    private static final String CREATE_FARMER_TYPE_TABLE = String.format("CREATE TABLE %s" +
                    "(%s INTEGER PRIMARY KEY AUTOINCREMENT, %s INTEGER, %s TEXT, %s INTEGER, %s INTEGER)",
            FarmerTypeMaster.TABLE,
            FarmerTypeMaster.Column.ID,
            FarmerTypeMaster.Column.FARMER_TYPE_ID,
            FarmerTypeMaster.Column.FARMER_TYPE_NAME,
            FarmerTypeMaster.Column.ORDER_INDEX,
            FarmerTypeMaster.Column.STATUS_ID);

    private static final String CREATE_FARM_OWNER_TABLE = String.format("CREATE TABLE %s" +
                    "(%s INTEGER PRIMARY KEY AUTOINCREMENT, %s INTEGER, %s TEXT, %s INTEGER, %s INTEGER)",
            FarmOwnerMaster.TABLE,
            FarmOwnerMaster.Column.ID,
            FarmOwnerMaster.Column.FARM_OWNER_TYPE_ID,
            FarmOwnerMaster.Column.FARM_OWNER_TYPE_NAME,
            FarmOwnerMaster.Column.ORDER_INDEX,
            FarmOwnerMaster.Column.STATUS_ID);

    private static final String CREATE_FARM_PROBLEM_TABLE = String.format("CREATE TABLE %s" +
                    "(%s INTEGER PRIMARY KEY AUTOINCREMENT,%s INTEGER, %s TEXT, %s INTEGER, %s INTEGER)",
            FarmProblemMaster.TABLE,
            FarmProblemMaster.Column.ID,
            FarmProblemMaster.Column.FARM_PROBLEM_ID,
            FarmProblemMaster.Column.FARM_PROBLEM_NAME,
            FarmProblemMaster.Column.ORDER_INDEX,
            FarmProblemMaster.Column.STATUS_ID);

    private static final String CREATE_FEED_KIND_TABLE = String.format("CREATE TABLE %s" +
                    "(%s INTEGER PRIMARY KEY AUTOINCREMENT, %s INTEGER, %s TEXT, %s TEXT, %s INTEGER, %s INTEGER)",
            FeedKindMaster.TABLE,
            FeedKindMaster.Column.ID,
            FeedKindMaster.Column.FEED_KIND_ID,
            FeedKindMaster.Column.FEED_KIND_NAME_EN,
            FeedKindMaster.Column.FEED_KIND_NAME_TH,
            FeedKindMaster.Column.ORDER_INDEX,
            FeedKindMaster.Column.STATUS_ID);

    private static final String CREATE_FEED_TABLE = String.format("CREATE TABLE %s" +
                    "(%s INTEGER PRIMARY KEY AUTOINCREMENT, %s INTEGER, %s TEXT, %s TEXT, %s INTEGER, %s INTEGER, %s INTEGER, %s INTEGER, %s TEXT, %s TEXT, %s TEXT, %s TEXT, %s TEXT," +
                    "%s INTEGER, %s TEXT, %s TEXT)",
            FeedModel.TABLE,
            FeedModel.Column.ID,
            FeedModel.Column.FEED_ID,
            FeedModel.Column.CREATE_BY,
            FeedModel.Column.CREATE_DATE,
            FeedModel.Column.FARM_ID,
            FeedModel.Column.FEED_AREA_NGAN,
            FeedModel.Column.FEED_AREA_RAI,
            FeedModel.Column.FEED_KIND_ID,
            FeedModel.Column.FEED_KIND_OTHER,
            FeedModel.Column.FEED_TYPE_ID,
            FeedModel.Column.FEED_TYPE_OTHER,
            FeedModel.Column.FEED_WATER_TYPE_ID,
            FeedModel.Column.ORDER_INDEX,
            FeedModel.Column.STATUS_ID,
            FeedModel.Column.UPDATE_BY,
            FeedModel.Column.UPDATE_DATE);

    private static final String CREATE_HELP_TABLE = String.format("CREATE TABLE %s" +
                    "(%s INTEGER PRIMARY KEY AUTOINCREMENT, %s INTEGER, %s TEXT,%s INTEGER,%s INTEGER)",
            HelpMaster.TABLE,
            HelpMaster.Column.ID,
            HelpMaster.Column.GOVERNMENT_HELP_ID,
            HelpMaster.Column.GOVERNMENT_HELP_NAME,
            HelpMaster.Column.ORDER_INDEX,
            HelpMaster.Column.STATUS_ID);

    private static final String CREATE_HOLDING_LAND_TABLE = String.format("CREATE TABLE %s" +
                    "(%s INTEGER PRIMARY KEY AUTOINCREMENT, %s INTEGER, %s TEXT,%s INTEGER,%s INTEGER)",
            HoldingLandMaster.TABLE,
            HoldingLandMaster.Column.ID,
            HoldingLandMaster.Column.HOLDING_LAND_ID,
            HoldingLandMaster.Column.HOLDING_LAND_NAME,
            HoldingLandMaster.Column.ORDER_INDEX,
            HoldingLandMaster.Column.STATUS_ID);

    private static final String CREATE_LIST_LIVESTOCK_TABLE = String.format("CREATE TABLE %s" +
                    "(%s INTEGER PRIMARY KEY AUTOINCREMENT, %s INTEGER, %s TEXT, %s TEXT, %s TEXT, %s TEXT,  %s TEXT, %s TEXT, %s TEXT," +
                    "%s INTEGER, %s INTEGER, %s TEXT, %s INTEGER, %s TEXT, %s TEXT)",
            LivestockMaster.TABLE,
            LivestockMaster.Column.ID,
            LivestockMaster.Column.LIVESTOCK_TYPE_ID,
            LivestockMaster.Column.CREATE_BY,
            LivestockMaster.Column.CREATE_DATE,
            LivestockMaster.Column.LIVESTOCK_TYPE_CODE,
            LivestockMaster.Column.LIVESTOCK_TYPE_FLGTAIL,
            LivestockMaster.Column.LIVESTOCK_TYPE_FULLNAME,
            LivestockMaster.Column.LIVESTOCK_TYPE_NAME,
            LivestockMaster.Column.LIVESTOCK_TYPE_OLD_ID,
            LivestockMaster.Column.LIVESTOCK_TYPE_PARENT,
            LivestockMaster.Column.LIVESTOCK_TYPE_PRICE,
            LivestockMaster.Column.LIVESTOCK_TYPE_UNIT,
            LivestockMaster.Column.LIVESTOCK_TYPE_UNITLIMIT,
            LivestockMaster.Column.UPDATE_BY,
            LivestockMaster.Column.UPDATE_DATE);

    private static final String CREATE_MAIN_JOB_TABLE = String.format("CREATE TABLE %s" +
                    "(%s INTEGER PRIMARY KEY AUTOINCREMENT, %s INTEGER, %s TEXT,%s INTEGER,%s INTEGER)",
            MainJobMaster.TABLE,
            MainJobMaster.Column.ID,
            MainJobMaster.Column.MAIN_JOB_ID,
            MainJobMaster.Column.MAIN_JOB_NAME,
            MainJobMaster.Column.ORDER_INDEX,
            MainJobMaster.Column.STATUS_ID);

    private static final String CREATE_SECOND_JOB_TABLE = String.format("CREATE TABLE %s" +
                    "(%s INTEGER PRIMARY KEY AUTOINCREMENT, %s INTEGER, %s TEXT,%s INTEGER,%s INTEGER)",
            SecondJobMaster.TABLE,
            SecondJobMaster.Column.ID,
            SecondJobMaster.Column.SECOND_JOB_ID,
            SecondJobMaster.Column.SECOND_JOB_NAME,
            SecondJobMaster.Column.ORDER_INDEX,
            SecondJobMaster.Column.STATUS_ID);

    private static final String CREATE_LIVESTOCK_TABLE = String.format("CREATE TABLE %s" +
                    "(%s INTEGER PRIMARY KEY AUTOINCREMENT, %s INTEGER, %s TEXT, %s TEXT, %s INTEGER,%s INTEGER,%s INTEGER,%s INTEGER,%s TEXT, %s TEXT)",
            LivestockModel.TABLE,
            LivestockModel.Column.ID,
            LivestockModel.Column.LIVESTOCK_ID,
            LivestockModel.Column.CREATE_BY,
            LivestockModel.Column.CREATE_DATE,
            LivestockModel.Column.FARM_ID,
            LivestockModel.Column.LIVESTOCK_AMOUNT,
            LivestockModel.Column.LIVESTOCK_TYPE_ID,
            LivestockModel.Column.LIVESTOCK_VALUES,
            LivestockModel.Column.UPDATE_BY,
            LivestockModel.Column.UPDATE_DATE);

    private static final String CREATE_STANDARD_TABLE = String.format("CREATE TABLE %s" +
                    "(%s INTEGER PRIMARY KEY AUTOINCREMENT, %s INTEGER, %s TEXT,%s INTEGER,%s INTEGER)",
            StandardMaster.TABLE,
            StandardMaster.Column.ID,
            StandardMaster.Column.STANDARD_ID,
            StandardMaster.Column.STANDARD_NAME,
            StandardMaster.Column.ORDER_INDEX,
            StandardMaster.Column.STATUS_ID);

    private static final String CREATE_SUPPORT_STANDARD_TABLE = String.format("CREATE TABLE %s" +
                    "(%s INTEGER PRIMARY KEY AUTOINCREMENT,%s INTEGER,%s TEXT,%s INTEGER,%s INTEGER)",
            SupportStandardMaster.TABLE,
            SupportStandardMaster.Column.ID,
            SupportStandardMaster.Column.SUPPORT_STANDARD_ID,
            SupportStandardMaster.Column.SUPPORT_STANDARD_NAME,
            SupportStandardMaster.Column.ORDER_INDEX,
            SupportStandardMaster.Column.STATUS_ID);


    public DBHelper(Context context) {
        super(context, PrefixMaster.DATABASE_NAME, null, PrefixMaster.DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {

//        Log.i(TAG, CREATE_FARM_OWNER_TABLE);

        db.execSQL(CREATE_FARMER_TABLE);
        db.execSQL(CREATE_PREFIX_TABLE);
        db.execSQL(CREATE_PROVINCE_TABLE);
        db.execSQL(CREATE_AMPHUR_TABLE);
        db.execSQL(CREATE_TAMBOL_TABLE);
        db.execSQL(CREATE_VILLAGE_TABLE);
        db.execSQL(CREATE_FARMER_PROBLEM_TABLE);
        db.execSQL(CREATE_FARMER_TYPE_TABLE);
        db.execSQL(CREATE_FARM_TABLE);
        db.execSQL(CREATE_FARM_OWNER_TABLE);
        db.execSQL(CREATE_FARM_PROBLEM_TABLE);
        db.execSQL(CREATE_FEED_KIND_TABLE);
        db.execSQL(CREATE_FEED_TABLE);
        db.execSQL(CREATE_HELP_TABLE);
        db.execSQL(CREATE_HOLDING_LAND_TABLE);
        db.execSQL(CREATE_LIST_LIVESTOCK_TABLE);
        db.execSQL(CREATE_MAIN_JOB_TABLE);
        db.execSQL(CREATE_SECOND_JOB_TABLE);
        db.execSQL(CREATE_LIVESTOCK_TABLE);
        db.execSQL(CREATE_STANDARD_TABLE);
        db.execSQL(CREATE_SUPPORT_STANDARD_TABLE);

    }


    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }


    public boolean initialized() {
        try {
            SQLiteDatabase db = this.getReadableDatabase();
        } catch (SQLiteException e) {
            // no database created yet
            return false;
        }
        // TODO: check that version in db matches version in web API
        return true;
    }

    public boolean isTablePopulated(String tableName) {
        SQLiteDatabase db = this.getReadableDatabase();
        try {
            String selectQuery = "SELECT  * FROM " + tableName;
            Cursor cursor = db.rawQuery(selectQuery, null);
            if (cursor == null)
                return false;
            if (cursor.getCount() == 0)
                return false;
            return true;
        } finally {
            db.close();
        }
    }

    public ArrayList<PrefixEntity> getPrefixList() {
        ArrayList<PrefixEntity> prefixs = new ArrayList<>();

        SQLiteDatabase db = this.getReadableDatabase();

        Cursor cursor = db.query(PrefixMaster.TABLE, null, null, null, null, null, null);

        if (cursor != null) {
            cursor.moveToFirst();
        }

        while (!cursor.isAfterLast()) {
            prefixs.add(getPrefixInfo(cursor));
            cursor.moveToNext();
        }
        db.close();

        return prefixs;
    }

    public PrefixEntity getPrefixInfo(Cursor c) {
        PrefixEntity prefix = new PrefixEntity();

        String prefix_ID = c.getString(c.getColumnIndex(PrefixMaster.Column.PREFIX_ID));
        String prefix_NameTh = c.getString(c.getColumnIndex(PrefixMaster.Column.PREFIX_NAME_TH));

        prefix.setPrefix_ID(Integer.parseInt(prefix_ID));
        prefix.setPrefix_NameTh(prefix_NameTh);

        return prefix;
    }

    public FarmerEntity getInfoFarmer() {

        SQLiteDatabase db = this.getReadableDatabase();

        FarmerEntity farmer = new FarmerEntity();
        String selectQuery = "SELECT  * FROM " + FarmerModel.TABLE;

//        String selectQuery = "SELECT  * FROM " + FarmModel.TABLE + "WHERE" + FarmerModel.Column.PID + "=" + pid;

        Cursor cursor = db.rawQuery(selectQuery, null);

        if (cursor != null) {
            cursor.moveToFirst();
        }

        farmer.setFarmer_ID(cursor.getInt(cursor.getColumnIndex(FarmerModel.Column.FARMER_ID)));
        farmer.setAmphur_ID(cursor.getInt(cursor.getColumnIndex(FarmerModel.Column.AMPHUR_ID)));
        farmer.setBirthDay(cursor.getString(cursor.getColumnIndex(FarmerModel.Column.BIRTHDAY)));
        farmer.setCompany_Type_ID(cursor.getString(cursor.getColumnIndex(FarmerModel.Column.COMPANY_TYPE_ID)));
        farmer.setCreateBy(cursor.getString(cursor.getColumnIndex(FarmerModel.Column.CREATE_BY)));
        farmer.setCreateDate(cursor.getString(cursor.getColumnIndex(FarmerModel.Column.CREATE_DATE)));
        farmer.setDebtAmount(cursor.getInt(cursor.getColumnIndex(FarmerModel.Column.DEBTAMOUNT)));
        farmer.setDebtsAmountIn(cursor.getString(cursor.getColumnIndex(FarmerModel.Column.DEBTSAMOUNT_IN)));
        farmer.setDebtsAmountOut(cursor.getString(cursor.getColumnIndex(FarmerModel.Column.DEBTSAMOUNT_OUT)));
        farmer.setEmail(cursor.getString(cursor.getColumnIndex(FarmerModel.Column.EMAIL)));
        farmer.setFarmer_Group_ID(cursor.getInt(cursor.getColumnIndex(FarmerModel.Column.FARMER_GROUP_ID)));
        farmer.setFarmer_Problem_ID(cursor.getString(cursor.getColumnIndex(FarmerModel.Column.FARMER_PROBLEM_ID)));
        farmer.setFarmer_Problem_Other(cursor.getString(cursor.getColumnIndex(FarmerModel.Column.FARMER_PROBLEM_OTHER)));
        farmer.setFarmer_Status_ID(cursor.getInt(cursor.getColumnIndex(FarmerModel.Column.FARMER_STATUS_ID)));
        farmer.setFarmer_Type_ID(cursor.getString(cursor.getColumnIndex(FarmerModel.Column.FARMER_TYPE_ID)));
        farmer.setFirst_Name(cursor.getString(cursor.getColumnIndex(FarmerModel.Column.FIRST_NAME)));
        farmer.setGender_ID(cursor.getString(cursor.getColumnIndex(FarmerModel.Column.GENDER_ID)));
        farmer.setGovernment_Help_ID(cursor.getInt(cursor.getColumnIndex(FarmerModel.Column.GOVERNMENT_HELP_ID)));
        farmer.setHolding_Equipment(cursor.getInt(cursor.getColumnIndex(FarmerModel.Column.HOLDING_EQUIPMENT)));
        farmer.setHolding_Land_ID(cursor.getInt(cursor.getColumnIndex(FarmerModel.Column.HOLDING_LAND_ID)));
        farmer.setHome_ID(cursor.getInt(cursor.getColumnIndex(FarmerModel.Column.HOME_ID)));
        farmer.setHomeNo(cursor.getString(cursor.getColumnIndex(FarmerModel.Column.HOME_NO)));
        farmer.setImage_ID(cursor.getInt(cursor.getColumnIndex(FarmerModel.Column.IMAGE_ID)));
        farmer.setIsAgency(cursor.getInt(cursor.getColumnIndex(FarmerModel.Column.IS_AGENCY)));
        farmer.setIsCancel(cursor.getInt(cursor.getColumnIndex(FarmerModel.Column.IS_Cancel)));
        farmer.setIsDead(cursor.getInt(cursor.getColumnIndex(FarmerModel.Column.IS_DEAD)));
        farmer.setLast_Name(cursor.getString(cursor.getColumnIndex(FarmerModel.Column.LASTNAME)));
        farmer.setLatitude(cursor.getString(cursor.getColumnIndex(FarmerModel.Column.LATITUDE)));
        farmer.setLongitude(cursor.getString(cursor.getColumnIndex(FarmerModel.Column.LONGITUDE)));
        farmer.setMain_Job_ID(cursor.getInt(cursor.getColumnIndex(FarmerModel.Column.MAIN_JOB_ID)));
        farmer.setMarital_Status_ID(cursor.getInt(cursor.getColumnIndex(FarmerModel.Column.MARITAL_STATUS_ID)));
        farmer.setMobile(cursor.getString(cursor.getColumnIndex(FarmerModel.Column.MOBILE)));
        farmer.setPhone(cursor.getString(cursor.getColumnIndex(FarmerModel.Column.PHONE)));
        farmer.setPid(cursor.getLong(cursor.getColumnIndex(FarmerModel.Column.PID)));
        farmer.setPostCode(cursor.getString(cursor.getColumnIndex(FarmerModel.Column.POSTCODE)));
        farmer.setPrefix_ID(cursor.getInt(cursor.getColumnIndex(FarmerModel.Column.PREFIX_ID)));
        farmer.setProvince_ID(cursor.getInt(cursor.getColumnIndex(FarmerModel.Column.PROVINCE_ID)));
        farmer.setRevenue(cursor.getString(cursor.getColumnIndex(FarmerModel.Column.REVENUE)));
        farmer.setRoad(cursor.getString(cursor.getColumnIndex(FarmerModel.Column.ROAD)));
        farmer.setSecond_Job_ID(cursor.getInt(cursor.getColumnIndex(FarmerModel.Column.SECOND_JOB_ID)));
        farmer.setSoi(cursor.getString(cursor.getColumnIndex(FarmerModel.Column.SOI)));
        farmer.setStatus_ID(cursor.getString(cursor.getColumnIndex(FarmerModel.Column.STATUS_ID)));
        farmer.setTambol_ID(cursor.getInt(cursor.getColumnIndex(FarmerModel.Column.TAMBOL_ID)));
        farmer.setTax_ID(cursor.getInt(cursor.getColumnIndex(FarmerModel.Column.TAX_ID)));
        farmer.setUpdateBy(cursor.getString(cursor.getColumnIndex(FarmerModel.Column.UPDATE_BY)));
        farmer.setUpdateDate(cursor.getString(cursor.getColumnIndex(FarmerModel.Column.UPDATE_DATE)));
        farmer.setVillage(cursor.getString(cursor.getColumnIndex(FarmerModel.Column.VILLAGE)));
        farmer.setVillage_ID(cursor.getInt(cursor.getColumnIndex(FarmerModel.Column.VILLAGE_ID)));

        db.close();

        return farmer;

    }

    public ArrayList<ProvinceEntity> getProvinceList() {

        SQLiteDatabase db = this.getWritableDatabase();

        ArrayList<ProvinceEntity> provinces = new ArrayList<>();

        String selectQuery = "SELECT  * FROM " + ProvinceMaster.TABLE;

        Cursor cursor = db.rawQuery(selectQuery, null);

        if (cursor != null) {
            cursor.moveToFirst();
        }

        while (!cursor.isAfterLast()) {
            provinces.add(getProvinceInfo(cursor));
            cursor.moveToNext();
        }
        db.close();

        return provinces;
    }

    public ProvinceEntity getProvinceInfo(Cursor c) {

        ProvinceEntity province = new ProvinceEntity();

        String province_id = c.getString(c.getColumnIndex(ProvinceMaster.Column.PROVINCE_ID));
        String countryRanch_ID = c.getString(c.getColumnIndex(ProvinceMaster.Column.COUNTRYRANCH_ID));
        String province_NameTh = c.getString(c.getColumnIndex(ProvinceMaster.Column.PROVINCE_NAME_TH));
        String provine_Code = c.getString(c.getColumnIndex(ProvinceMaster.Column.PROVINCE_CODE));

        province.setProvince_ID(Integer.parseInt(province_id));
        province.setCountryRanch_ID(Integer.parseInt(countryRanch_ID));
        province.setProvince_NameTh(province_NameTh);
        province.setProvince_Code(Integer.parseInt(provine_Code));

        return province;
    }

    public ArrayList<AmphurEntity> getAmphur(int province) {

        ArrayList<AmphurEntity> amphurs = new ArrayList<>();

        SQLiteDatabase db = this.getReadableDatabase();

        String selectQuery = "SELECT  * FROM " + AmphurMaster.TABLE + " WHERE " + AmphurMaster.Column.PROVINCE_ID + " = " + province;

        Cursor cursor = db.rawQuery(selectQuery, null);

        if (cursor != null) {
            cursor.moveToFirst();
        }

        while (!cursor.isAfterLast()) {
            amphurs.add(getAmphurInfo(cursor));
            cursor.moveToNext();
        }
        db.close();

        return amphurs;
    }

    public AmphurEntity getAmphurInfo(Cursor c) {
        AmphurEntity amphur = new AmphurEntity();

        String amphur_ID = c.getString(c.getColumnIndex(AmphurMaster.Column.AMPHUR_ID));
        String amphur_Code = c.getString(c.getColumnIndex(AmphurMaster.Column.AMPHUR_CODE));
        String amphur_NameTh = c.getString(c.getColumnIndex(AmphurMaster.Column.AMPHUR_NAME_TH));
        String province_ID = c.getString(c.getColumnIndex(AmphurMaster.Column.PROVINCE_ID));

        amphur.setAmphur_ID(Integer.parseInt(amphur_ID));
        amphur.setAmphur_Code(Integer.parseInt(amphur_Code));
        amphur.setAmphur_nameTh(amphur_NameTh);
        amphur.setProvince_ID(Integer.parseInt(province_ID));

        return amphur;
    }

    public ArrayList<TambolEntity> getTambol(int amphur) {

        ArrayList<TambolEntity> tambols = new ArrayList<>();

        SQLiteDatabase db = this.getReadableDatabase();

        String selectQuery = "SELECT  * FROM " + TambolMaster.TABLE + " WHERE " + TambolMaster.Column.AMPHUR_ID + " = " + amphur;

        Cursor cursor = db.rawQuery(selectQuery, null);

        if (cursor != null) {
            cursor.moveToFirst();
        }
        while (!cursor.isAfterLast()) {
            tambols.add(getTabolInfo(cursor));
            cursor.moveToNext();
        }
        db.close();

        return tambols;
    }

    public TambolEntity getTabolInfo(Cursor c) {
        TambolEntity tambol = new TambolEntity();

        String tambol_ID = c.getString(c.getColumnIndex(TambolMaster.Column.TAMBOL_ID));
        String amphur_ID = c.getString(c.getColumnIndex(TambolMaster.Column.AMPHUR_ID));
        String tambol_Code = c.getString(c.getColumnIndex(TambolMaster.Column.TAMBOL_CODE));
        String tambol_NameTh = c.getString(c.getColumnIndex(TambolMaster.Column.TAMBOL_NAME_TH));

        tambol.setTambol_ID(Integer.parseInt(tambol_ID));
        tambol.setAmphur_ID(Integer.parseInt(amphur_ID));
        tambol.setTambol_Code(Integer.parseInt(tambol_Code));
        tambol.setTambol_NameTh(tambol_NameTh);

        return tambol;
    }

    public ArrayList<VillageEntity> getVillage(int tambol) {
        ArrayList<VillageEntity> villages = new ArrayList<>();

        SQLiteDatabase db = this.getReadableDatabase();

        String selectQuery = "SELECT  * FROM " + VillageMaster.TABLE + " WHERE " + VillageMaster.Column.TAMBOL_ID + " = " + tambol;

        Cursor cursor = db.rawQuery(selectQuery, null);

        if (cursor != null) {
            cursor.moveToFirst();
        }
        while (!cursor.isAfterLast()) {
            villages.add(getVillageInfo(cursor));
            cursor.moveToNext();
        }
        db.close();

        return villages;
    }

    public VillageEntity getVillageInfo(Cursor c) {
        VillageEntity village = new VillageEntity();

        String village_ID = c.getString(c.getColumnIndex(VillageMaster.Column.VILLAGE_ID));
        String tambol_ID = c.getString(c.getColumnIndex(VillageMaster.Column.TAMBOL_ID));
        String moo = c.getString(c.getColumnIndex(VillageMaster.Column.MOO));
        String vilage_Code = c.getString(c.getColumnIndex(VillageMaster.Column.VILLAGE_CODE));
        String village_Name = c.getString(c.getColumnIndex(VillageMaster.Column.VILLAGE_NAME));

        village.setVillage_ID(Integer.parseInt(village_ID));
        village.setTambol_ID(Integer.parseInt(tambol_ID));
        village.setMoo(Integer.parseInt(moo));
        village.setVilage_Code(Integer.parseInt(vilage_Code));
        village.setVillage_Name(village_Name);

        return village;
    }

    public ArrayList<JobEntity> getMainJob() {

        ArrayList<JobEntity> mainjobs = new ArrayList<>();

        SQLiteDatabase db = this.getReadableDatabase();

        Cursor cursor = db.query(MainJobMaster.TABLE, null, null, null, null, null, null);

        if (cursor != null) {
            cursor.moveToFirst();
        }
        while (!cursor.isAfterLast()) {
            mainjobs.add(getMainJobInfo(cursor));
            cursor.moveToNext();
        }
        db.close();

        return mainjobs;
    }

    public JobEntity getMainJobInfo(Cursor c) {
        JobEntity mainjob = new JobEntity();

        String main_Job_ID = c.getString(c.getColumnIndex(MainJobMaster.Column.MAIN_JOB_ID));
        String main_Job_Name = c.getString(c.getColumnIndex(MainJobMaster.Column.MAIN_JOB_NAME));

        mainjob.setMain_Job_ID(Integer.parseInt(main_Job_ID));
        mainjob.setMain_Job_Name(main_Job_Name);

        return mainjob;
    }

    public ArrayList<SecondJobEntity> getSecondJob() {
        ArrayList<SecondJobEntity> secondJobs = new ArrayList<>();

        SQLiteDatabase db = this.getReadableDatabase();

        Cursor cursor = db.query(SecondJobMaster.TABLE, null, null, null, null, null, null);

        if (cursor != null) {
            cursor.moveToFirst();
        }
        while (!cursor.isAfterLast()) {
            secondJobs.add(getSecondJobInfo(cursor));
            cursor.moveToNext();
        }
        db.close();

        return secondJobs;
    }

    public SecondJobEntity getSecondJobInfo(Cursor c) {
        SecondJobEntity secondjob = new SecondJobEntity();

        String second_Job_ID = c.getString(c.getColumnIndex(SecondJobMaster.Column.SECOND_JOB_ID));
        String second_Job_Name = c.getString(c.getColumnIndex(SecondJobMaster.Column.SECOND_JOB_NAME));

        secondjob.setSecond_Job_ID(Integer.parseInt(second_Job_ID));
        secondjob.setSecond_Job_Name(second_Job_Name);

        return secondjob;
    }

    public ArrayList<LandEntity> getLand() {
        ArrayList<LandEntity> lands = new ArrayList<>();

        SQLiteDatabase db = this.getReadableDatabase();

        Cursor cursor = db.query(HoldingLandMaster.TABLE, null, null, null, null, null, null);

        if (cursor != null) {
            cursor.moveToFirst();
        }
        while (!cursor.isAfterLast()) {
            lands.add(getLandInfo(cursor));
            cursor.moveToNext();
        }
        db.close();

        return lands;
    }

    public LandEntity getLandInfo(Cursor c) {
        LandEntity land = new LandEntity();

        String holding_Land_ID = c.getString(c.getColumnIndex(HoldingLandMaster.Column.HOLDING_LAND_ID));
        String holding_Land_Name = c.getString(c.getColumnIndex(HoldingLandMaster.Column.HOLDING_LAND_NAME));

        land.setHolding_Land_ID(Integer.parseInt(holding_Land_ID));
        land.setHolding_Land_Name(holding_Land_Name);

        return land;
    }

    public ArrayList<HelpEntity> getHelp() {
        ArrayList<HelpEntity> helps = new ArrayList<>();

        SQLiteDatabase db = this.getReadableDatabase();

        Cursor cursor = db.query(HelpMaster.TABLE, null, null, null, null, null, null);

        if (cursor != null) {
            cursor.moveToFirst();
        }
        while (!cursor.isAfterLast()) {
            helps.add(getHelpInfo(cursor));
            cursor.moveToNext();
        }
        db.close();

        return helps;
    }

    public HelpEntity getHelpInfo(Cursor c) {
        HelpEntity help = new HelpEntity();

        String government_Help_ID = c.getString(c.getColumnIndex(HelpMaster.Column.GOVERNMENT_HELP_ID));
        String government_Help_Name = c.getString(c.getColumnIndex(HelpMaster.Column.GOVERNMENT_HELP_NAME));

        help.setGovernment_Help_ID(Integer.parseInt(government_Help_ID));
        help.setGovernment_Help_Name(government_Help_Name);

        return help;
    }

    public ArrayList<FarmerProblemEntity> getFarmerProblem() {
        ArrayList<FarmerProblemEntity> farmerProblems = new ArrayList<>();

        SQLiteDatabase db = this.getReadableDatabase();

        Cursor cursor = db.query(FarmerProblemMaster.TABLE, null, null, null, null, null, null);

        if (cursor != null) {
            cursor.moveToFirst();
        }
        while (!cursor.isAfterLast()) {
            farmerProblems.add(getFarmerProblemInfo(cursor));
            cursor.moveToNext();
        }
        db.close();

        return farmerProblems;
    }

    public FarmerProblemEntity getFarmerProblemInfo(Cursor c) {
        FarmerProblemEntity farmerProblem = new FarmerProblemEntity();

        String farmer_Problem_ID = c.getString(c.getColumnIndex(FarmerProblemMaster.Column.FARMER_PROBLEM_ID));
        String farmer_Problem_Name = c.getString(c.getColumnIndex(FarmerProblemMaster.Column.FARMER_PROBLEM_NAME));
        String farmer_Problem_Desc = c.getString(c.getColumnIndex(FarmerProblemMaster.Column.FARMER_PROBLEM_DESC));

        farmerProblem.setFarmer_Problem_ID(Integer.parseInt(farmer_Problem_ID));
        farmerProblem.setFarmer_Problem_Name(farmer_Problem_Name);
        farmerProblem.setFarmer_Problem_Desc(farmer_Problem_Desc);

        return farmerProblem;
    }

    public ArrayList<StandardEntity> getStandard() {
        ArrayList<StandardEntity> standards = new ArrayList<>();

        SQLiteDatabase db = this.getReadableDatabase();

        Cursor cursor = db.query(StandardMaster.TABLE, null, null, null, null, null, null);

        if (cursor != null) {
            cursor.moveToFirst();
        }
        while (!cursor.isAfterLast()) {
            standards.add(getStandardInfo(cursor));
            cursor.moveToNext();
        }
        db.close();

        return standards;
    }

    public StandardEntity getStandardInfo(Cursor c) {
        StandardEntity standard = new StandardEntity();

        String standard_ID = c.getString(c.getColumnIndex(StandardMaster.Column.STANDARD_ID));
        String standard_Name = c.getString(c.getColumnIndex(StandardMaster.Column.STANDARD_NAME));


        standard.setStandard_ID(Integer.parseInt(standard_ID));
        standard.setStandard_Name(standard_Name);


        return standard;
    }

    public ArrayList<SupportEntity> getSupport() {
        ArrayList<SupportEntity> supports = new ArrayList<>();

        SQLiteDatabase db = this.getReadableDatabase();

        Cursor cursor = db.query(SupportStandardMaster.TABLE, null, null, null, null, null, null);

        if (cursor != null) {
            cursor.moveToFirst();
        }
        while (!cursor.isAfterLast()) {
            supports.add(getSupportInfo(cursor));
            cursor.moveToNext();
        }
        db.close();

        return supports;
    }

    public SupportEntity getSupportInfo(Cursor c) {
        SupportEntity support = new SupportEntity();

        String support_Standard_ID = c.getString(c.getColumnIndex(SupportStandardMaster.Column.SUPPORT_STANDARD_ID));
        String support_Standard_Name = c.getString(c.getColumnIndex(SupportStandardMaster.Column.SUPPORT_STANDARD_NAME));


        support.setSupport_Standard_ID(Integer.parseInt(support_Standard_ID));
        support.setSupport_Standard_Name(support_Standard_Name);


        return support;
    }

    public ArrayList<FarmEntity> getFarm() {
        ArrayList<FarmEntity> farms = new ArrayList<>();

        SQLiteDatabase db = this.getReadableDatabase();

        Cursor cursor = db.query(FarmerModel.TABLE, null, null, null, null, null, null);

        if (cursor != null) {
            cursor.moveToFirst();
        }
        while (!cursor.isAfterLast()) {
            farms.add(getFarmInfo(cursor));
            cursor.moveToNext();
        }
        db.close();
        return farms;
    }

    public FarmEntity getFarmInfo(Cursor c) {
        FarmEntity farm = new FarmEntity();

        String farm_ID = c.getString(c.getColumnIndex(FarmModel.Column.FARM_ID));
        String amphur_ID = c.getString(c.getColumnIndex(FarmModel.Column.AMPHUR_ID));
        String animalWorth = c.getString(c.getColumnIndex(FarmModel.Column.ANIMAL_WORTH));
        String area_Rai = c.getString(c.getColumnIndex(FarmModel.Column.AREA_RAI));
        String area_Ngan = c.getString(c.getColumnIndex(FarmModel.Column.AREA_NGAN));
        String area_Type_ID = c.getString(c.getColumnIndex(FarmModel.Column.AREA_TYPE_ID));
        String cancelBy = c.getString(c.getColumnIndex(FarmModel.Column.CANCEL_BY));
        String cancelDate = c.getString(c.getColumnIndex(FarmModel.Column.CANCEL_DATE));
        String createBy = c.getString(c.getColumnIndex(FarmModel.Column.CREATE_BY));
        String createDate = c.getString(c.getColumnIndex(FarmModel.Column.CREATE_DATE));
        String email = c.getString(c.getColumnIndex(FarmModel.Column.EMAIL));
        String farm_Name  = c.getString(c.getColumnIndex(FarmModel.Column.FARM_NAME));
        String farm_Owner_Type_ID = c.getString(c.getColumnIndex(FarmModel.Column.FARM_OWNER_TYPE_ID));
        String farm_Problem_Desc = c.getString(c.getColumnIndex(FarmModel.Column.FARM_PROBLEM_DESC));
        String farm_Stadard_Number = c.getString(c.getColumnIndex(FarmModel.Column.FARM_STANDARD_NUMBER));
        String farmer_ID = c.getString(c.getColumnIndex(FarmModel.Column.FARMER_ID));
        String homeNo = c.getString(c.getColumnIndex(FarmModel.Column.HOME_NO));
        String isCancel = c.getString(c.getColumnIndex(FarmModel.Column.IS_CANCEL));
        String latitude = c.getString(c.getColumnIndex(FarmModel.Column.LATITUDE));
        String longitude = c.getString(c.getColumnIndex(FarmModel.Column.LONGITUDE));
        String mobile = c.getString(c.getColumnIndex(FarmModel.Column.MOBILE));
        String orderIndex = c.getString(c.getColumnIndex(FarmModel.Column.ORDER_INDEX));
        String phone = c.getString(c.getColumnIndex(FarmModel.Column.PHONE));
        String postcode = c.getString(c.getColumnIndex(FarmModel.Column.POSTCODE));
        String province_ID = c.getString(c.getColumnIndex(FarmModel.Column.PROVINCE_ID));


    }


    public void addPrefixs(List<PrefixEntity> prefixs) {
        SQLiteDatabase db = this.getWritableDatabase();
        db.beginTransaction();
        try {
            for (PrefixEntity prefix : prefixs) {
                ContentValues values = new ContentValues();

                values.put(PrefixMaster.Column.PREFIX_ID, prefix.getPrefix_ID());
                values.put(PrefixMaster.Column.ORDER_INDEX, prefix.getOrderIndex());
                values.put(PrefixMaster.Column.PREFIX_NAME_EN, prefix.getPrefix_NameEn());
                values.put(PrefixMaster.Column.PREFIX_NAME_TH, prefix.getPrefix_NameTh());

                db.insert(PrefixMaster.TABLE, null, values);
            }
            db.setTransactionSuccessful();
        } catch (SQLException ex) {
            Log.d(TAG, ex.toString());
        }
        db.endTransaction();
        db.close();
    }

    public void addProvinces(List<ProvinceEntity> provinces) {
        SQLiteDatabase db = this.getWritableDatabase();
        db.beginTransaction();
        try {
            for (ProvinceEntity province : provinces) {
                ContentValues values = new ContentValues();

                values.put(ProvinceMaster.Column.PROVINCE_ID, province.getProvince_ID());
                values.put(ProvinceMaster.Column.COUNTRYRANCH_ID, province.getCountryRanch_ID());
                values.put(ProvinceMaster.Column.GEO_ID, province.getGeo_ID());
                values.put(ProvinceMaster.Column.LATITUDE, province.getLatitude());
                values.put(ProvinceMaster.Column.LONGITUDE, province.getLongitude());
                values.put(ProvinceMaster.Column.ORDER_INDEX, province.getOrderIndex());
                values.put(ProvinceMaster.Column.PROVINCE_NAME_EN, province.getProvince_NameEn());
                values.put(ProvinceMaster.Column.PROVINCE_NAME_TH, province.getProvince_NameTh());
                values.put(ProvinceMaster.Column.PROVINCE_CODE, province.getProvince_Code());
                values.put(ProvinceMaster.Column.STATUS_ID, province.getStatus_ID());

                db.insert(ProvinceMaster.TABLE, null, values);
            }
            db.setTransactionSuccessful();
        } catch (SQLException ex) {
            Log.d(TAG, ex.toString());
        }
        db.endTransaction();
        db.close();
    }

    public void addAmphurs(List<AmphurEntity> amphurs) {
        SQLiteDatabase db = this.getWritableDatabase();
        db.beginTransaction();
        try {
            for (AmphurEntity amphur : amphurs) {
                ContentValues values = new ContentValues();

                values.put(AmphurMaster.Column.AMPHUR_ID, amphur.getAmphur_ID());
                values.put(AmphurMaster.Column.AMPHUR_CODE, amphur.getAmphur_Code());
                values.put(AmphurMaster.Column.AMPHUR_NAME_EN, amphur.getAmphur_NameEn());
                values.put(AmphurMaster.Column.AMPHUR_NAME_TH, amphur.getAmphur_nameTh());
                values.put(AmphurMaster.Column.LATITUDE, amphur.getLatitude());
                values.put(AmphurMaster.Column.LONGITUDE, amphur.getLongtitude());
                values.put(AmphurMaster.Column.ORDER_INDEX, amphur.getOrderIndex());
                values.put(AmphurMaster.Column.PROVINCE_ID, amphur.getProvince_ID());
                values.put(AmphurMaster.Column.STATUS_ID, amphur.getStatus_ID());

                db.insert(AmphurMaster.TABLE, null, values);
            }
            db.setTransactionSuccessful();
        } catch (SQLException ex) {
            Log.d(TAG, ex.toString());
        }
        db.endTransaction();
        db.close();
    }

    public void addTambols(List<TambolEntity> tambols) {
        SQLiteDatabase db = this.getWritableDatabase();
        db.beginTransaction();
        try {
            for (TambolEntity tambol : tambols) {
                ContentValues values = new ContentValues();

                values.put(TambolMaster.Column.TAMBOL_ID, tambol.getTambol_ID());
                values.put(TambolMaster.Column.AMPHUR_ID, tambol.getAmphur_ID());
                values.put(TambolMaster.Column.LATITUDE, tambol.getLatitude());
                values.put(TambolMaster.Column.LONGITUDE, tambol.getLongitude());
                values.put(TambolMaster.Column.ORDER_INDEX, tambol.getOrderIndex());
                values.put(TambolMaster.Column.STATUS_ID, tambol.getStatus_ID());
                values.put(TambolMaster.Column.TAMBOL_CODE, tambol.getTambol_Code());
                values.put(TambolMaster.Column.TAMBOL_NAME_EN, tambol.getTambol_NameEn());
                values.put(TambolMaster.Column.TAMBOL_NAME_TH, tambol.getTambol_NameTh());

                db.insert(TambolMaster.TABLE, null, values);
            }
            db.setTransactionSuccessful();
        } catch (SQLException ex) {
            Log.d(TAG, ex.toString());
        }
        db.endTransaction();
        db.close();
    }

    public void addVillages(List<VillageEntity> villages) {
        SQLiteDatabase db = this.getWritableDatabase();
        db.beginTransaction();
        try {
            for (VillageEntity village : villages) {
                ContentValues values = new ContentValues();

                values.put(VillageMaster.Column.VILLAGE_ID, village.getVillage_ID());
                values.put(VillageMaster.Column.MOO, village.getMoo());
                values.put(VillageMaster.Column.ORDER_INDEX, village.getOrderIndex());
                values.put(VillageMaster.Column.STATUS_ID, village.getStatus_ID());
                values.put(VillageMaster.Column.TAMBOL_ID, village.getTambol_ID());
                values.put(VillageMaster.Column.VILLAGE_CODE, village.getVilage_Code());
                values.put(VillageMaster.Column.VILLAGE_NAME, village.getVillage_Name());

                db.insert(VillageMaster.TABLE, null, values);
            }
            db.setTransactionSuccessful();
        } catch (SQLException ex) {
            Log.d(TAG, ex.toString());
        }
        db.endTransaction();
        db.close();
    }

    public void addFarmer(FarmerEntity farmer) {

        SQLiteDatabase db = this.getWritableDatabase();

        db.insert(FarmerModel.TABLE, null, setInfoFarmer(farmer));

        db.close();
    }


    public void addFarmOwner(List<OwnerEntity> owners) {
        SQLiteDatabase db = this.getWritableDatabase();
        db.beginTransaction();
        try {
            for (OwnerEntity owner : owners) {
                ContentValues values = new ContentValues();

                values.put(FarmOwnerMaster.Column.FARM_OWNER_TYPE_ID, owner.getFarm_Owner_Type_ID());
                values.put(FarmOwnerMaster.Column.FARM_OWNER_TYPE_NAME, owner.getFarm_Owner_Type_Name());
                values.put(FarmOwnerMaster.Column.ORDER_INDEX, owner.getOrderIndex());
                values.put(FarmOwnerMaster.Column.STATUS_ID, owner.getStatus_ID());

                db.insert(FarmOwnerMaster.TABLE, null, values);
            }
            db.setTransactionSuccessful();
        } catch (SQLException ex) {
            Log.d(TAG, ex.toString());
        }
        db.endTransaction();
        db.close();
    }

    public void addFarmProblem(List<FarmProblemEntity> farmProblems) {
        SQLiteDatabase db = this.getWritableDatabase();
        db.beginTransaction();
        try {
            for (FarmProblemEntity farmProblem : farmProblems) {
                ContentValues values = new ContentValues();

                values.put(FarmProblemMaster.Column.FARM_PROBLEM_ID, farmProblem.getFarm_Problem_ID());
                values.put(FarmProblemMaster.Column.FARM_PROBLEM_NAME, farmProblem.getFarm_Problem_Name());
                values.put(FarmProblemMaster.Column.ORDER_INDEX, farmProblem.getOrderIndex());
                values.put(FarmProblemMaster.Column.STATUS_ID, farmProblem.getStatus_ID());

                db.insert(FarmProblemMaster.TABLE, null, values);
            }
            db.setTransactionSuccessful();
        } catch (SQLException ex) {
            Log.d(TAG, ex.toString());
        }
        db.endTransaction();
        db.close();
    }

    public void addFarmerProblem(List<FarmerProblemEntity> farmerProblems) {
        SQLiteDatabase db = this.getWritableDatabase();
        db.beginTransaction();
        try {
            for (FarmerProblemEntity farmerProblem : farmerProblems) {
                ContentValues values = new ContentValues();

                values.put(FarmerProblemMaster.Column.FARMER_PROBLEM_ID, farmerProblem.getFarmer_Problem_ID());
                values.put(FarmerProblemMaster.Column.FARMER_PROBLEM_DESC, farmerProblem.getFarmer_Problem_Desc());
                values.put(FarmerProblemMaster.Column.FARMER_PROBLEM_NAME, farmerProblem.getFarmer_Problem_Name());
                values.put(FarmerProblemMaster.Column.ORDER_INDEX, farmerProblem.getOrderIndex());
                values.put(FarmerProblemMaster.Column.STATUS_ID, farmerProblem.getStatus_ID());

                db.insert(FarmerProblemMaster.TABLE, null, values);
            }
            db.setTransactionSuccessful();
        } catch (SQLException ex) {
            Log.d(TAG, ex.toString());
        }
        db.endTransaction();
        db.close();
    }

    public void addFarmerType(List<FarmerTypeEntity> farmerTypes) {
        SQLiteDatabase db = this.getWritableDatabase();
        db.beginTransaction();
        try {
            for (FarmerTypeEntity farmerType : farmerTypes) {
                ContentValues values = new ContentValues();

                values.put(FarmerTypeMaster.Column.FARMER_TYPE_ID, farmerType.getFarmer_Type_ID());
                values.put(FarmerTypeMaster.Column.FARMER_TYPE_NAME, farmerType.getFarmer_Type_Name());
                values.put(FarmerTypeMaster.Column.ORDER_INDEX, farmerType.getOrderIndex());
                values.put(FarmerTypeMaster.Column.STATUS_ID, farmerType.getStatus_ID());

                db.insert(FarmerTypeMaster.TABLE, null, values);
            }
            db.setTransactionSuccessful();
        } catch (SQLException ex) {
            Log.d(TAG, ex.toString());
        }
        db.endTransaction();
        db.close();
    }

    public void addFeed(List<FeedEntity> feeds) {
        SQLiteDatabase db = this.getWritableDatabase();
        db.beginTransaction();
        try {
            for (FeedEntity feed : feeds) {
                ContentValues values = new ContentValues();

                values.put(FeedModel.Column.FEED_ID, feed.getFeed_ID());
                values.put(FeedModel.Column.CREATE_BY, feed.getCreateBy());
                values.put(FeedModel.Column.CREATE_DATE, feed.getCreateDate());
                values.put(FeedModel.Column.FARM_ID, feed.getFarm_ID());
                values.put(FeedModel.Column.FEED_AREA_NGAN, feed.getFeed_Area_Ngan());
                values.put(FeedModel.Column.FEED_AREA_RAI, feed.getFeed_Area_Rai());
                values.put(FeedModel.Column.FEED_KIND_ID, feed.getFeed_Kind_ID());
                values.put(FeedModel.Column.FEED_KIND_OTHER, feed.getFeed_Kind_Other());
                values.put(FeedModel.Column.FEED_TYPE_ID, feed.getFeed_Type_ID());
                values.put(FeedModel.Column.FEED_TYPE_OTHER, feed.getFeed_Type_Other());
                values.put(FeedModel.Column.FEED_WATER_TYPE_ID, feed.getFeed_Water_Type_ID());
                values.put(FeedModel.Column.ORDER_INDEX, feed.getOrderIndex());
                values.put(FeedModel.Column.STATUS_ID, feed.getStatus_ID());
                values.put(FeedModel.Column.UPDATE_BY, feed.getUpdateBy());
                values.put(FeedModel.Column.UPDATE_DATE, feed.getUpdateDate());

                db.insert(FeedModel.TABLE, null, values);
            }
            db.setTransactionSuccessful();
        } catch (SQLException ex) {
            Log.d(TAG, ex.toString());
        }
        db.endTransaction();
        db.close();
    }

    public void addFeedKind(List<FeedKindEntity> feedKinds) {
        SQLiteDatabase db = this.getWritableDatabase();
        db.beginTransaction();
        try {
            for (FeedKindEntity feedKind : feedKinds) {
                ContentValues values = new ContentValues();

                values.put(FeedKindMaster.Column.FEED_KIND_ID, feedKind.getFeed_Kind_ID());
                values.put(FeedKindMaster.Column.FEED_KIND_NAME_EN, feedKind.getFeed_Kind_NameEn());
                values.put(FeedKindMaster.Column.FEED_KIND_NAME_TH, feedKind.getFeed_Kind_NameTh());
                values.put(FeedKindMaster.Column.ORDER_INDEX, feedKind.getOrderIndex());
                values.put(FeedKindMaster.Column.STATUS_ID, feedKind.getStatus_ID());

                db.insert(FeedKindMaster.TABLE, null, values);
            }
            db.setTransactionSuccessful();
        } catch (SQLException ex) {
            Log.d(TAG, ex.toString());
        }
        db.endTransaction();
        db.close();
    }

    public void addHelp(List<HelpEntity> helps) {
        SQLiteDatabase db = this.getWritableDatabase();
        db.beginTransaction();
        try {
            for (HelpEntity help : helps) {
                ContentValues values = new ContentValues();

                values.put(HelpMaster.Column.GOVERNMENT_HELP_ID, help.getGovernment_Help_ID());
                values.put(HelpMaster.Column.GOVERNMENT_HELP_NAME, help.getGovernment_Help_Name());
                values.put(HelpMaster.Column.ORDER_INDEX, help.getOrderIndex());
                values.put(HelpMaster.Column.STATUS_ID, help.getStatus_ID());

                db.insert(HelpMaster.TABLE, null, values);
            }
            db.setTransactionSuccessful();
        } catch (SQLException ex) {
            Log.d(TAG, ex.toString());
        }
        db.endTransaction();
        db.close();
    }

    public void addHoldingLand(List<LandEntity> lands) {
        SQLiteDatabase db = this.getWritableDatabase();
        db.beginTransaction();
        try {
            for (LandEntity land : lands) {
                ContentValues values = new ContentValues();

                values.put(HoldingLandMaster.Column.HOLDING_LAND_ID, land.getHolding_Land_ID());
                values.put(HoldingLandMaster.Column.HOLDING_LAND_NAME, land.getHolding_Land_Name());
                values.put(HoldingLandMaster.Column.ORDER_INDEX, land.getOrderIndex());
                values.put(HoldingLandMaster.Column.STATUS_ID, land.getStatus_ID());

                db.insert(HoldingLandMaster.TABLE, null, values);
            }
            db.setTransactionSuccessful();
        } catch (SQLException ex) {
            Log.d(TAG, ex.toString());
        }
        db.endTransaction();
        db.close();
    }

    public void addMainJob(List<JobEntity> mainjobs) {
        SQLiteDatabase db = this.getWritableDatabase();
        db.beginTransaction();
        try {
            for (JobEntity mainjob : mainjobs) {
                ContentValues values = new ContentValues();

                values.put(MainJobMaster.Column.MAIN_JOB_ID, mainjob.getMain_Job_ID());
                values.put(MainJobMaster.Column.MAIN_JOB_NAME, mainjob.getMain_Job_Name());
                values.put(MainJobMaster.Column.ORDER_INDEX, mainjob.getOrderIndex());
                values.put(MainJobMaster.Column.STATUS_ID, mainjob.getStatus_ID());

                db.insert(MainJobMaster.TABLE, null, values);
            }
            db.setTransactionSuccessful();
        } catch (SQLException ex) {
            Log.d(TAG, ex.toString());
        }
        db.endTransaction();
        db.close();
    }

    public void addSecondJob(List<SecondJobEntity> secondjobs) {
        SQLiteDatabase db = this.getWritableDatabase();
        db.beginTransaction();
        try {
            for (SecondJobEntity secondjob : secondjobs) {
                ContentValues values = new ContentValues();

                values.put(SecondJobMaster.Column.SECOND_JOB_ID, secondjob.getSecond_Job_ID());
                values.put(SecondJobMaster.Column.SECOND_JOB_NAME, secondjob.getSecond_Job_Name());
                values.put(SecondJobMaster.Column.ORDER_INDEX, secondjob.getOrderIndex());
                values.put(SecondJobMaster.Column.STATUS_ID, secondjob.getStatus_ID());

                db.insert(SecondJobMaster.TABLE, null, values);
            }
            db.setTransactionSuccessful();
        } catch (SQLException ex) {
            Log.d(TAG, ex.toString());
        }
        db.endTransaction();
        db.close();
    }

    public void addStandard(List<StandardEntity> standards) {
        SQLiteDatabase db = this.getWritableDatabase();
        db.beginTransaction();
        try {
            for (StandardEntity standard : standards) {
                ContentValues values = new ContentValues();

                values.put(StandardMaster.Column.STANDARD_ID, standard.getStandard_ID());
                values.put(StandardMaster.Column.STANDARD_NAME, standard.getStandard_Name());
                values.put(StandardMaster.Column.ORDER_INDEX, standard.getOrderIndex());
                values.put(StandardMaster.Column.STATUS_ID, standard.getStatus_ID());

                db.insert(StandardMaster.TABLE, null, values);
            }
            db.setTransactionSuccessful();
        } catch (SQLException ex) {
            Log.d(TAG, ex.toString());
        }
        db.endTransaction();
        db.close();
    }

    public void addSupportStandard(List<SupportEntity> supports) {
        SQLiteDatabase db = this.getWritableDatabase();
        db.beginTransaction();
        try {
            for (SupportEntity support : supports) {
                ContentValues values = new ContentValues();

                values.put(SupportStandardMaster.Column.SUPPORT_STANDARD_ID, support.getSupport_Standard_ID());
                values.put(SupportStandardMaster.Column.SUPPORT_STANDARD_NAME, support.getSupport_Standard_Name());
                values.put(SupportStandardMaster.Column.ORDER_INDEX, support.getOrderIndex());
                values.put(SupportStandardMaster.Column.STATUS_ID, support.getStatus_ID());

                db.insert(SupportStandardMaster.TABLE, null, values);
            }
            db.setTransactionSuccessful();
        } catch (SQLException ex) {
            Log.d(TAG, ex.toString());
        }
        db.endTransaction();
        db.close();
    }

    public void addListFarm(List<FarmEntity> farms) {
        SQLiteDatabase db = this.getWritableDatabase();
        db.beginTransaction();
        try {
            for (FarmEntity farm : farms) {

                ContentValues values = new ContentValues();

                setInfoFarm(farm);

                db.insert(SupportStandardMaster.TABLE, null, values);
            }
            db.setTransactionSuccessful();
        } catch (SQLException ex) {
            Log.d(TAG, ex.toString());
        }
        db.endTransaction();
        db.close();
    }

    public void updateFarmer(FarmerEntity farmer, int id) {

        SQLiteDatabase db = this.getWritableDatabase();

        db.update(FarmerModel.TABLE, setInfoFarmer(farmer), FarmerModel.Column.FARMER_ID + "=" + id, null);

        db.close();
    }

    public void updateFarm(FarmEntity farm, int id) {

        SQLiteDatabase db = this.getWritableDatabase();

        db.update(FarmModel.TABLE, setInfoFarm(farm), FarmModel.Column.FARM_ID + "=" + id, null);

        db.close();
    }

    public ContentValues setInfoFarmer(FarmerEntity farmer) {

        ContentValues values = new ContentValues();

        values.put(FarmerModel.Column.FARMER_ID, farmer.getFarmer_ID());
        values.put(FarmerModel.Column.AMPHUR_ID, farmer.getAmphur_ID());
        values.put(FarmerModel.Column.BIRTHDAY, farmer.getBirthDay());
        values.put(FarmerModel.Column.COMPANY_TYPE_ID, farmer.getCompany_Type_ID());
        values.put(FarmerModel.Column.CREATE_BY, farmer.getCreateBy());
        values.put(FarmerModel.Column.CREATE_DATE, farmer.getCreateDate());
        values.put(FarmerModel.Column.DEBTAMOUNT, farmer.getDebtAmount());
        values.put(FarmerModel.Column.DEBTSAMOUNT_IN, farmer.getDebtsAmountIn());
        values.put(FarmerModel.Column.DEBTSAMOUNT_OUT, farmer.getDebtsAmountOut());
        values.put(FarmerModel.Column.EMAIL, farmer.getEmail());
        values.put(FarmerModel.Column.FARMER_GROUP_ID, farmer.getFarmer_Group_ID());
        values.put(FarmerModel.Column.FARMER_PROBLEM_ID, farmer.getFarmer_Problem_ID());
        values.put(FarmerModel.Column.FARMER_PROBLEM_OTHER, farmer.getFarmer_Problem_Other());
        values.put(FarmerModel.Column.FARMER_STATUS_ID, farmer.getStatus_ID());
        values.put(FarmerModel.Column.FARMER_TYPE_ID, farmer.getFarmer_Type_ID());
        values.put(FarmerModel.Column.FIRST_NAME, farmer.getFirst_Name());
        values.put(FarmerModel.Column.GENDER_ID, farmer.getGender_ID());
        values.put(FarmerModel.Column.GOVERNMENT_HELP_ID, farmer.getGovernment_Help_ID());
        values.put(FarmerModel.Column.HOLDING_EQUIPMENT, farmer.getHolding_Equipment());
        values.put(FarmerModel.Column.HOLDING_LAND_ID, farmer.getHolding_Land_ID());
        values.put(FarmerModel.Column.HOME_ID, farmer.getHome_ID());
        values.put(FarmerModel.Column.HOME_NO, farmer.getHomeNo());
        values.put(FarmerModel.Column.IMAGE_ID, farmer.getImage_ID());
        values.put(FarmerModel.Column.IS_AGENCY, farmer.getIsAgency());
        values.put(FarmerModel.Column.IS_Cancel, farmer.getIsCancel());
        values.put(FarmerModel.Column.IS_DEAD, farmer.getIsDead());
        values.put(FarmerModel.Column.LASTNAME, farmer.getLast_Name());
        values.put(FarmerModel.Column.LATITUDE, farmer.getLatitude());
        values.put(FarmerModel.Column.LONGITUDE, farmer.getLongitude());
        values.put(FarmerModel.Column.MAIN_JOB_ID, farmer.getMain_Job_ID());
        values.put(FarmerModel.Column.MARITAL_STATUS_ID, farmer.getMarital_Status_ID());
        values.put(FarmerModel.Column.MOBILE, farmer.getMobile());
        values.put(FarmerModel.Column.PHONE, farmer.getPhone());
        values.put(FarmerModel.Column.PID, farmer.getPid());
        values.put(FarmerModel.Column.POSTCODE, farmer.getPostCode());
        values.put(FarmerModel.Column.PREFIX_ID, farmer.getPrefix_ID());
        values.put(FarmerModel.Column.PROVINCE_ID, farmer.getProvince_ID());
        values.put(FarmerModel.Column.REVENUE, farmer.getRevenue());
        values.put(FarmerModel.Column.ROAD, farmer.getRoad());
        values.put(FarmerModel.Column.SECOND_JOB_ID, farmer.getSecond_Job_ID());
        values.put(FarmerModel.Column.SOI, farmer.getSoi());
        values.put(FarmerModel.Column.STATUS_ID, farmer.getStatus_ID());
        values.put(FarmerModel.Column.TAMBOL_ID, farmer.getTambol_ID());
        values.put(FarmerModel.Column.TAX_ID, farmer.getTax_ID());
        values.put(FarmerModel.Column.UPDATE_BY, farmer.getUpdateBy());
        values.put(FarmerModel.Column.UPDATE_DATE, farmer.getUpdateDate());
        values.put(FarmerModel.Column.VILLAGE, farmer.getVillage());
        values.put(FarmerModel.Column.VILLAGE_ID, farmer.getVillage_ID());

        return values;
    }

    public ContentValues setInfoFarm(FarmEntity farm) {

        ContentValues values = new ContentValues();

        values.put(FarmModel.Column.FARM_ID, farm.getFarmer_ID());
        values.put(FarmModel.Column.AMPHUR_ID, farm.getAmphur_ID());
        values.put(FarmModel.Column.ANIMAL_WORTH, farm.getAnimalWorth());
        values.put(FarmModel.Column.AREA_RAI, farm.getArea_Rai());
        values.put(FarmModel.Column.AREA_NGAN, farm.getArea_Ngan());
        values.put(FarmModel.Column.AREA_TYPE_ID, farm.getArea_Type_ID());
        values.put(FarmModel.Column.CANCEL_BY, farm.getCancelBy());
        values.put(FarmModel.Column.CANCEL_DATE, farm.getCancelDate());
        values.put(FarmModel.Column.CREATE_BY, farm.getCreateBy());
        values.put(FarmModel.Column.CREATE_DATE, farm.getCreateDate());
        values.put(FarmModel.Column.EMAIL, farm.getEmail());
        values.put(FarmModel.Column.FARM_NAME, farm.getFarm_Name());
        values.put(FarmModel.Column.FARM_OWNER_TYPE_ID, farm.getFarm_Owner_Type_ID());
        values.put(FarmModel.Column.FARM_PROBLEM_DESC, farm.getFarm_Problem_Desc());
        values.put(FarmModel.Column.FARM_STANDARD_NUMBER, farm.getFarm_Standard_Number());
        values.put(FarmModel.Column.FARMER_ID, farm.getFarmer_ID());
        values.put(FarmModel.Column.HOME_NO, farm.getHomeNo());
        values.put(FarmModel.Column.IS_CANCEL, farm.getIsCancel());
        values.put(FarmModel.Column.LATITUDE, farm.getLatitude());
        values.put(FarmModel.Column.LONGITUDE, farm.getLongitude());
        values.put(FarmModel.Column.MOBILE, farm.getMobile());
        values.put(FarmModel.Column.ORDER_INDEX, farm.getOrderIndex());
        values.put(FarmModel.Column.PHONE, farm.getPhone());
        values.put(FarmModel.Column.POSTCODE, farm.getPostCode());
        values.put(FarmModel.Column.PROVINCE_ID, farm.getProvince_ID());
        values.put(FarmModel.Column.REVENUE_OF_LIVESTOCK, farm.getRevenueOfLivestock());
        values.put(FarmModel.Column.ROAD, farm.getRoad());
        values.put(FarmModel.Column.SOI, farm.getSoi());
        values.put(FarmModel.Column.STANDARD_ID, farm.getStandard_ID());
        values.put(FarmModel.Column.STATUS_ID, farm.getStatus_ID());
        values.put(FarmModel.Column.SUPPORT_STANDARD_ID, farm.getSupport_Standard_ID());
        values.put(FarmModel.Column.TAMBOL_ID, farm.getTambol_ID());
        values.put(FarmModel.Column.UPDATE_BY, farm.getUpdateBy());
        values.put(FarmModel.Column.UPDATE_DATE, farm.getUpdateDate());
        values.put(FarmModel.Column.VILLAGE, farm.getVillage());
        values.put(FarmModel.Column.VILLAGE_ID, farm.getVillage_ID());
        values.put(FarmModel.Column.WORKER_ID, farm.getWorker_ID());

        return values;

    }


}
