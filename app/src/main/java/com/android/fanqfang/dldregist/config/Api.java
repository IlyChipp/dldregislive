package com.android.fanqfang.dldregist.config;


import com.android.fanqfang.dldregist.model.AmphurMaster;
import com.android.fanqfang.dldregist.model.FarmModel;
import com.android.fanqfang.dldregist.model.FarmOwnerMaster;
import com.android.fanqfang.dldregist.model.FarmProblemMaster;
import com.android.fanqfang.dldregist.model.FarmerModel;
import com.android.fanqfang.dldregist.model.FarmerProblemMaster;
import com.android.fanqfang.dldregist.model.FarmerTypeMaster;
import com.android.fanqfang.dldregist.model.FeedKindMaster;
import com.android.fanqfang.dldregist.model.FeedModel;
import com.android.fanqfang.dldregist.model.HelpMaster;
import com.android.fanqfang.dldregist.model.HoldingLandMaster;
import com.android.fanqfang.dldregist.model.LivestockMaster;
import com.android.fanqfang.dldregist.model.LivestockModel;
import com.android.fanqfang.dldregist.model.LoginModel;
import com.android.fanqfang.dldregist.model.MainJobMaster;
import com.android.fanqfang.dldregist.model.PrefixMaster;
import com.android.fanqfang.dldregist.model.ProvinceMaster;
import com.android.fanqfang.dldregist.model.SecondJobMaster;
import com.android.fanqfang.dldregist.model.StandardMaster;
import com.android.fanqfang.dldregist.model.SupportStandardMaster;
import com.android.fanqfang.dldregist.model.TambolMaster;
import com.android.fanqfang.dldregist.model.VillageMaster;

import retrofit2.Call;
import retrofit2.http.POST;
import retrofit2.http.Query;

public interface Api {

//    @Multipart
//    @POST(ApplicationConfig.UPLOAD_API_URL)
//    Call<ResponseUploadFileItem> uploadFile(@Part MultipartBody.Part file1, @Query("fileType") String fileType, @Query("readGroups") ArrayList<String> allowedRoles, @Header("token") String token);
//

    @POST("farmerApi/getFarmerData/post")
    Call<FarmerModel> loginPID(@Query("Pid") String Pid);

    @POST("loginApi/post")
    Call<LoginModel> login(@Query("userEmail") String userEmail, @Query("password") String password);

    @POST("amphurMasterData/getList/post")
    Call<AmphurMaster> getListAmphur();

    @POST("prefixMasterData/getList/post")
    Call<PrefixMaster> getListPrefix();

    @POST("provinceMasterData/getList/post")
    Call<ProvinceMaster> getListProvince();

    @POST("tambolMasterData/getList/post")
    Call<TambolMaster> getListTambol();

    @POST("villageMasterData/getList/post")
    Call<VillageMaster> getListVillage();

    @POST("farmApi/getfarmById/post")
    Call<FarmModel> getFarm(@Query("farmId") String farmId);

    @POST("farmOwnerTypeMasterData/getList/post")
    Call<FarmOwnerMaster> getFarmOwner();

    @POST("farmProblemMasterData/getList/post")
    Call<FarmProblemMaster> getFarmProblem();

    @POST("farmerProblemMasterData/getList/post")
    Call<FarmerProblemMaster> getFarmerProblem();

    @POST("farmerTypeMasterData/getList/post")
    Call<FarmerTypeMaster> getFarmerType();

    @POST("feedApi/getList/post")
    Call<FeedModel> getFeed(@Query("farmId") String farmId);

    @POST("feedKindMasterData/getList/post")
    Call<FeedKindMaster> getFeedKind();

    @POST("governmentHelpMasterData/getList/post")
    Call<HelpMaster> getHelp();

    @POST("holdingLandMasterData/getList/post")
    Call<HoldingLandMaster> getHoldingLand();

    @POST("farmApi/getfarmList/post")
    Call<FarmModel> getListFarm(@Query("farmerId") String farmerId);

    @POST("mainJobMasterData/getList/post")
    Call<MainJobMaster> getListMainJob();

    @POST("secondJobMasterData/getList/post")
    Call<SecondJobMaster> getListSecondJob();

    @POST("livestockMasterData/getList/post")
    Call<LivestockMaster> getListLivestock();

    @POST("livestockApi/getList/post")
    Call<LivestockModel> getLivestock();

    @POST("standardMasterData/getList/post")
    Call<StandardMaster> getStandard();

    @POST("supportStandardMasterData/getList/post")
    Call<SupportStandardMaster> getSupportStandard();








}


