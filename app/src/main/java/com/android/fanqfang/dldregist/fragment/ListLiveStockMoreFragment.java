package com.android.fanqfang.dldregist.fragment;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.android.fanqfang.dldregist.R;

import butterknife.ButterKnife;

public class ListLiveStockMoreFragment extends Fragment {

    private LinearLayout llAddAnimal;
    private LinearLayout btnAddAnimal;


    public ListLiveStockMoreFragment() {
        super();
    }

    public static ListLiveStockMoreFragment newInstance() {
        ListLiveStockMoreFragment fragment = new ListLiveStockMoreFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public View onCreateView(final LayoutInflater inflater, final ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_list_livstock_more, container, false);
//        View toAdd = inflater.inflate(R.layout.item_list_livestock_more, container, false);
        ButterKnife.inject(this, rootView);
        initInstances(rootView);

        btnAddAnimal.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                View toAdd = inflater.inflate(R.layout.item_list_livestock_more, container, false);
                llAddAnimal.addView(toAdd);
            }
        });

        return rootView;
    }


    private void initInstances(View rootView) {
        // Init 'View' instance(s) with rootView.findViewById here
        btnAddAnimal = (LinearLayout) rootView.findViewById(R.id.btnAddAnimal);
        llAddAnimal = (LinearLayout) rootView.findViewById(R.id.llAddAnimal);


    }


    @Override
    public void onStart() {
        super.onStart();
    }

    @Override
    public void onStop() {
        super.onStop();
    }

    /*
     * Save Instance State Here
     */
    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        // Save Instance State here
    }

    /*
     * Restore Instance State Here
     */
    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        if (savedInstanceState != null) {
            // Restore Instance State here
        }
    }

}
