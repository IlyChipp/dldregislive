package com.android.fanqfang.dldregist.fragment;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Spinner;

import com.android.fanqfang.dldregist.R;
import com.android.fanqfang.dldregist.database.DBHelper;
import com.android.fanqfang.dldregist.model.FarmerModel.FarmerEntity;
import com.android.fanqfang.dldregist.model.FarmerProblemMaster.FarmerProblemEntity;
import com.android.fanqfang.dldregist.model.HelpMaster.HelpEntity;
import com.android.fanqfang.dldregist.model.HoldingLandMaster.LandEntity;
import com.android.fanqfang.dldregist.model.MainJobMaster.JobEntity;
import com.android.fanqfang.dldregist.model.SecondJobMaster.SecondJobEntity;
import com.android.fanqfang.dldregist.widget.MyEditNoHLTextView;
import com.android.fanqfang.dldregist.widget.MySpinnerAdapter;

import java.util.ArrayList;

import butterknife.ButterKnife;
import butterknife.OnClick;


public class JobTypeFragment extends Fragment {

    private static final String CLASS_NAME = "JobTypeFragment";
    private Spinner spnJob;
    private Spinner spnSecondJob;
    private Spinner spnLand;
    private Spinner spnMachine;
    private MyEditNoHLTextView txtRevenue;
    private MyEditNoHLTextView txtDebtAmount;
    private MyEditNoHLTextView txtDebtAmountIn;
    private MyEditNoHLTextView txtDebtAmountOut;

    ArrayList<String> listJobs = new ArrayList<>();
    ArrayList<String> listSecondJobs = new ArrayList<>();
    ArrayList<String> listLands = new ArrayList<>();
    ArrayList<String> listMachines = new ArrayList<>();
    ArrayList<String> listHelps = new ArrayList<>();
    ArrayList<String> listProblems = new ArrayList<>();

    ArrayList<JobEntity> jobs = new ArrayList<>();
    ArrayList<SecondJobEntity> secondJobs = new ArrayList<>();
    ArrayList<LandEntity> lands = new ArrayList<>();
    ArrayList<HelpEntity> helps = new ArrayList<>();
    ArrayList<FarmerProblemEntity> farmerProblems = new ArrayList<>();


    DBHelper DBHelper;
    private Spinner spnProblem;
    private Spinner spnHelp;

    private FarmerEntity mFarmer;

    public JobTypeFragment() {
        super();
    }

    public static JobTypeFragment newInstance() {
        JobTypeFragment fragment = new JobTypeFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_job_type, container, false);

        initInstances(rootView);
        ButterKnife.inject(this, rootView);
        DBHelper = new DBHelper(getContext());
        mFarmer = DBHelper.getInfoFarmer();
        setCategory();

        getListMainJob();
        getListSecondJob();
        getListLand();
        getListHelp();
        getListFarmerProblem();

        return rootView;
    }

    private void initInstances(View rootView) {
        // Init 'View' instance(s) with rootView.findViewById here
        spnJob = (Spinner) rootView.findViewById(R.id.spnJob);
        spnSecondJob = (Spinner) rootView.findViewById(R.id.spnSecondJob);
        spnLand = (Spinner) rootView.findViewById(R.id.spnArea);
        spnHelp = (Spinner) rootView.findViewById(R.id.spnHelp);
        spnProblem = (Spinner) rootView.findViewById(R.id.spnProblem);
        spnMachine = (Spinner) rootView.findViewById(R.id.spnMachine);
        txtRevenue = (MyEditNoHLTextView) rootView.findViewById(R.id.txtSalary);
        txtDebtAmount = (MyEditNoHLTextView) rootView.findViewById(R.id.txtDebtAmount);
        txtDebtAmountIn = (MyEditNoHLTextView) rootView.findViewById(R.id.txtDebtAmountIn);
        txtDebtAmountOut = (MyEditNoHLTextView) rootView.findViewById(R.id.txtDebtAmountOut);
    }

    public void setCategory() {
        mFarmer = DBHelper.getInfoFarmer();

        txtRevenue.setText(mFarmer.getRevenue());
        txtDebtAmount.setText(mFarmer.getDebtAmount()+"");
        txtDebtAmountIn.setText(mFarmer.getDebtsAmountIn()+"");
        txtDebtAmountOut.setText(mFarmer.getDebtsAmountOut()+"");
    }

    @Override
    public void onStart() {
        super.onStart();
    }

    @Override
    public void onStop() {
        super.onStop();
    }

    /*
     * Save Instance State Here
     */
    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        // Save Instance State here
//        outState.putParcelable(CLASS_NAME, Parcels.wrap());
    }

    /*
     * Restore Instance State Here
     */
    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        if (savedInstanceState != null) {
            // Restore Instance State here
        }
    }

    @OnClick(R.id.btnSubmit)
    public void clickBtnSubmit(){
        mFarmer.setRevenue(txtRevenue.getText().toString());
        mFarmer.setDebtAmount(Integer.parseInt(txtDebtAmount.getText().toString()));
        mFarmer.setDebtsAmountIn(txtDebtAmountIn.getText().toString());
        mFarmer.setDebtsAmountOut(txtDebtAmountOut.getText().toString());

        DBHelper.updateFarmer(mFarmer, mFarmer.getFarmer_ID());
    }

    public void getListMainJob() {
        int selectIndex = -1;
        jobs = DBHelper.getMainJob();

        for (int i = 0; i < jobs.size(); i++) {
            listJobs.add(jobs.get(i).getMain_Job_Name());
            if (jobs.get(i).getMain_Job_ID() == mFarmer.getMain_Job_ID()) {
                selectIndex = i;
            }
        }

        MySpinnerAdapter adapter = new MySpinnerAdapter(
                getContext(),
                android.R.layout.simple_dropdown_item_1line,
                listJobs);
        spnJob.setAdapter(adapter);

        spnJob.setSelection(selectIndex);

        spnJob.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                mFarmer.setMain_Job_ID(jobs.get(position).getMain_Job_ID());
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    public void getListSecondJob() {
        int selectIndex = -1;
        secondJobs = DBHelper.getSecondJob();

        for (int i = 0; i < secondJobs.size(); i++) {
            listSecondJobs.add(secondJobs.get(i).getSecond_Job_Name());
            if (secondJobs.get(i).getSecond_Job_ID() == mFarmer.getSecond_Job_ID()) {
                selectIndex = i;
            }
        }

        MySpinnerAdapter adapter = new MySpinnerAdapter(
                getContext(),
                android.R.layout.simple_dropdown_item_1line,
                listSecondJobs);
        spnSecondJob.setAdapter(adapter);

        spnSecondJob.setSelection(selectIndex);

        spnSecondJob.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                mFarmer.setSecond_Job_ID(secondJobs.get(position).getSecond_Job_ID());
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    public void getListLand() {
        int selectIndex = -1;
        lands = DBHelper.getLand();

        for (int i = 0; i < lands.size(); i++) {
            listLands.add(lands.get(i).getHolding_Land_Name());
            if (lands.get(i).getHolding_Land_ID() == mFarmer.getHolding_Land_ID()) {
                selectIndex = i;
            }
        }

        MySpinnerAdapter adapter = new MySpinnerAdapter(
                getContext(),
                android.R.layout.simple_dropdown_item_1line,
                listLands);
        spnLand.setAdapter(adapter);

        spnLand.setSelection(selectIndex);

        spnLand.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                mFarmer.setHolding_Land_ID(lands.get(position).getHolding_Land_ID());
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    public void getListHelp() {
        int selectIndex = -1;
        helps = DBHelper.getHelp();

        for (int i = 0; i < helps.size(); i++) {
            listHelps.add(helps.get(i).getGovernment_Help_Name());
            if (helps.get(i).getGovernment_Help_ID() == mFarmer.getGovernment_Help_ID()) {
                selectIndex = i;
            }
        }

        MySpinnerAdapter adapter = new MySpinnerAdapter(
                getContext(),
                android.R.layout.simple_dropdown_item_1line,
                listHelps);
        spnHelp.setAdapter(adapter);

        spnHelp.setSelection(selectIndex);

        spnHelp.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                mFarmer.setGovernment_Help_ID(helps.get(position).getGovernment_Help_ID());
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    public void getListFarmerProblem() {
        int selectIndex = -1;
        farmerProblems = DBHelper.getFarmerProblem();

        for (int i = 0; i < farmerProblems.size(); i++) {
            listProblems.add(farmerProblems.get(i).getFarmer_Problem_Name());
//            if (farmerProblems.get(i).getFarmer_Problem_ID() == Integer.parseInt(mFarmer.getFarmer_Problem_ID())) {
//                selectIndex = i;
//            }
        }

        MySpinnerAdapter adapter = new MySpinnerAdapter(
                getContext(),
                android.R.layout.simple_dropdown_item_1line,
                listProblems);
        spnProblem.setAdapter(adapter);

//        spnProblem.setSelection(selectIndex);

        spnProblem.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

}
