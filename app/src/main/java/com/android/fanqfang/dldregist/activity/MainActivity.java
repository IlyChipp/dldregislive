package com.android.fanqfang.dldregist.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;

import com.android.fanqfang.dldregist.R;
import com.android.fanqfang.dldregist.fragment.MainFragment;
import com.android.fanqfang.dldregist.view.CircleShape;
import com.android.fanqfang.dldregist.widget.MyNoHBTextView;

import butterknife.ButterKnife;

public class MainActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {
    private static final String CLASS_NAME = "MainAvtivity";
    private static final String TAG_HOME_FRAGMENT = "tag_home_fragment";

    DrawerLayout drawer;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        CoordinatorLayout appBar = (CoordinatorLayout) findViewById(R.id.app_bar_main);

        CircleShape.RoundedImageView imgProfile = (CircleShape.RoundedImageView) appBar.findViewById(R.id.imgProfileBar);
        MyNoHBTextView txtNav = (MyNoHBTextView) appBar.findViewById(R.id.txtNav);

        imgProfile.setImageResource(R.drawable.test);



        ButterKnife.inject(this);


        initInstance();


        if (savedInstanceState == null) {
            getSupportFragmentManager().beginTransaction()
                    .replace(R.id.contentContainer, MainFragment.newInstance(), TAG_HOME_FRAGMENT)
                    .commit();
        }

    }

    private void initInstance() {

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        toolbar.setTitle("");

        drawer = (DrawerLayout) findViewById(R.id.drawerLayout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.open_drawer, R.string.close_drawer);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawerLayout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }


    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_logout) {

        } else if (id == R.id.nav_findpid) {


        } else if (id == R.id.nav_updatedata) {

        } else if (id == R.id.nav_history) {

        } else if (id == R.id.nav_aboutus) {

            Intent intent = new Intent(MainActivity.this, AboutUsActivity.class);
            startActivity(intent);

        } else if (id == R.id.nav_update) {

        }
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

}
