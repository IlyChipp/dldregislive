package com.android.fanqfang.dldregist.manager;

import android.content.Context;
import android.content.SharedPreferences;

public class MyPreferenceManager {

    public static final String MyPREFERENCES = "dldregislive";
    private SharedPreferences sharedpreferences;

    private final String userProfile = "userProfile";
    private final String PROVINCE = "PROVINCE";
    private final String AMPHUR = "AMPHUR";
    private final String TAMBOL = "TAMBOL";
    private final String VILLAGE = "VILLAGE";

    private final String API = "API";

    public MyPreferenceManager(Context context) {
        sharedpreferences = context.getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE);
    }


    public static String getMyPREFERENCES() {
        return MyPREFERENCES;
    }

    public SharedPreferences getSharedpreferences() {
        return sharedpreferences;
    }

    public void setSharedpreferences(SharedPreferences sharedpreferences) {
        this.sharedpreferences = sharedpreferences;
    }

//    public void setProvince(String jsonObject){
//        SharedPreferences.Editor editor = sharedpreferences.edit();
//        editor.putString(PROVINCE, jsonObject);
//        editor.commit();
//    }
//    public void setAmphur(String jsonObject){
//        SharedPreferences.Editor editor = sharedpreferences.edit();
//        editor.putString(AMPHUR, jsonObject);
//        editor.commit();
//    }
//    public void setTambol(String jsonObject){
//        SharedPreferences.Editor editor = sharedpreferences.edit();
//        editor.putString(TAMBOL, jsonObject);
//        editor.commit();
//    }
//    public void setVillage(String jsonObject){
//        SharedPreferences.Editor editor = sharedpreferences.edit();
//        editor.putString(VILLAGE, jsonObject);
//        editor.commit();
//    }
//
//    public String getProvince(){
//        if (sharedpreferences.contains(PROVINCE)){
//            return sharedpreferences.getString(PROVINCE, "0");
//        }
//        return "0";
//    }
//
//    public String getAmphur(){
//        if (sharedpreferences.contains(AMPHUR)){
//            return sharedpreferences.getString(AMPHUR, "0");
//        }
//        return "0";
//    }
//
//    public String getTambol(){
//        if (sharedpreferences.contains(TAMBOL)){
//            return sharedpreferences.getString(TAMBOL, "0");
//        }
//        return "0";
//    }
//    public String getVillage(){
//        if (sharedpreferences.contains(VILLAGE)){
//            return sharedpreferences.getString(VILLAGE, "0");
//        }
//        return "0";
//    }


    public String getUserProfile(){
        if (sharedpreferences.contains(userProfile)) {
            return sharedpreferences.getString(userProfile, "0");
        }
        return "0";
    }

    public void setUserProfile(String jsonObject){
        SharedPreferences.Editor editor = sharedpreferences.edit();
        // editor.remove(userProfile);
        editor.putString(userProfile, jsonObject);
        editor.commit();
    }


}
