package com.android.fanqfang.dldregist.fragment;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.android.fanqfang.dldregist.R;
import com.android.fanqfang.dldregist.widget.MyNoHBTextView;

import java.util.ArrayList;

import butterknife.ButterKnife;

public class FeedFragment extends Fragment {

    private LinearLayout llAddSell;
    private LinearLayout llAddUse;
    private LinearLayout btnAddSell;
    private LinearLayout btnAddUse;
    ArrayList<View> listFeed = new ArrayList<>();
    private MyNoHBTextView btnSubmit;

    public FeedFragment() {
        super();
    }

    public static FeedFragment newInstance() {
        FeedFragment fragment = new FeedFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public View onCreateView(final LayoutInflater inflater, final ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_feed, container, false);
        ButterKnife.inject(this, rootView);
        initInstances(rootView);
        View addSell = inflater.inflate(R.layout.item_seed, container, false);
        llAddSell.addView(addSell);
        View addUse = inflater.inflate(R.layout.item_seed, container, false);
        llAddUse.addView(addUse);

        btnAddSell.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                View addSell = inflater.inflate(R.layout.item_seed, container, false);
                listFeed.add(addSell);
                llAddSell.addView(addSell);
            }
        });

        btnAddUse.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                View addUse = inflater.inflate(R.layout.item_seed, container, false);
                llAddUse.addView(addUse);
            }
        });


        return rootView;
    }
//
//    @OnClick(R.id.btnSubmit)
//    private void clickSubmit(){
//
//    }


    private void initInstances(View rootView) {
        // Init 'View' instance(s) with rootView.findViewById here'
        llAddSell = (LinearLayout) rootView.findViewById(R.id.llAddSell);
        llAddUse = (LinearLayout) rootView.findViewById(R.id.llAddUse);
        btnAddSell = (LinearLayout) rootView.findViewById(R.id.btnAddSell);
        btnAddUse = (LinearLayout) rootView.findViewById(R.id.btnAddUse);
        btnSubmit = (MyNoHBTextView) rootView.findViewById(R.id.btnSubmit);
    }


    @Override
    public void onStart() {
        super.onStart();
    }

    @Override
    public void onStop() {
        super.onStop();
    }

    /*
     * Save Instance State Here
     */
    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        // Save Instance State here
    }

    /*
     * Restore Instance State Here
     */
    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        if (savedInstanceState != null) {
            // Restore Instance State here
        }
    }
}
