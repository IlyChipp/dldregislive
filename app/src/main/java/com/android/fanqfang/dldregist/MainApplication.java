package com.android.fanqfang.dldregist;

import android.app.Application;

import com.android.fanqfang.dldregist.manager.Contextor;

/**
 * Created by fanqfang on 9/16/2017 AD.
 */

public class MainApplication extends Application {

    @Override
    public void onCreate() {
        super.onCreate();

        Contextor.getInstance().init(getApplicationContext());
    }

    @Override
    public void onTerminate() {
        super.onTerminate();
    }
}
