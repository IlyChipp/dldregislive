package com.android.fanqfang.dldregist.model;

import android.provider.BaseColumns;

import org.parceler.Parcel;

import java.util.ArrayList;

/**
 * Created by fanqfang on 9/28/2017 AD.
 */

@Parcel
public class FeedModel {
    //DBHelper
    public static final String TABLE = "feed";

    String responseCode;
    String responseMessage;
    ArrayList<FeedEntity> data;

    public FeedModel() { }

    public String getResponseCode() {
        return responseCode;
    }

    public void setResponseCode(String responseCode) {
        this.responseCode = responseCode;
    }

    public String getResponseMessage() {
        return responseMessage;
    }

    public void setResponseMessage(String responseMessage) {
        this.responseMessage = responseMessage;
    }

    public ArrayList<FeedEntity> getData() {
        return data;
    }

    public void setData(ArrayList<FeedEntity> data) {
        this.data = data;
    }

    @Parcel
    public static class FeedEntity{
        int feed_ID;
        String createBy;
        String createDate;
        int farm_ID;
        int feed_Area_Ngan;
        int feed_Area_Rai;
        int feed_Kind_ID;
        String feed_Kind_Other;
        String feed_Type_ID;
        String feed_Type_Other;
        String feed_Water_Type_ID;
        String orderIndex;
        int status_ID;
        String updateBy;
        String updateDate;

        public FeedEntity(){ }

        public int getFeed_ID() {
            return feed_ID;
        }

        public void setFeed_ID(int feed_ID) {
            this.feed_ID = feed_ID;
        }

        public String getCreateBy() {
            return createBy;
        }

        public void setCreateBy(String createBy) {
            this.createBy = createBy;
        }

        public String getCreateDate() {
            return createDate;
        }

        public void setCreateDate(String createDate) {
            this.createDate = createDate;
        }

        public int getFarm_ID() {
            return farm_ID;
        }

        public void setFarm_ID(int farm_ID) {
            this.farm_ID = farm_ID;
        }

        public int getFeed_Area_Ngan() {
            return feed_Area_Ngan;
        }

        public void setFeed_Area_Ngan(int feed_Area_Ngan) {
            this.feed_Area_Ngan = feed_Area_Ngan;
        }

        public int getFeed_Area_Rai() {
            return feed_Area_Rai;
        }

        public void setFeed_Area_Rai(int feed_Area_Rai) {
            this.feed_Area_Rai = feed_Area_Rai;
        }

        public int getFeed_Kind_ID() {
            return feed_Kind_ID;
        }

        public void setFeed_Kind_ID(int feed_Kind_ID) {
            this.feed_Kind_ID = feed_Kind_ID;
        }

        public String getFeed_Kind_Other() {
            return feed_Kind_Other;
        }

        public void setFeed_Kind_Other(String feed_Kind_Other) {
            this.feed_Kind_Other = feed_Kind_Other;
        }

        public String getFeed_Type_ID() {
            return feed_Type_ID;
        }

        public void setFeed_Type_ID(String feed_Type_ID) {
            this.feed_Type_ID = feed_Type_ID;
        }

        public String getFeed_Type_Other() {
            return feed_Type_Other;
        }

        public void setFeed_Type_Other(String feed_Type_Other) {
            this.feed_Type_Other = feed_Type_Other;
        }

        public String getFeed_Water_Type_ID() {
            return feed_Water_Type_ID;
        }

        public void setFeed_Water_Type_ID(String feed_Water_Type_ID) {
            this.feed_Water_Type_ID = feed_Water_Type_ID;
        }

        public String getOrderIndex() {
            return orderIndex;
        }

        public void setOrderIndex(String orderIndex) {
            this.orderIndex = orderIndex;
        }

        public int getStatus_ID() {
            return status_ID;
        }

        public void setStatus_ID(int status_ID) {
            this.status_ID = status_ID;
        }

        public String getUpdateBy() {
            return updateBy;
        }

        public void setUpdateBy(String updateBy) {
            this.updateBy = updateBy;
        }

        public String getUpdateDate() {
            return updateDate;
        }

        public void setUpdateDate(String updateDate) {
            this.updateDate = updateDate;
        }
    }

    public class Column{
        public static final String ID = BaseColumns._ID;
        public static final String FEED_ID = "feed_ID";
        public static final String CREATE_BY = "createBy";
        public static final String CREATE_DATE = "createDate";
        public static final String FARM_ID = "farm_ID";
        public static final String FEED_AREA_NGAN = "feed_Area_Ngan";
        public static final String FEED_AREA_RAI = "feed_Area_Rai";
        public static final String FEED_KIND_ID = "feed_Kind_ID";
        public static final String FEED_KIND_OTHER = "feed_Kind_Other";
        public static final String FEED_TYPE_ID = "feed_Type_ID";
        public static final String FEED_TYPE_OTHER = "feed_Type_Other";
        public static final String FEED_WATER_TYPE_ID = "feed_Water_Type_ID";
        public static final String ORDER_INDEX = "orderIndex";
        public static final String STATUS_ID = "status_ID";
        public static final String UPDATE_BY = "updateBy";
        public static final String UPDATE_DATE = "updateDate";
    }
}
