package com.android.fanqfang.dldregist.activity;

import android.os.Bundle;
import android.support.design.widget.CoordinatorLayout;
import android.support.v7.app.AppCompatActivity;
import android.view.Gravity;

import com.android.fanqfang.dldregist.R;
import com.android.fanqfang.dldregist.fragment.FarmActivityFragment;
import com.android.fanqfang.dldregist.model.FarmModel.FarmEntity;
import com.android.fanqfang.dldregist.widget.MyNoHBTextView;

import org.parceler.Parcels;

import butterknife.ButterKnife;

public class FarmActivity extends AppCompatActivity {

    final private static String TAG_FARM = "tag_farm";
    final private static String FARMINFO = "farmInfo";
    private MyNoHBTextView txtNav;
    private CoordinatorLayout appBar;

    FarmEntity farm;

    public FarmEntity getFarm() {
        return farm;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_farm);
        ButterKnife.inject(this);

        farm = Parcels.unwrap(getIntent().getParcelableExtra(FARMINFO));

        initInstance();
        setTxtNav();

    }

    private void initInstance() {
        appBar = (CoordinatorLayout) findViewById(R.id.app_bar_main);
        txtNav = (MyNoHBTextView) appBar.findViewById(R.id.txtNav) ;
    }

    public void getActivityFragment() {

        FarmActivityFragment farmActivity = new FarmActivityFragment();
        getSupportFragmentManager().beginTransaction()
                .replace(R.id.contentContainer, farmActivity, TAG_FARM)
                .commit();
    }

    private void setTxtNav(){
        if (farm == null){
            getActivityFragment();
        }else {
            txtNav.setText(farm.getFarm_Name());
            txtNav.setGravity(Gravity.CENTER);
            getActivityFragment();
        }
    }


}
