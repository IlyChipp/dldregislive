package com.android.fanqfang.dldregist.util;

//import com.zti.android.siiscan.activity.PatientInfoActivity;

import com.android.fanqfang.dldregist.config.Api;
import com.android.fanqfang.dldregist.config.ApplicationConfig;
import com.android.fanqfang.dldregist.manager.MyPreferenceManager;
import com.android.fanqfang.dldregist.model.FarmerModel;
import com.google.gson.Gson;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class AppUtils {

    private static final String CLASS_NAME = "Utils";

//    private static final int REQUEST_PERMISSION = 1;
//
//    public static void showAlertDialog(String title,String message,Activity activity){
//        new AlertDialog.Builder(activity)
//                .setTitle(title)
//                .setMessage(message)
//                .setPositiveButton(activity.getResources().getString(R.string.confirm), new DialogInterface.OnClickListener() {
//                    public void onClick(DialogInterface dialog, int which) {
//
//                    }
//                })
//                .setIcon(android.R.drawable.ic_dialog_alert)
//                .show();
//    }
//
//    public static void showAlertDialogOk(String title,String message,Activity activity){
//        new AlertDialog.Builder(activity)
//                .setTitle(title)
//                .setMessage(message)
//                .setPositiveButton(activity.getResources().getString(R.string.ok), new DialogInterface.OnClickListener() {
//                    public void onClick(DialogInterface dialog, int which) {
//
//                    }
//                })
//                .setIcon(android.R.drawable.ic_dialog_alert)
//                .show();
//    }


    public static void setUserProfile(FarmerModel.FarmerEntity pidModel, MyPreferenceManager myPreferenceManager) {
        Gson gson = new Gson();
//        if(fbModel != null && fbModel.size() > 0) {
//            String json = gson.toJson(fbModel.get(0));
//            myPreferenceManager.setUserProfile(json);
//        }
        String json = gson.toJson(pidModel);
        myPreferenceManager.setUserProfile(json);
    }

//    public static FarmerEntity getUserProfile(Context context) {
//        MyPreferenceManager myPreferenceManager = new MyPreferenceManager(context);
//        Gson gson = new Gson();
//        String json = myPreferenceManager.getUserProfile();
//        return gson.fromJson(json, FarmerEntity.class);
//    }

//    public static void setProvince(ArrayList<ProvinceEntity> pvModel, MyPreferenceManager myPreferenceManager) {
//        Gson gson = new Gson();
//        String json;
//        if (pvModel != null && pvModel.size() > 0) {
//            json = gson.toJson(pvModel);
//            myPreferenceManager.setProvince(json);
//        }
//    }
//    public static void setAmphur(ArrayList<AmphurEntity> amModel, MyPreferenceManager myPreferenceManager){
//        Gson gson = new Gson();
//        String json;
//        if(amModel != null && amModel.size() > 0){
//            json = gson.toJson(amModel);
//            myPreferenceManager.setAmphur(json);
//        }
//    }
//    public static void setTambol(ArrayList<TambolEntity> tmModel, MyPreferenceManager myPreferenceManager){
//        Gson gson = new Gson();
//        String json;
//        if(tmModel != null && tmModel.size() > 0){
//            json = gson.toJson(tmModel);
//            myPreferenceManager.setTambol(json);
//        }
//    }
//    public static void setVillage(ArrayList<VillageEntity> vlModel, MyPreferenceManager myPreferenceManager){
//        Gson gson = new Gson();
//        String json;
//        if (vlModel != null && vlModel.size() > 0){
//            json = gson.toJson(vlModel);
//            myPreferenceManager.setVillage(json);
//        }
//    }


    public static Api getApiService() {

//        Gson gson = new GsonBuilder()
//                .setDateFormat("yyy-MM-dd'T'HH:mm:ssZ")
//                .create();

        Retrofit client = new Retrofit.Builder()
                .baseUrl(ApplicationConfig.getApiBaseUrl())
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        return client.create(Api.class);
    }

}
