package com.android.fanqfang.dldregist.activity;

import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;

import com.android.fanqfang.dldregist.R;
import com.android.fanqfang.dldregist.fragment.FarmerCorporateFragment;
import com.android.fanqfang.dldregist.fragment.FarmerDepartmentFragment;
import com.android.fanqfang.dldregist.fragment.FarmerGeneralFragment;

import java.util.ArrayList;
import java.util.List;


public class FindFarmerActivity extends AppCompatActivity {

    private TabLayout tabLayout;
    private ViewPager viewPager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_find_farmer);

        init();
    }

    private void init() {
        // Init 'View' instance(s) with rootView.findViewById here
        viewPager = (ViewPager) findViewById(R.id.viewPager);
        tabLayout = (TabLayout) findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(viewPager);
        tabLayout.setBackgroundColor(getResources().getColor(R.color.slidingTab));

        setupViewPager(viewPager);
    }

    private void setupViewPager(ViewPager viewPager) {
        FindFarmerActivity.ViewPagerAdapter adapter = new ViewPagerAdapter(this.getSupportFragmentManager());

        adapter.addFragment(new FarmerGeneralFragment(), getString(R.string.general));
        adapter.addFragment(new FarmerCorporateFragment(), getString(R.string.corporate));
        adapter.addFragment(new FarmerDepartmentFragment(), getString(R.string.department));

        viewPager.setAdapter(adapter);
    }

    class ViewPagerAdapter extends FragmentPagerAdapter {
        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        public ViewPagerAdapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        public void addFragment(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }
    }

}
