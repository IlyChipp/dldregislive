package com.android.fanqfang.dldregist.activity;

import android.os.Bundle;
import android.support.design.widget.CoordinatorLayout;
import android.support.v7.app.AppCompatActivity;

import com.android.fanqfang.dldregist.R;
import com.android.fanqfang.dldregist.widget.MyNoHBTextView;

public class AboutUsActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_about_us);

        CoordinatorLayout appBar = (CoordinatorLayout) findViewById(R.id.app_bar_main);
        MyNoHBTextView txtNav = (MyNoHBTextView) appBar.findViewById(R.id.txtNav) ;

        txtNav.setText(getString(R.string.aboutus));
    }
}
