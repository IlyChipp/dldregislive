package com.android.fanqfang.dldregist.fragment;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.android.fanqfang.dldregist.R;
import com.android.fanqfang.dldregist.widget.MyEditNoHLTextView;
import com.android.fanqfang.dldregist.widget.MyNoHBTextView;
import com.android.fanqfang.dldregist.widget.MyNoHLTextView;

public class ListLiveStockFragment extends Fragment {

    private ListTypeAdapter listTypeAdapter;
    private RecyclerView recyclerListType;

    public ListLiveStockFragment() {
        super();
    }

    public static ListLiveStockFragment newInstance() {
        ListLiveStockFragment fragment = new ListLiveStockFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_list_livestock, container, false);
        initInstances(rootView);
        getListType();
        return rootView;
    }

    private void initInstances(View rootView) {
        // Init 'View' instance(s) with rootView.findViewById here
        recyclerListType = (RecyclerView) rootView.findViewById(R.id.recyclerLivestock);
    }

    @Override
    public void onStart() {
        super.onStart();
    }

    @Override
    public void onStop() {
        super.onStop();
    }

    /*
     * Save Instance State Here
     */
    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        // Save Instance State here
    }
    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        if (savedInstanceState != null) {
            // Restore Instance State here
        }
    }
    public void getListType() {

        recyclerListType.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false));
        listTypeAdapter = new ListTypeAdapter();
        recyclerListType.setAdapter(listTypeAdapter);

    }


    public class ListTypeAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

        @Override
        public int getItemViewType(int position) {
            return position %2 * 2;
        }

        @Override
        public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            switch (viewType) {
                case 0:
                    View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_livestock, parent, false);

                    return new ListTypeHolder(v);
                case 2:
                    View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_list_livestock, parent, false);
                    return new ListLiveStockHolder(view);
            }return null;
        }

        @Override
        public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
            switch (holder.getItemViewType()){
                case 0:
                    ListTypeHolder listTypeHolder = (ListTypeHolder)holder;
                    listTypeHolder.mlistAnimalType.setText("Type " + position);

                    break;
                case 2:
                    ListLiveStockHolder listLiveStockHolder = (ListLiveStockHolder) holder;
                    listLiveStockHolder.mtxtSubType.setText("โคเนื้อ" + position);
                    listLiveStockHolder.mtxtPrice.setText("20000");
                    listLiveStockHolder.mtxtQuantity.setText("20");

                    break;
            }
        }

        @Override
        public int getItemCount() {
            return 10;
        }

        public class ListTypeHolder extends RecyclerView.ViewHolder {


            private final MyNoHBTextView mlistAnimalType;
            private final RecyclerView recyclerLiveStock;

            public ListTypeHolder(View itemView) {
                super(itemView);

                mlistAnimalType = (MyNoHBTextView) itemView.findViewById(R.id.listAnimalType);
                recyclerLiveStock = (RecyclerView) itemView.findViewById(R.id.recyclerLivestock);
            }
        }

        public class ListLiveStockHolder extends RecyclerView.ViewHolder {

            private final MyNoHLTextView mtxtSubType;
            private final MyEditNoHLTextView mtxtQuantity;
            private final MyEditNoHLTextView mtxtPrice;

            public ListLiveStockHolder(View itemView) {
                super(itemView);

                mtxtSubType = (MyNoHLTextView) itemView.findViewById(R.id.txtSubType);
                mtxtQuantity = (MyEditNoHLTextView) itemView.findViewById(R.id.txtQuantity);
                mtxtPrice = (MyEditNoHLTextView) itemView.findViewById(R.id.txtPrice);
            }
        }

    }
}
