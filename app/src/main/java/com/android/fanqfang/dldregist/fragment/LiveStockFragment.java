package com.android.fanqfang.dldregist.fragment;

import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.android.fanqfang.dldregist.R;
import com.android.fanqfang.dldregist.widget.MyNoHLTextView;

import butterknife.ButterKnife;
import butterknife.OnClick;

public class LiveStockFragment extends Fragment {

    private RecyclerView recyclerListType;
    private FrameLayout contentContainer;
    private LinearLayout mbtnMeat;
    private LinearLayout mbtnCow;
    private LinearLayout mbtnBuf;
    private LinearLayout mbtnPig;
    private LinearLayout mbtnChicken;
    private LinearLayout mbtnDuck;
    private LinearLayout mbtnGoat;
    private LinearLayout mbtnEtc;

    static final int BTN_MEAT = 0;
    static final int BTN_COW = 1;
    static final int BTN_BUF = 2;
    static final int BTN_PIG = 3;
    static final int BTN_CHICKEN = 4;
    static final int BTN_DUCK = 5;
    static final int BTN_GOAT = 6;
    static final int BTN_ETC = 7;
    private ImageView imgMeat;
    private MyNoHLTextView txtMeat;
    private ImageView imgCow;
    private ImageView imgBuf;
    private MyNoHLTextView txtBuf;
    private ImageView imgPig;
    private MyNoHLTextView txtPig;
    private ImageView imgChicken;
    private MyNoHLTextView txtChicken;
    private ImageView imgDuck;
    private MyNoHLTextView txtDuck;
    private ImageView imgGoat;
    private MyNoHLTextView txtGoat;
    private ImageView imgEtc;
    private MyNoHLTextView txtEtc;
    private MyNoHLTextView txtCow;

    public LiveStockFragment() {
        super();
    }

    public static LiveStockFragment newInstance() {
        LiveStockFragment fragment = new LiveStockFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_live_stock, container, false);
        ButterKnife.inject(this, rootView);
        initInstances(rootView);
        getListLivestock(BTN_MEAT,"MEAT");
        return rootView;
    }

    private void initInstances(View rootView) {
        // Init 'View' instance(s) with rootView.findViewById here

        contentContainer = (FrameLayout) rootView.findViewById(R.id.contentContainerListAnimal);
        mbtnMeat = (LinearLayout) rootView.findViewById(R.id.btnMeat);
        mbtnCow = (LinearLayout) rootView.findViewById(R.id.btnCow);
        mbtnBuf = (LinearLayout) rootView.findViewById(R.id.btnBuf);
        mbtnPig = (LinearLayout) rootView.findViewById(R.id.btnPig);
        mbtnChicken = (LinearLayout) rootView.findViewById(R.id.btnChicken);
        mbtnDuck = (LinearLayout) rootView.findViewById(R.id.btnDuck);
        mbtnGoat = (LinearLayout) rootView.findViewById(R.id.btnGoat);
        mbtnEtc = (LinearLayout) rootView.findViewById(R.id.btnEtc);

        imgMeat = (ImageView) rootView.findViewById(R.id.imgMeat);
        txtMeat = (MyNoHLTextView) rootView.findViewById(R.id.txtMeat);
        imgCow = (ImageView) rootView.findViewById(R.id.imgCow);
        txtCow = (MyNoHLTextView) rootView.findViewById(R.id.txtCow);
        imgBuf = (ImageView) rootView.findViewById(R.id.imgBuf);
        txtBuf = (MyNoHLTextView) rootView.findViewById(R.id.txtBuf);
        imgPig = (ImageView) rootView.findViewById(R.id.imgPig);
        txtPig = (MyNoHLTextView) rootView.findViewById(R.id.txtPig);
        imgChicken = (ImageView) rootView.findViewById(R.id.imgChicken);
        txtChicken = (MyNoHLTextView) rootView.findViewById(R.id.txtChicken);
        imgDuck = (ImageView) rootView.findViewById(R.id.imgDuck);
        txtDuck = (MyNoHLTextView) rootView.findViewById(R.id.txtDuck);
        imgGoat = (ImageView) rootView.findViewById(R.id.imgGoat);
        txtGoat = (MyNoHLTextView) rootView.findViewById(R.id.txtGoat);
        imgEtc = (ImageView) rootView.findViewById(R.id.imgEtc);
        txtEtc = (MyNoHLTextView) rootView.findViewById(R.id.txtEtc);
    }

    private void setMeatActive(){
        txtMeat.setTextColor(ContextCompat.getColor(getContext(), R.color.txt_blue2));
        txtMeat.setTextSize(TypedValue.COMPLEX_UNIT_SP, 16);
        txtMeat.setTypeface(txtMeat.getTypeface(), Typeface.BOLD);
    }
    private void setMeatPassive(){
        txtMeat.setTextColor(ContextCompat.getColor(getContext(), R.color.text_color));
        txtMeat.setTextSize(TypedValue.COMPLEX_UNIT_SP, 15);
        txtMeat.setTypeface(txtMeat.getTypeface(), Typeface.NORMAL);
    }
    private void setCowActive(){
        txtCow.setTextColor(ContextCompat.getColor(getContext(), R.color.txt_blue2));
        txtCow.setTextSize(TypedValue.COMPLEX_UNIT_SP, 16);
        txtCow.setTypeface(txtCow.getTypeface(), Typeface.BOLD);
    }
    private void setCowPassive(){
        txtCow.setTextColor(ContextCompat.getColor(getContext(), R.color.text_color));
        txtCow.setTextSize(TypedValue.COMPLEX_UNIT_SP, 15);
        txtCow.setTypeface(txtCow.getTypeface(), Typeface.NORMAL);
    }
    private void setBufActive(){
        txtBuf.setTextColor(ContextCompat.getColor(getContext(), R.color.txt_blue2));
        txtBuf.setTextSize(TypedValue.COMPLEX_UNIT_SP, 16);
        txtBuf.setTypeface(txtBuf.getTypeface(), Typeface.BOLD);
    }
    private void setBufPassive(){
        txtBuf.setTextColor(ContextCompat.getColor(getContext(), R.color.text_color));
        txtBuf.setTextSize(TypedValue.COMPLEX_UNIT_SP, 15);
        txtBuf.setTypeface(txtBuf.getTypeface(), Typeface.NORMAL);
    }
    private void setPigActive(){
        txtPig.setTextColor(ContextCompat.getColor(getContext(), R.color.txt_blue2));
        txtPig.setTextSize(TypedValue.COMPLEX_UNIT_SP, 16);
        txtPig.setTypeface(txtPig.getTypeface(), Typeface.BOLD);
    }
    private void setPigPassive(){
        txtPig.setTextColor(ContextCompat.getColor(getContext(), R.color.text_color));
        txtPig.setTextSize(TypedValue.COMPLEX_UNIT_SP, 15);
        txtPig.setTypeface(txtPig.getTypeface(), Typeface.NORMAL);
    }
    private void setChickActive() {
        txtChicken.setTextColor(ContextCompat.getColor(getContext(), R.color.txt_blue2));
        txtChicken.setTextSize(TypedValue.COMPLEX_UNIT_SP, 16);
        txtChicken.setTypeface(txtChicken.getTypeface(), Typeface.BOLD);
    }
    private void setChickPassive() {
        txtChicken.setTextColor(ContextCompat.getColor(getContext(), R.color.text_color));
        txtChicken.setTextSize(TypedValue.COMPLEX_UNIT_SP, 15);
        txtChicken.setTypeface(txtChicken.getTypeface(), Typeface.NORMAL);
    }
    private void setDuckActive() {
        txtDuck.setTextColor(ContextCompat.getColor(getContext(), R.color.txt_blue2));
        txtDuck.setTextSize(TypedValue.COMPLEX_UNIT_SP, 16);
        txtDuck.setTypeface(txtDuck.getTypeface(), Typeface.BOLD);
    }
    private void setDuckPassive() {
        txtDuck.setTextColor(ContextCompat.getColor(getContext(), R.color.text_color));
        txtDuck.setTextSize(TypedValue.COMPLEX_UNIT_SP, 15);
        txtDuck.setTypeface(txtDuck.getTypeface(), Typeface.NORMAL);
    }
    private void setGoatActive() {
        txtGoat.setTextColor(ContextCompat.getColor(getContext(), R.color.txt_blue2));
        txtGoat.setTextSize(TypedValue.COMPLEX_UNIT_SP, 16);
        txtGoat.setTypeface(txtGoat.getTypeface(), Typeface.BOLD);
    }
    private void setGoatPassive() {
        txtGoat.setTextColor(ContextCompat.getColor(getContext(), R.color.text_color));
        txtGoat.setTextSize(TypedValue.COMPLEX_UNIT_SP, 15);
        txtGoat.setTypeface(txtGoat.getTypeface(), Typeface.NORMAL);
    }
    private void setEtcActive() {
        txtEtc.setTextColor(ContextCompat.getColor(getContext(), R.color.txt_blue2));
        txtEtc.setTextSize(TypedValue.COMPLEX_UNIT_SP, 16);
        txtEtc.setTypeface(txtEtc.getTypeface(), Typeface.BOLD);
    }
    private void setEtcPassive() {
        txtEtc.setTextColor(ContextCompat.getColor(getContext(), R.color.text_color));
        txtEtc.setTextSize(TypedValue.COMPLEX_UNIT_SP, 15);
        txtEtc.setTypeface(txtEtc.getTypeface(), Typeface.NORMAL);
    }


    private void changeIcon(int icon){
        switch (icon){
            case 1:
                setMeatActive();

                setCowPassive();
                setBufPassive();
                setPigPassive();
                setChickPassive();
                setDuckPassive();
                setGoatPassive();
                setEtcPassive();
                break;
            case 2:
                setCowActive();

                setMeatPassive();
                setBufPassive();
                setPigPassive();
                setChickPassive();
                setDuckPassive();
                setGoatPassive();
                setEtcPassive();

                break;
            case 3:
                setBufActive();

                setCowPassive();
                setMeatPassive();
                setPigPassive();
                setChickPassive();
                setDuckPassive();
                setGoatPassive();
                setEtcPassive();
                break;
            case 4:
                setPigActive();

                setCowPassive();
                setBufPassive();
                setMeatPassive();
                setChickPassive();
                setDuckPassive();
                setGoatPassive();
                setEtcPassive();
                break;
            case 5:
                setChickActive();

                setCowPassive();
                setBufPassive();
                setPigPassive();
                setMeatPassive();
                setDuckPassive();
                setGoatPassive();
                setEtcPassive();

                break;
            case 6:
                setDuckActive();

                setCowPassive();
                setBufPassive();
                setPigPassive();
                setChickPassive();
                setMeatPassive();
                setGoatPassive();
                setEtcPassive();

                break;
            case 7:
                setGoatActive();

                setCowPassive();
                setBufPassive();
                setPigPassive();
                setChickPassive();
                setDuckPassive();
                setMeatPassive();
                setEtcPassive();

                break;
            case 8:
                setEtcActive();

                setCowPassive();
                setBufPassive();
                setPigPassive();
                setChickPassive();
                setDuckPassive();
                setGoatPassive();
                setMeatPassive();

                break;
        }
    }

    @OnClick(R.id.btnMeat)
    public void setBtnMeat() {
        getListLivestock(BTN_MEAT, "MEAT");
        changeIcon(1);
    }

    @OnClick(R.id.btnCow)
    public void setBtnCow() {
        getListLivestock(BTN_COW, "COW");
        changeIcon(2);
    }

    @OnClick(R.id.btnBuf)
    public void setBtnBuf() {
        getListLivestock(BTN_BUF, "BUF");
        changeIcon(3);
    }

    @OnClick(R.id.btnPig)
    public void setBtnPig() {
        getListLivestock(BTN_PIG, "PIG");
        changeIcon(4);
    }

    @OnClick(R.id.btnChicken)
    public void setBtnChicken() {
        getListLivestock(BTN_CHICKEN,"CHICKEN");
        changeIcon(5);
    }

    @OnClick(R.id.btnDuck)
    public void setBtnDuck() {
        getListLivestock(BTN_DUCK,"DUCK");
        changeIcon(6);
    }

    @OnClick(R.id.btnGoat)
    public void setBtnGoat() {
        getListLivestock(BTN_GOAT,"GOAT");
        changeIcon(7);
    }

    @OnClick(R.id.btnEtc)
    public void setBtnEtc() {
        getListLivestock(BTN_ETC,"ETC");
        changeIcon(8);
    }


    public void getListLivestock(int type, String tag) {

        for (int i = 0; i < 8; i++) {
            if (type <= 6) {
                ListLiveStockFragment listLiveStockFragment = new ListLiveStockFragment();
                getActivity().getSupportFragmentManager().beginTransaction()
                        .replace(R.id.contentContainerListAnimal, listLiveStockFragment, tag)
                        .addToBackStack(tag)
                        .commit();
            } else {
                ListLiveStockMoreFragment listLiveStockMoreFragment = new ListLiveStockMoreFragment();
                getActivity().getSupportFragmentManager().beginTransaction()
                        .replace(R.id.contentContainerListAnimal, listLiveStockMoreFragment, tag)
                        .addToBackStack(tag)
                        .commit();
            }
        }
    }

    @Override
    public void onStart() {
        super.onStart();
    }

    @Override
    public void onStop() {
        super.onStop();
    }

    /*
     * Save Instance State Here
     */
    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        // Save Instance State here
    }

    /*
     * Restore Instance State Here
     */
    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        if (savedInstanceState != null) {
            // Restore Instance State here
        }
    }


}
