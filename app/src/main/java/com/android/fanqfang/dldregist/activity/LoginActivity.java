package com.android.fanqfang.dldregist.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.Toast;

import com.android.fanqfang.dldregist.R;
import com.android.fanqfang.dldregist.config.Api;
import com.android.fanqfang.dldregist.database.DBHelper;
import com.android.fanqfang.dldregist.model.APIResponse;
import com.android.fanqfang.dldregist.model.AmphurMaster;
import com.android.fanqfang.dldregist.model.FarmOwnerMaster;
import com.android.fanqfang.dldregist.model.FarmProblemMaster;
import com.android.fanqfang.dldregist.model.FarmerProblemMaster;
import com.android.fanqfang.dldregist.model.FarmerTypeMaster;
import com.android.fanqfang.dldregist.model.FeedKindMaster;
import com.android.fanqfang.dldregist.model.HelpMaster;
import com.android.fanqfang.dldregist.model.HoldingLandMaster;
import com.android.fanqfang.dldregist.model.LoginModel;
import com.android.fanqfang.dldregist.model.MainJobMaster;
import com.android.fanqfang.dldregist.model.PrefixMaster;
import com.android.fanqfang.dldregist.model.ProvinceMaster;
import com.android.fanqfang.dldregist.model.SecondJobMaster;
import com.android.fanqfang.dldregist.model.StandardMaster;
import com.android.fanqfang.dldregist.model.SupportStandardMaster;
import com.android.fanqfang.dldregist.model.TambolMaster;
import com.android.fanqfang.dldregist.model.VillageMaster;
import com.android.fanqfang.dldregist.util.AppUtils;
import com.android.fanqfang.dldregist.util.Uploader;
import com.android.fanqfang.dldregist.widget.MyEditNoHLTextView;

import java.io.IOException;

import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.OnClick;
import okhttp3.MediaType;
import okhttp3.Request;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LoginActivity extends AppCompatActivity {

    final String TAG = "LoginActivity";

    DBHelper DBHelper = new DBHelper(this);

    @InjectView(R.id.txtUsername)
    MyEditNoHLTextView txtUsername;

    @InjectView(R.id.txtPassword)
    MyEditNoHLTextView txtPassword;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        ButterKnife.inject(this);

        Thread t = new Thread(new Runnable() {
            @Override
            public void run() {
                Uploader.init(getBaseContext());
                populateDatabase();
            }
        });
        t.setName("database-initializer");
        t.start();

        txtUsername.setText("a1302@dld.go.th");
        txtPassword.setText("1234567890123");

        Uploader.init(getBaseContext());

//        new Thread(new Runnable() {
//            @Override
//            public void run() {
//                testUploads();
//            }
//        }).start();
    }

    void testUploads() {
        Request r;
        for (int i = 0; i < 10000; i++) {
            r = new Request.Builder()
                    .post(RequestBody.create(MediaType.parse("text/plain"), "request number: " + i))
                    .url("http://192.168.1.103:8080/hey" + i)
                    .build();
            Uploader.enqueue(r);
            Uploader.sleep(1000);
        }
    }


    <T extends APIResponse> Response<T> getFromApi(Call<T> call, int numRetries) {
        for (int i = 0; i < numRetries; i++) {
            try {
                Response<T> res = call.execute();
                String url = res.raw().request().url().toString();
                Log.d(TAG, "Executing query: " + url);
                if (!res.isSuccessful()) {
                    Log.d(TAG, "Unexpected HTTP status code on " + url + ": " + res.code());
                    continue;
                }
                if (res.body() == null) {
                    Log.d(TAG, "Null body on response to " + url);
                    continue;
                }
                if (!res.body().getResponseMessage().equals(getString(R.string.success)))
                {
                    Log.d(TAG, "Unexpected response message on " + url + ": " +
                            res.body().getResponseMessage());
                }
                return res;
            } catch (IOException e) {
                Log.d(TAG, "Exception on API request: " + e);
            }
        }
        // TODO: give error report to user (toast?) and terminate program.
        return null;
    }

    <T extends APIResponse> Response<T> getFromApi(Call<T> call) {
        return getFromApi(call, 5);
    }

    void populateDatabase() {
        Api api = AppUtils.getApiService();
        if (!DBHelper.isTablePopulated(PrefixMaster.TABLE)){
            Response<PrefixMaster> res = getFromApi(api.getListPrefix());
            DBHelper.addPrefixs(res.body().getData());
        }
        if (!DBHelper.isTablePopulated(ProvinceMaster.TABLE))  {
            Response<ProvinceMaster> res = getFromApi(api.getListProvince());
            DBHelper.addProvinces(res.body().getData());
        }
        if (!DBHelper.isTablePopulated(AmphurMaster.TABLE)) {
            Response<AmphurMaster> res = getFromApi(api.getListAmphur());
            DBHelper.addAmphurs(res.body().getData());
        }
        if (!DBHelper.isTablePopulated(TambolMaster.TABLE)) {
            Response<TambolMaster> res = getFromApi(api.getListTambol());
            DBHelper.addTambols(res.body().getData());
        }
        if (!DBHelper.isTablePopulated(VillageMaster.TABLE)) {
            Response<VillageMaster> res = getFromApi(api.getListVillage());
            DBHelper.addVillages(res.body().getData());
        }
        if (!DBHelper.isTablePopulated(FarmOwnerMaster.TABLE)) {
            Response<FarmOwnerMaster> res = getFromApi(api.getFarmOwner());
            DBHelper.addFarmOwner(res.body().getData());
        }
        if (!DBHelper.isTablePopulated(FarmProblemMaster.TABLE)) {
            Response<FarmProblemMaster> res = getFromApi(api.getFarmProblem());
            DBHelper.addFarmProblem(res.body().getData());
        }
        if (!DBHelper.isTablePopulated(FarmerProblemMaster.TABLE)) {
            Response<FarmerProblemMaster> res = getFromApi(api.getFarmerProblem());
            DBHelper.addFarmerProblem(res.body().getData());
        }
        if (!DBHelper.isTablePopulated(FarmerTypeMaster.TABLE)) {
            Response<FarmerTypeMaster> res = getFromApi(api.getFarmerType());
            DBHelper.addFarmerType(res.body().getData());
        }
        if (!DBHelper.isTablePopulated(FeedKindMaster.TABLE)) {
            Response<FeedKindMaster> res = getFromApi(api.getFeedKind());
            DBHelper.addFeedKind(res.body().getData());
        }
        if (!DBHelper.isTablePopulated(FeedKindMaster.TABLE)) {
            Response<FeedKindMaster> res = getFromApi(api.getFeedKind());
            DBHelper.addFeedKind(res.body().getData());
        }
        if (!DBHelper.isTablePopulated(HelpMaster.TABLE)) {
            Response<HelpMaster> res = getFromApi(api.getHelp());
            DBHelper.addHelp(res.body().getData());
        }
        if (!DBHelper.isTablePopulated(HoldingLandMaster.TABLE)) {
            Response<HoldingLandMaster> res = getFromApi(api.getHoldingLand());
            DBHelper.addHoldingLand(res.body().getData());
        }
        if (!DBHelper.isTablePopulated(MainJobMaster.TABLE)) {
            Response<MainJobMaster> res = getFromApi(api.getListMainJob());
            DBHelper.addMainJob(res.body().getData());
        }
        if (!DBHelper.isTablePopulated(SecondJobMaster.TABLE)) {
            Response<SecondJobMaster> res = getFromApi(api.getListSecondJob());
            DBHelper.addSecondJob(res.body().getData());
        }
        if (!DBHelper.isTablePopulated(StandardMaster.TABLE)) {
            Response<StandardMaster> res = getFromApi(api.getStandard());
            DBHelper.addStandard(res.body().getData());
        }
        if (!DBHelper.isTablePopulated(SupportStandardMaster.TABLE)) {
            Response<SupportStandardMaster> res = getFromApi(api.getSupportStandard());
            DBHelper.addSupportStandard(res.body().getData());
        }

    }

    @OnClick(R.id.btnLogOnline)
    public void clickBtnLogin() {
        if (txtUsername.getText().length() != 0 && txtPassword.getText().length() != 0) {
            login(txtUsername.getText().toString(), txtPassword.getText().toString());
        } else {
            Toast.makeText(LoginActivity.this, getString(R.string.field_req), Toast.LENGTH_LONG).show();
        }
    }

    @OnClick(R.id.btnRegist)
    public void clickBtnRegist(){
        Intent intent = new Intent(LoginActivity.this, RegisterActivity.class);
        startActivity(intent);
    }

//    @OnClick(R.id.btnSubmit)
//    public void clickBtnSubmit(){
//
//    }

    private void login(String username, String password) {
        Api api = AppUtils.getApiService();
        Call<LoginModel> call = api.login(username, password);
        call.enqueue(new Callback<LoginModel>() {
            @Override
            public void onResponse(Call<LoginModel> call, Response<LoginModel> response) {
                if (response.isSuccessful()) {
                    if (response.body() != null) {
                        if (response.body().getResponseMessage().equals(getString(R.string.success))) {
                            Intent intent = new Intent(LoginActivity.this, FindFarmerActivity.class);
                            startActivity(intent);
                        } else {
                            Toast.makeText(LoginActivity.this, getString(R.string.alert_pass), Toast.LENGTH_LONG).show();
                        }
                    } else {
                        try {
                            Toast.makeText(LoginActivity.this, response.errorBody().string(), Toast.LENGTH_LONG).show();
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                }
            }

            @Override
            public void onFailure(Call<LoginModel> call, Throwable t) {

            }

        });

    }


    //    public void loadFarm() {
//        Api api = AppUtils.getApiService();
//        Call<FarmModel> call = api.getFarm();
//        call.enqueue(new Callback<FarmModel>() {
//            @Override
//            public void onResponse(Call<FarmModel> call, Response<FarmModel> response) {
//                if (response.body() != null && response.body().getResponseMessage().equals(getString(R.string.success))) {
//                    Log.d("MainActivity", "Adding Farm to db....");
//                    final Response<FarmModel> resp = response;
//                    new Thread(new Runnable() {
//                        @Override
//                        public void run() {
//                            DBHelper.addFarm(resp.body().getData());
//                            Log.d("MainActivity", "Added Farm to the db.");
//                        }
//                    }).start();
//                }
//            }
//
//            @Override
//            public void onFailure(Call<FarmModel> call, Throwable t) {
//
//            }
//        });
//
//    }

//    public void loadListFarm() {
//        Api api = AppUtils.getApiService();
//        Call<FarmModel> call = api.getListFarm();
//        call.enqueue(new Callback<FarmModel>() {
//            @Override
//            public void onResponse(Call<FarmModel> call, Response<FarmModel> response) {
//                if (response.body() != null && response.body().getResponseMessage().equals(getString(R.string.success))) {
//                    Log.d("MainActivity", "Adding Farm to db....");
//                    final Response<FarmModel> resp = response;
//                    new Thread(new Runnable() {
//                        @Override
//                        public void run() {
//                            DBHelper.addFarm(resp.body().getData());
//                            Log.d("MainActivity", "Added Farm to the db.");
//                        }
//                    }).start();
//                }
//            }
//
//            @Override
//            public void onFailure(Call<FarmModel> call, Throwable t) {
//
//            }
//        });
//
//    }


    // TODO: SELECT FARM_ID TO FIND FEED
//    public void loadFeed() {
//        Api api = AppUtils.getApiService();
//        Call<FeedModel> call = api.getFeed();
//        call.enqueue(new Callback<FeedModel>() {
//            @Override
//            public void onResponse(Call<FeedModel> call, Response<FeedModel> response) {
//                if (response.body() != null && response.body().getResponseMessage().equals(getString(R.string.success))) {
//                    Log.d("MainActivity", "Adding feed to db....");
//                    final Response<FeedModel> resp = response;
//                    new Thread(new Runnable() {
//                        @Override
//                        public void run() {
//                            DBHelper.addFeed(resp.body().getData());
//                            Log.d("MainActivity", "Added feed to the db.");
//                        }
//                    }).start();
//                }
//            }
//            @Override
//            public void onFailure(Call<FeedModel> call, Throwable t) {
//
//            }
//        });
//    }


}
