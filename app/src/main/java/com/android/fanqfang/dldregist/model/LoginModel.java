package com.android.fanqfang.dldregist.model;

import org.parceler.Parcel;

/**
 * Created by fanqfang on 9/18/2017 AD.
 */

@Parcel
public class LoginModel {
    String responseCode;
    String responseMessage;
    LoginEntity data;

    public String getResponseCode() {
        return responseCode;
    }

    public void setResponseCode(String responseCode) {
        this.responseCode = responseCode;
    }

    public String getResponseMessage() {
        return responseMessage;
    }

    public void setResponseMessage(String responseMessage) {
        this.responseMessage = responseMessage;
    }

    public LoginEntity getData() {
        return data;
    }

    public void setData(LoginEntity data) {
        this.data = data;
    }

    public LoginModel() {
    }

    @Parcel
    public static class LoginEntity {
        int accessFailedCount;
        String address;
        String amphur;
        String birthday;
        int canAdd;
        int canConfirm;
        int canModify;
        int canReadReport;
        String createBy;
        String creatDate;
        String email;
        Boolean emailConfirmed;
        String firstnameEng;
        String firstnameThai;
        String id;
        String IDcard;
        String imageProfile;
        String imei;
        int isActive;
        String lastnameEng;
        String lastnameThai;
        Boolean lockoutEnabled;
        String lockoutEndDateUtc;
        String modifyBy;
        String modifyDate;
        String passwordHash;
        String phoneNumber;
        Boolean phoneNumberConfirmed;
        String postcode;
        String province;
        String securityStamp;
        String tambol;
        String title ;
        Boolean twoFactorEnabled;
        String userName;

        public LoginEntity(){

        }

        public int getAccessFailedCount() {
            return accessFailedCount;
        }

        public void setAccessFailedCount(int accessFailedCount) {
            this.accessFailedCount = accessFailedCount;
        }

        public String getAddress() {
            return address;
        }

        public void setAddress(String address) {
            this.address = address;
        }

        public String getAmphur() {
            return amphur;
        }

        public void setAmphur(String amphur) {
            this.amphur = amphur;
        }

        public String getBirthday() {
            return birthday;
        }

        public void setBirthday(String birthday) {
            this.birthday = birthday;
        }

        public int getCanAdd() {
            return canAdd;
        }

        public void setCanAdd(int canAdd) {
            this.canAdd = canAdd;
        }

        public int getCanConfirm() {
            return canConfirm;
        }

        public void setCanConfirm(int canConfirm) {
            this.canConfirm = canConfirm;
        }

        public int getCanModify() {
            return canModify;
        }

        public void setCanModify(int canModify) {
            this.canModify = canModify;
        }

        public int getCanReadReport() {
            return canReadReport;
        }

        public void setCanReadReport(int canReadReport) {
            this.canReadReport = canReadReport;
        }

        public String getCreateBy() {
            return createBy;
        }

        public void setCreateBy(String createBy) {
            this.createBy = createBy;
        }

        public String getCreatDate() {
            return creatDate;
        }

        public void setCreatDate(String creatDate) {
            this.creatDate = creatDate;
        }

        public String getEmail() {
            return email;
        }

        public void setEmail(String email) {
            this.email = email;
        }

        public Boolean getEmailConfirmed() {
            return emailConfirmed;
        }

        public void setEmailConfirmed(Boolean emailConfirmed) {
            this.emailConfirmed = emailConfirmed;
        }

        public String getFirstnameEng() {
            return firstnameEng;
        }

        public void setFirstnameEng(String firstnameEng) {
            this.firstnameEng = firstnameEng;
        }

        public String getFirstnameThai() {
            return firstnameThai;
        }

        public void setFirstnameThai(String firstnameThai) {
            this.firstnameThai = firstnameThai;
        }

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getIDcard() {
            return IDcard;
        }

        public void setIDcard(String IDcard) {
            this.IDcard = IDcard;
        }

        public String getImageProfile() {
            return imageProfile;
        }

        public void setImageProfile(String imageProfile) {
            this.imageProfile = imageProfile;
        }

        public String getImei() {
            return imei;
        }

        public void setImei(String imei) {
            this.imei = imei;
        }

        public int getIsActive() {
            return isActive;
        }

        public void setIsActive(int isActive) {
            this.isActive = isActive;
        }

        public String getLastnameEng() {
            return lastnameEng;
        }

        public void setLastnameEng(String lastnameEng) {
            this.lastnameEng = lastnameEng;
        }

        public String getLastnameThai() {
            return lastnameThai;
        }

        public void setLastnameThai(String lastnameThai) {
            this.lastnameThai = lastnameThai;
        }

        public Boolean getLockoutEnabled() {
            return lockoutEnabled;
        }

        public void setLockoutEnabled(Boolean lockoutEnabled) {
            this.lockoutEnabled = lockoutEnabled;
        }

        public String getLockoutEndDateUtc() {
            return lockoutEndDateUtc;
        }

        public void setLockoutEndDateUtc(String lockoutEndDateUtc) {
            this.lockoutEndDateUtc = lockoutEndDateUtc;
        }

        public String getModifyBy() {
            return modifyBy;
        }

        public void setModifyBy(String modifyBy) {
            this.modifyBy = modifyBy;
        }

        public String getModifyDate() {
            return modifyDate;
        }

        public void setModifyDate(String modifyDate) {
            this.modifyDate = modifyDate;
        }

        public String getPasswordHash() {
            return passwordHash;
        }

        public void setPasswordHash(String passwordHash) {
            this.passwordHash = passwordHash;
        }

        public String getPhoneNumber() {
            return phoneNumber;
        }

        public void setPhoneNumber(String phoneNumber) {
            this.phoneNumber = phoneNumber;
        }

        public Boolean getPhoneNumberConfirmed() {
            return phoneNumberConfirmed;
        }

        public void setPhoneNumberConfirmed(Boolean phoneNumberConfirmed) {
            this.phoneNumberConfirmed = phoneNumberConfirmed;
        }

        public String getPostcode() {
            return postcode;
        }

        public void setPostcode(String postcode) {
            this.postcode = postcode;
        }

        public String getProvince() {
            return province;
        }

        public void setProvince(String province) {
            this.province = province;
        }

        public String getSecurityStamp() {
            return securityStamp;
        }

        public void setSecurityStamp(String securityStamp) {
            this.securityStamp = securityStamp;
        }

        public String getTambol() {
            return tambol;
        }

        public void setTambol(String tambol) {
            this.tambol = tambol;
        }

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public Boolean getTwoFactorEnabled() {
            return twoFactorEnabled;
        }

        public void setTwoFactorEnabled(Boolean twoFactorEnabled) {
            this.twoFactorEnabled = twoFactorEnabled;
        }

        public String getUserName() {
            return userName;
        }

        public void setUserName(String userName) {
            this.userName = userName;
        }
    }
}
