package com.android.fanqfang.dldregist.fragment;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;

import com.android.fanqfang.dldregist.R;
import com.android.fanqfang.dldregist.widget.MyNoHBTextView;

import butterknife.ButterKnife;

public class FarmActivityFragment extends Fragment {

    private FrameLayout contentActivity;
    private static final int TAB_ONE = 1;
    private static final int TAB_TWO = 2;
    private boolean isFirst = true;
    private MyNoHBTextView tabOne;
    private MyNoHBTextView tabTwo;

    public FarmActivityFragment() {
        super();
    }

    public static FarmActivityFragment newInstance() {
        FarmActivityFragment fragment = new FarmActivityFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_farm_activity, container, false);
        ButterKnife.inject(this, rootView);
        initInstances(rootView);
        getTabOneFragment();
        return rootView;
    }

    private void initInstances(View rootView) {
        // Init 'View' instance(s) with rootView.findViewById here
        contentActivity = (FrameLayout) rootView.findViewById(R.id.contentActivity);
        tabOne = (MyNoHBTextView) rootView.findViewById(R.id.tabOne);
        tabTwo = (MyNoHBTextView) rootView.findViewById(R.id.tabTwo);

    }

    public void getTabOneFragment() {
        getActivity().getSupportFragmentManager().beginTransaction()
                .replace(R.id.contentActivity, MenuFarmFragment.newInstance())
                .commit();
    }

    public void getTabTwoFragment() {
        getActivity().getSupportFragmentManager().beginTransaction()
                .replace(R.id.contentActivity, LiveStockFragment.newInstance())
                .commit();
    }

//    @OnClick(R.id.tabOne)
    public void getFragmentTabOne() {
        tabTwo.setBackgroundColor(getResources().getColor(R.color.edit_txt_grey));
        tabOne.setBackgroundColor(getResources().getColor(R.color.white));
        if (isFirst) {
            isFirst = false;
            MenuFarmFragment menuFarm = new MenuFarmFragment();
            getActivity().getSupportFragmentManager().beginTransaction()
                    .replace(R.id.contentActivity, menuFarm)
                    .commit();
            getTabOneFragment();
        } else {
            getTabOneFragment();
        }
    }

//    @OnClick(R.id.tabTwo)
    public void getFragmentTabTwo() {
        tabOne.setBackgroundColor(getResources().getColor(R.color.edit_txt_grey));
        tabTwo.setBackgroundColor(getResources().getColor(R.color.white));
        if (isFirst) {
            isFirst = false;
            LiveStockFragment liveStock = new LiveStockFragment();
            getActivity().getSupportFragmentManager().beginTransaction()
                    .replace(R.id.contentActivity, liveStock)
                    .commit();
            getTabTwoFragment();
        } else {
            getTabTwoFragment();
        }
    }

    @Override
    public void onStart() {
        super.onStart();
    }

    @Override
    public void onStop() {
        super.onStop();
    }

    /*
     * Save Instance State Here
     */
    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        // Save Instance State here
    }

    /*
     * Restore Instance State Here
     */
    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        if (savedInstanceState != null) {
            // Restore Instance State here
        }
    }
}
