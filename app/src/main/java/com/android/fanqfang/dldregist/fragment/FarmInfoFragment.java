package com.android.fanqfang.dldregist.fragment;

import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.IdRes;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.Toast;

import com.android.fanqfang.dldregist.R;
import com.android.fanqfang.dldregist.activity.FarmActivity;
import com.android.fanqfang.dldregist.database.DBHelper;
import com.android.fanqfang.dldregist.model.FarmModel.FarmEntity;
import com.android.fanqfang.dldregist.model.SupportStandardMaster.SupportEntity;
import com.android.fanqfang.dldregist.widget.MyEditNoHLTextView;
import com.android.fanqfang.dldregist.widget.MySpinnerAdapter;

import java.util.ArrayList;

import butterknife.ButterKnife;
import butterknife.OnClick;

public class FarmInfoFragment extends Fragment {

    private LinearLayout llFarmAddress;
    private LinearLayout llFarmerAddress;
    private RadioGroup radioGroup;
    private MyEditNoHLTextView txtAnimalType;
    private MyEditNoHLTextView txtFarmNo;
    private MyEditNoHLTextView txtRevenue;
    private MyEditNoHLTextView txtAnimalWorth;
    private MyEditNoHLTextView txtRai;
    private MyEditNoHLTextView txtNgan;
    private MyEditNoHLTextView txtMachine;
    private MyEditNoHLTextView txtProblem;
    private MyEditNoHLTextView txtStatus;
    private MyEditNoHLTextView txtHelp;

    ArrayList<SupportEntity> support = new ArrayList<>();
    ArrayList<String> listSupport = new ArrayList<>();

    FarmEntity farm;
    DBHelper dbHelper;
    private Spinner spnSupport;
    private RadioButton radisoOwner;
    private RadioButton radioNotOwner;

    public FarmInfoFragment() {
        super();
    }

    public static FarmInfoFragment newInstance() {
        FarmInfoFragment fragment = new FarmInfoFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_farm_info, container, false);
        ButterKnife.inject(this, rootView);

        initInstances(rootView);
        setTypeFaceRadio();
        farm = ((FarmActivity) getActivity()).getFarm();
        dbHelper = new DBHelper(getContext());

//        getSupport();
        setCategory();


        return rootView;
    }

    private void initInstances(View rootView) {
        // Init 'View' instance(s) with rootView.findViewById here
        radioGroup = (RadioGroup) rootView.findViewById(R.id.radio);
        radisoOwner = (RadioButton) rootView.findViewById(R.id.radioOwner);
        radioNotOwner = (RadioButton) rootView.findViewById(R.id.radioNotOwner);
        spnSupport = (Spinner) rootView.findViewById(R.id.spnStandSup);
        txtAnimalType = (MyEditNoHLTextView) rootView.findViewById(R.id.txtAnimalType);
        txtFarmNo = (MyEditNoHLTextView) rootView.findViewById(R.id.txtFarmNo);
        txtRevenue = (MyEditNoHLTextView) rootView.findViewById(R.id.txtSalary);
        txtAnimalWorth = (MyEditNoHLTextView) rootView.findViewById(R.id.txtPrice);
        txtRai = (MyEditNoHLTextView) rootView.findViewById(R.id.txtRai);
        txtNgan = (MyEditNoHLTextView) rootView.findViewById(R.id.txtNgan);
//        txtWaa = (MyEditNoHLTextView) rootView.findViewById(R.id.txtWaa);
        txtMachine = (MyEditNoHLTextView) rootView.findViewById(R.id.txtMachine);
        txtProblem = (MyEditNoHLTextView) rootView.findViewById(R.id.txtProblem);
        txtStatus = (MyEditNoHLTextView) rootView.findViewById(R.id.txtStatus);
        txtHelp = (MyEditNoHLTextView) rootView.findViewById(R.id.txtHelp);

    }

    private void setTypeFaceRadio(){
        Typeface tf = Typeface.createFromAsset(getContext().getAssets(), getResources().getString(R.string.font_en_light));
        radisoOwner.setTypeface(tf);
        radioNotOwner.setTypeface(tf);
    }

    private void setCategory() {
//        txtAnimalType.setText(farm.get);
        txtFarmNo.setText(farm.getFarm_ID() + "");
        txtRevenue.setText(farm.getRevenueOfLivestock() + "");
        txtAnimalWorth.setText(farm.getAnimalWorth() + "");
        txtRai.setText(farm.getArea_Rai());
        txtNgan.setText(farm.getArea_Ngan());
        txtProblem.setText(farm.getFarm_Problem_Desc());

        //TODO: SET DATA
    }

    private void setInfo(){
        farm.setFarm_ID(Integer.parseInt(txtFarmNo.getText().toString()));
        farm.setRevenueOfLivestock(Integer.parseInt(txtRevenue.getText().toString()));
        farm.setAnimalWorth(Integer.parseInt(txtAnimalWorth.getText().toString()));
        farm.setArea_Rai(txtRai.getText().toString());
        farm.setArea_Ngan(txtNgan.getText().toString());
        farm.setFarm_Problem_Desc(txtProblem.getText().toString());
    }

    private void setRadioGroup() {
        radioGroup.clearCheck();

        radioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, @IdRes int checkedId) {
                RadioButton rbOwn = (RadioButton) group.findViewById(checkedId);
                if (null != rbOwn && checkedId > -1) {
                    Toast.makeText(getContext(), rbOwn.getText(), Toast.LENGTH_SHORT).show();
                }
            }
        });


    }

    public void onClear(View v) {
        /* Clears all selected radio buttons to default */
        radioGroup.clearCheck();
    }

    public void onSubmit(View v) {
        RadioButton rb = (RadioButton) radioGroup.findViewById(radioGroup.getCheckedRadioButtonId());
        Toast.makeText(getContext(), rb.getText(), Toast.LENGTH_SHORT).show();
    }

    @OnClick(R.id.btnSubmit)
    public void clickSubmit(){
        setInfo();
        dbHelper.updateFarm(farm, farm.getFarm_ID());
    }

    @Override
    public void onStart() {
        super.onStart();
    }

    @Override
    public void onStop() {
        super.onStop();
    }

    /*
     * Save Instance State Here
     */
    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        // Save Instance State here
    }

    /*
     * Restore Instance State Here
     */
    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        if (savedInstanceState != null) {
            // Restore Instance State here
        }
    }

    public void getSupport() {
        int selectIndex = -1;
        support = dbHelper.getSupport();

        for (int i = 0; i < support.size(); i++) {
            listSupport.add(support.get(i).getSupport_Standard_Name());
            Log.d("STANDARD LIST -->> ", String.valueOf(support.get(i).getSupport_Standard_Name()));
            if (support.get(i).getSupport_Standard_ID() == farm.getSupport_Standard_ID()) {
                selectIndex = i;
            }
        }

        MySpinnerAdapter adapter = new MySpinnerAdapter(
                getContext(),
                android.R.layout.simple_dropdown_item_1line,
                listSupport);
        spnSupport.setAdapter(adapter);

        spnSupport.setSelection(selectIndex);

        spnSupport.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }


}
