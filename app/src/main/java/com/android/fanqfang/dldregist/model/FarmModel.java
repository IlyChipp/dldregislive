package com.android.fanqfang.dldregist.model;

import android.provider.BaseColumns;

import org.parceler.Parcel;

import java.util.ArrayList;

/**
 * Created by fanqfang on 9/26/2017 AD.
 */
@Parcel
public class FarmModel {
    //DBHelper
    public static final String TABLE = "farm";

    String responseCopde ;
    String responseMessage;
    ArrayList<FarmEntity> data;

    public String getResponseCopde() {
        return responseCopde;
    }

    public void setResponseCopde(String responseCopde) {
        this.responseCopde = responseCopde;
    }

    public String getResponseMessage() {
        return responseMessage;
    }

    public void setResponseMessage(String responseMessage) {
        this.responseMessage = responseMessage;
    }

    public ArrayList<FarmEntity> getData() {
        return data;
    }

    public void setData(ArrayList<FarmEntity> data) {
        this.data = data;
    }

    public FarmModel(){ }

    @Parcel
    public static class FarmEntity {
        int farm_ID;
        int amphur_ID;
        int animalWorth;
        String area_Rai;
        String area_Ngan;
        String area_Type_ID;
        String cancelBy;
        String cancelDate;
        String createBy;
        String createDate;
        String email;
        String farm_Name;
        String farm_Owner_Type_ID;
        String farm_Problem_Desc;
        String farm_Standard_Number;
        int farmer_ID;
        String homeNo;
        int isCancel;
        String latitude;
        String longitude;
        String mobile;
        String orderIndex;
        String phone;
        String postCode;
        int province_ID;
        int revenueOfLivestock;
        String road;
        String soi;
        int standard_ID;
        int status_ID;
        int support_Standard_ID;
        int tambol_ID;
        String updateBy;
        String updateDate;
        String village;
        int village_ID;
        String worker_ID;

        public FarmEntity(){ }

        public int getFarm_ID() {
            return farm_ID;
        }

        public void setFarm_ID(int farm_ID) {
            this.farm_ID = farm_ID;
        }

        public int getAmphur_ID() {
            return amphur_ID;
        }

        public void setAmphur_ID(int amphur_ID) {
            this.amphur_ID = amphur_ID;
        }

        public int getAnimalWorth() {
            return animalWorth;
        }

        public void setAnimalWorth(int animalWorth) {
            this.animalWorth = animalWorth;
        }

        public String getArea_Rai() {
            return area_Rai;
        }

        public void setArea_Rai(String area_Rai) {
            this.area_Rai = area_Rai;
        }

        public String getArea_Ngan() {
            return area_Ngan;
        }

        public void setArea_Ngan(String area_Ngan) {
            this.area_Ngan = area_Ngan;
        }

        public String getArea_Type_ID() {
            return area_Type_ID;
        }

        public void setArea_Type_ID(String area_Type_ID) {
            this.area_Type_ID = area_Type_ID;
        }

        public String getCancelBy() {
            return cancelBy;
        }

        public void setCancelBy(String cancelBy) {
            this.cancelBy = cancelBy;
        }

        public String getCancelDate() {
            return cancelDate;
        }

        public void setCancelDate(String cancelDate) {
            this.cancelDate = cancelDate;
        }

        public String getCreateBy() {
            return createBy;
        }

        public void setCreateBy(String createBy) {
            this.createBy = createBy;
        }

        public String getCreateDate() {
            return createDate;
        }

        public void setCreateDate(String createDate) {
            this.createDate = createDate;
        }

        public String getEmail() {
            return email;
        }

        public void setEmail(String email) {
            this.email = email;
        }

        public String getFarm_Name() {
            return farm_Name;
        }

        public void setFarm_Name(String farm_Name) {
            this.farm_Name = farm_Name;
        }

        public String getFarm_Owner_Type_ID() {
            return farm_Owner_Type_ID;
        }

        public void setFarm_Owner_Type_ID(String farm_Owner_Type_ID) {
            this.farm_Owner_Type_ID = farm_Owner_Type_ID;
        }

        public String getFarm_Problem_Desc() {
            return farm_Problem_Desc;
        }

        public void setFarm_Problem_Desc(String farm_Problem_Desc) {
            this.farm_Problem_Desc = farm_Problem_Desc;
        }

        public String getFarm_Standard_Number() {
            return farm_Standard_Number;
        }

        public void setFarm_Standard_Number(String farm_Standard_Number) {
            this.farm_Standard_Number = farm_Standard_Number;
        }

        public int getFarmer_ID() {
            return farmer_ID;
        }

        public void setFarmer_ID(int farmer_ID) {
            this.farmer_ID = farmer_ID;
        }

        public String getHomeNo() {
            return homeNo;
        }

        public void setHomeNo(String homeNo) {
            this.homeNo = homeNo;
        }

        public int getIsCancel() {
            return isCancel;
        }

        public void setIsCancel(int isCancel) {
            this.isCancel = isCancel;
        }

        public String getLatitude() {
            return latitude;
        }

        public void setLatitude(String latitude) {
            this.latitude = latitude;
        }

        public String getLongitude() {
            return longitude;
        }

        public void setLongitude(String longitude) {
            this.longitude = longitude;
        }

        public String getMobile() {
            return mobile;
        }

        public void setMobile(String mobile) {
            this.mobile = mobile;
        }

        public String getOrderIndex() {
            return orderIndex;
        }

        public void setOrderIndex(String orderIndex) {
            this.orderIndex = orderIndex;
        }

        public String getPhone() {
            return phone;
        }

        public void setPhone(String phone) {
            this.phone = phone;
        }

        public String getPostCode() {
            return postCode;
        }

        public void setPostCode(String postCode) {
            this.postCode = postCode;
        }

        public int getProvince_ID() {
            return province_ID;
        }

        public void setProvince_ID(int province_ID) {
            this.province_ID = province_ID;
        }

        public int getRevenueOfLivestock() {
            return revenueOfLivestock;
        }

        public void setRevenueOfLivestock(int revenueOfLivestock) {
            this.revenueOfLivestock = revenueOfLivestock;
        }

        public String getRoad() {
            return road;
        }

        public void setRoad(String road) {
            this.road = road;
        }

        public String getSoi() {
            return soi;
        }

        public void setSoi(String soi) {
            this.soi = soi;
        }

        public int getStandard_ID() {
            return standard_ID;
        }

        public void setStandard_ID(int standard_ID) {
            this.standard_ID = standard_ID;
        }

        public int getStatus_ID() {
            return status_ID;
        }

        public void setStatus_ID(int status_ID) {
            this.status_ID = status_ID;
        }

        public int getSupport_Standard_ID() {
            return support_Standard_ID;
        }

        public void setSupport_Standard_ID(int support_Standard_ID) {
            this.support_Standard_ID = support_Standard_ID;
        }

        public int getTambol_ID() {
            return tambol_ID;
        }

        public void setTambol_ID(int tambol_ID) {
            this.tambol_ID = tambol_ID;
        }

        public String getUpdateBy() {
            return updateBy;
        }

        public void setUpdateBy(String updateBy) {
            this.updateBy = updateBy;
        }

        public String getUpdateDate() {
            return updateDate;
        }

        public void setUpdateDate(String updateDate) {
            this.updateDate = updateDate;
        }

        public String getVillage() {
            return village;
        }

        public void setVillage(String village) {
            this.village = village;
        }

        public int getVillage_ID() {
            return village_ID;
        }

        public void setVillage_ID(int village_ID) {
            this.village_ID = village_ID;
        }

        public String getWorker_ID() {
            return worker_ID;
        }

        public void setWorker_ID(String worker_ID) {
            this.worker_ID = worker_ID;
        }
    }

    public class Column{
        public static final String ID = BaseColumns._ID;
        public static final String FARM_ID = "farm_ID";
        public static final String AMPHUR_ID = "amphur_ID";
        public static final String ANIMAL_WORTH = "animalWorth";
        public static final String AREA_RAI = "area_Rai";
        public static final String AREA_NGAN = "area_Ngan";
        public static final String AREA_TYPE_ID = "area_Type_ID";
        public static final String CANCEL_BY = "cancelBy";
        public static final String CANCEL_DATE = "cancelDate";
        public static final String CREATE_BY = "createBy";
        public static final String CREATE_DATE = "createDate";
        public static final String EMAIL = "email";
        public static final String FARM_NAME = "farm_Name";
        public static final String FARM_OWNER_TYPE_ID = "farm_Owner_Type_ID";
        public static final String FARM_PROBLEM_DESC = "farm_Problem_Desc";
        public static final String FARM_STANDARD_NUMBER = "farm_Standard_Number";
        public static final String FARMER_ID ="farmer_ID";
        public static final String HOME_NO = "homeNo";
        public static final String IS_CANCEL = "isCancel";
        public static final String LATITUDE = "latitude";
        public static final String LONGITUDE = "longitude";
        public static final String MOBILE = "mobile";
        public static final String ORDER_INDEX = "orderIndex";
        public static final String PHONE = "phone";
        public static final String POSTCODE = "postCode";
        public static final String PROVINCE_ID = "province_ID";
        public static final String REVENUE_OF_LIVESTOCK = "revenueOfLivestock";
        public static final String ROAD = "road";
        public static final String SOI = "soi";
        public static final String STANDARD_ID = "standard_ID";
        public static final String STATUS_ID = "status_ID";
        public static final String SUPPORT_STANDARD_ID ="support_Standard_ID";
        public static final String TAMBOL_ID = "tambol_ID";
        public static final String UPDATE_BY = "updateBy";
        public static final String UPDATE_DATE ="updateDate";
        public static final String VILLAGE = "village";
        public static final String VILLAGE_ID = "village_ID";
        public static final String WORKER_ID = "worker_ID";


    }
}
