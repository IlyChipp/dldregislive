package com.android.fanqfang.dldregist.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.android.fanqfang.dldregist.R;
import com.android.fanqfang.dldregist.activity.FarmActivity;
import com.android.fanqfang.dldregist.config.Api;
import com.android.fanqfang.dldregist.database.DBHelper;
import com.android.fanqfang.dldregist.model.FarmModel;
import com.android.fanqfang.dldregist.model.FarmModel.FarmEntity;
import com.android.fanqfang.dldregist.model.FarmerModel.FarmerEntity;
import com.android.fanqfang.dldregist.util.AppUtils;
import com.android.fanqfang.dldregist.widget.MyNoHBTextView;

import org.parceler.Parcels;

import java.util.ArrayList;

import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ListFarmFragment extends Fragment {

    DBHelper dbHelper ;
    private static final String CLASS_NAME = "ListFarmFragment";
    final private static String FARMINFO = "farmInfo";
    private ArrayList<FarmEntity> farmEntities ;
    private FarmEntity farm = new FarmEntity();
    private RecyclerView listFarm;
    private FarmerEntity mFarmer;


    public ListFarmFragment() {
        super();
    }

    public static ListFarmFragment newInstance() {
        ListFarmFragment fragment = new ListFarmFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_list_farm, container, false);
        ButterKnife.inject(this, rootView);
        initInstances(rootView);

        getListFarm();

        return rootView;
    }

    private void initInstances(View rootView) {
        // Init 'View' instance(s) with rootView.findViewById here
        listFarm = (RecyclerView) rootView.findViewById(R.id.listFarm);
    }

    @Override
    public void onStart() {
        super.onStart();
    }

    @Override
    public void onStop() {
        super.onStop();
    }

    /*
     * Save Instance State Here
     */
    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        // Save Instance State here
    }

    /*
     * Restore Instance State Here
     */
    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        if (savedInstanceState != null) {
            // Restore Instance State here

        }
    }

    @OnClick(R.id.addFarm)
    public void gotoFarm() {
        //TODO: CHANGE FLOW TO ADD FARM
        Intent intent = new Intent(getActivity(), FarmActivity.class);
        getActivity().startActivity(intent);
    }

    public void getListFarm() {
        dbHelper = new DBHelper(getContext());
        mFarmer = dbHelper.getInfoFarmer();

        Api api = AppUtils.getApiService();
        Call<FarmModel> call = api.getListFarm(""+ mFarmer.getFarmer_ID());
        call.enqueue(new Callback<FarmModel>() {
            @Override
            public void onResponse(Call<FarmModel> call, Response<FarmModel> response) {
                if (response.body() != null && response.body().getResponseMessage().equals(getString(R.string.success))) {
                        dbHelper.addListFarm(response.body().getData());
                } else {
                    Toast.makeText(getContext(), getString(R.string.content_fail), Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onFailure(Call<FarmModel> call, Throwable t) {
                try {
                    Log.d(CLASS_NAME, t.getMessage());

                    Toast.makeText(getContext(), getString(R.string.content_fail), Toast.LENGTH_SHORT).show();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });

        listFarm.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false));
        ListAdapter listFarmAdapter = new ListAdapter();
        listFarm.setAdapter(listFarmAdapter);

    }



    public class ListAdapter extends RecyclerView.Adapter<ListAdapter.ViewHolder> {

        @Override
        public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_list_farm, parent, false);

            return new ViewHolder(v);
        }

        @Override
        public void onBindViewHolder(ViewHolder holder, final int position) {

            final FarmModel.FarmEntity item = farmEntities.get(position);

            holder.mtxtFarmName.setText(item.getFarm_Name());
            holder.mshowFarm.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent = new Intent(getActivity(), FarmActivity.class);
                    intent.putExtra(FARMINFO , Parcels.wrap(item));
                    getActivity().startActivity(intent);
                }
            });

        }

        @Override
        public int getItemCount() {
            return farmEntities.size();
        }

        public class ViewHolder extends RecyclerView.ViewHolder {

            private final MyNoHBTextView mtxtFarmName;
            private final LinearLayout mshowFarm;

            public ViewHolder(View itemView) {
                super(itemView);

                mtxtFarmName = (MyNoHBTextView) itemView.findViewById(R.id.txtFarmName);
                mshowFarm = (LinearLayout) itemView.findViewById(R.id.showFarm);
            }
        }

    }
}
