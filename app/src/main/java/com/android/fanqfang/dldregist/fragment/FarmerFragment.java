package com.android.fanqfang.dldregist.fragment;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.LinearLayout;
import android.widget.Spinner;

import com.android.fanqfang.dldregist.R;
import com.android.fanqfang.dldregist.database.DBHelper;
import com.android.fanqfang.dldregist.manager.MyPreferenceManager;
import com.android.fanqfang.dldregist.model.AmphurMaster.AmphurEntity;
import com.android.fanqfang.dldregist.model.FarmerModel.FarmerEntity;
import com.android.fanqfang.dldregist.model.PrefixMaster.PrefixEntity;
import com.android.fanqfang.dldregist.model.ProvinceMaster.ProvinceEntity;
import com.android.fanqfang.dldregist.model.TambolMaster.TambolEntity;
import com.android.fanqfang.dldregist.model.VillageMaster.VillageEntity;
import com.android.fanqfang.dldregist.widget.MyEditNoHLTextView;
import com.android.fanqfang.dldregist.widget.MyNoHBTextView;
import com.android.fanqfang.dldregist.widget.MySpinnerAdapter;

import java.util.ArrayList;

import butterknife.ButterKnife;
import butterknife.OnClick;


public class FarmerFragment extends Fragment {


    private static final String CLASS_NAME = "FarmerFragment";


    ArrayList<String> listProvince = new ArrayList<>();
    ArrayList<String> listPrefix = new ArrayList<>();
    ArrayList<String> listVillage = new ArrayList<>();
    ArrayList<String> listTambol = new ArrayList<>();
    ArrayList<String> listAmphur = new ArrayList<>();

    ArrayList<ProvinceEntity> province = new ArrayList<>();
    ArrayList<AmphurEntity> amphur = new ArrayList<>();
    ArrayList<TambolEntity> tambol = new ArrayList<>();
    ArrayList<VillageEntity> village = new ArrayList<>();
    ArrayList<PrefixEntity> prefix = new ArrayList<>();

    DBHelper DBHelper;
    final static String PROVINCE = "province";
    final static String AMPHUR = "amphur";
    final static String TAMBOL = "tambol";
    final static String VILLAGE = "village";

    private FarmerEntity mFarmer;
    private MyEditNoHLTextView txtPID;
    private MyEditNoHLTextView txtNameTH;
    private MyEditNoHLTextView txtLastnameTH;
    private MyEditNoHLTextView txtHomeID;
    private MyEditNoHLTextView txtHomeNo;
    private MyEditNoHLTextView txtPost;
    private MyEditNoHLTextView txtMobile;
    private MyEditNoHLTextView txtPhone;
    private MyEditNoHLTextView txtLat;
    private MyEditNoHLTextView txtLong;
    private MyEditNoHLTextView txtEmail;
    private LinearLayout addFarm;
    private LinearLayout llFarmerAddress;
    private Spinner spnAmphur;
    private Spinner spnPrefix;
    private Spinner spnTambol;
    private Spinner spnProvince;
    private Spinner spnVillage;
    private MyNoHBTextView btnSubmit;
    private MyEditNoHLTextView txtMoo;
    private MyPreferenceManager myPreferenceManager;


    public FarmerFragment() {
        super();
    }

    public static FarmerFragment newInstance() {
        FarmerFragment fragment = new FarmerFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_farmer, container, false);
        View v = inflater.inflate(R.layout.item_farmer_address, container, false);
        ButterKnife.inject(this, rootView);

        initInstances(rootView, v);
        myPreferenceManager = new MyPreferenceManager(getContext());
        llFarmerAddress.addView(v);

        DBHelper = new DBHelper(getContext());
//        setEnabled();

        setInfoFarmer();
        txtPID.setEnabled(false);

        getListPrefix();
        getListProvince();


//        txtEmail.setOnFocusChangeListener(new View.OnFocusChangeListener() {
//            @Override
//            public void onFocusChange(View v, boolean hasFocus) {
//                if (!hasFocus)
//                    return;
//                ValidatingView vv = (ValidatingView)v;
//                if (vv.isDataValid()) {
//                    TextView tv = (TextView)v;
//                    dataToSubmit.setEmail(tv.getText()));
//                } else {
//                    vv.showError();
//                }
//            }
//        });


        return rootView;
    }

    private void setEnabled(){
        spnAmphur.setEnabled(false);
        spnTambol.setEnabled(false);
        spnVillage.setEnabled(false);
        txtPID.setEnabled(false);
    }

    private void initInstances(View rootView, View view) {
        // Init 'View' instance(s) with rootView.findViewById here

        llFarmerAddress = (LinearLayout) rootView.findViewById(R.id.llFarmerAddress);
        txtPID = (MyEditNoHLTextView) view.findViewById(R.id.txtPID);
        txtNameTH = (MyEditNoHLTextView) view.findViewById(R.id.txtNameTH);
        txtLastnameTH = (MyEditNoHLTextView) view.findViewById(R.id.txtLastnameTH);
        txtHomeID = (MyEditNoHLTextView) view.findViewById(R.id.txtHomeID);
        txtHomeNo = (MyEditNoHLTextView) view.findViewById(R.id.txtHomeNo);
        txtMoo = (MyEditNoHLTextView) view.findViewById(R.id.txtMoo);
        txtPost = (MyEditNoHLTextView) view.findViewById(R.id.txtPost);
        txtMobile = (MyEditNoHLTextView) view.findViewById(R.id.txtTel);
        txtPhone = (MyEditNoHLTextView) view.findViewById(R.id.txtPhone);
        txtLat = (MyEditNoHLTextView) view.findViewById(R.id.txtLat);
        txtLong = (MyEditNoHLTextView) view.findViewById(R.id.txtLong);
        txtEmail = (MyEditNoHLTextView) view.findViewById(R.id.txtEmail);

        spnAmphur = (Spinner) view.findViewById(R.id.spnAmphur);
        spnPrefix = (Spinner) view.findViewById(R.id.spnPrefix);
        spnTambol = (Spinner) view.findViewById(R.id.spnTambol);
        spnProvince = (Spinner) view.findViewById(R.id.spnProvince);
        spnVillage = (Spinner) view.findViewById(R.id.spnVillage);
        btnSubmit = (MyNoHBTextView) rootView.findViewById(R.id.btnSubmit);

    }

    private void setInfoFarmer() {

        mFarmer = DBHelper.getInfoFarmer();

        txtPID.setText("" + mFarmer.getPid());

        txtNameTH.setText(mFarmer.getFirst_Name());
        txtLastnameTH.setText(mFarmer.getLast_Name());
        txtHomeID.setText("" + mFarmer.getHome_ID());
        txtHomeNo.setText(mFarmer.getHomeNo());
        txtMoo.setText(mFarmer.getVillage());
        txtPost.setText(mFarmer.getPostCode());
        txtMobile.setText(mFarmer.getMobile());
        txtPhone.setText(mFarmer.getPhone());
        txtLat.setText(mFarmer.getLatitude());
        txtLong.setText(mFarmer.getLongitude());
        txtEmail.setText(mFarmer.getEmail());

    }

//    @OnClick(R.id.btnSubmit)
//    public void clickBtnSubmit() {
//        FarmerEntity old_d = AppUtils.getUserProfile(getContext());
//        FarmerEntity d = new FarmerModel.FarmerEntity();
//        d.setPid(old_d.getPid());
//
//        ProvinceEntity pEnt = provinceEntities.get(spnProvince.getSelectedItemPosition());
//        d.setProvince_ID(pEnt.getProvince_Code());
//
//
//        Gson gson = new GsonBuilder().create();
//        String json = gson.toJson(d);
//        // TODO: save to file, and start upload service...
//    }

    @OnClick(R.id.btnSubmit)
    public void clickBtnSubmit(){

        mFarmer.setFirst_Name(txtNameTH.getText().toString());
        mFarmer.setLast_Name(txtLastnameTH.getText().toString());
        mFarmer.setHome_ID(Integer.parseInt(txtHomeID.getText().toString()));
        mFarmer.setHomeNo(txtHomeNo.getText().toString());
        //TODO : Check village agian
        mFarmer.setVillage(txtMoo.getText().toString());
        mFarmer.setPostCode(txtPost.getText().toString());
        mFarmer.setMobile(txtMobile.getText().toString());
        mFarmer.setPhone(txtPhone.getText().toString());
        mFarmer.setLatitude(txtLat.getText().toString());
        mFarmer.setLongitude(txtLong.getText().toString());
        mFarmer.setEmail(txtEmail.getText().toString());

        DBHelper.updateFarmer(mFarmer, mFarmer.getFarmer_ID());

    }
    @Override
    public void onStart() {
        super.onStart();
    }

    @Override
    public void onStop() {
        super.onStop();
    }

    /*
     * Save Instance State Here
     */
    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        // Save Instance State here
        //outState.putParcelable(CLASS_NAME, Parcels.wrap(loginPIDEntity));

    }

    /*
     * Restore Instance State Here
     */
    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        if (savedInstanceState != null) {
            // Restore Instance State here
            //loginPIDEntity = Parcels.unwrap(savedInstanceState.getParcelable(CLASS_NAME));
        }
    }


    public void getListPrefix() {
        int selectIndex = -1;
        prefix = DBHelper.getPrefixList();

        for (int i = 0; i < prefix.size(); i++) {
            listPrefix.add(prefix.get(i).getPrefix_NameTh());
            if (prefix.get(i).getPrefix_ID() == mFarmer.getPrefix_ID()) {
                selectIndex = i;
            }
        }
        MySpinnerAdapter adapter = new MySpinnerAdapter(
                getContext(),
                android.R.layout.simple_dropdown_item_1line,
                listPrefix);
        spnPrefix.setAdapter(adapter);

        spnPrefix.setSelection(selectIndex);

        spnPrefix.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                mFarmer.setPrefix_ID(prefix.get(position).getPrefix_ID());

            }
            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });


    }

    public void getListProvince() {
        int selectIndex = -1;
        province = DBHelper.getProvinceList();

        for (int i = 0; i < province.size(); i++) {
            listProvince.add(province.get(i).getProvince_NameTh());
            if (province.get(i).getProvince_Code() == mFarmer.getProvince_ID()) {
                selectIndex = i;
            }
        }

        MySpinnerAdapter adapter = new MySpinnerAdapter(
                getContext(),
                android.R.layout.simple_dropdown_item_1line,
                listProvince);

        spnProvince.setAdapter(adapter);

        spnProvince.setSelection(selectIndex);


        spnProvince.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                getListAmphur(province.get(position).getProvince_ID());
                mFarmer.setProvince_ID(province.get(position).getProvince_Code());
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });


    }

    public void getListAmphur(int position) {
        int selectIndex = -1;

        listAmphur.clear();
        amphur = DBHelper.getAmphur(position);

        for (int i = 0; i < amphur.size(); i++) {
            listAmphur.add(amphur.get(i).getAmphur_nameTh());
            if (amphur.get(i).getAmphur_Code() == mFarmer.getAmphur_ID()) {
                selectIndex = i;
            }
        }

        MySpinnerAdapter adapter = new MySpinnerAdapter(
                getContext(),
                android.R.layout.simple_dropdown_item_1line,
                listAmphur);
        spnAmphur.setAdapter(adapter);

        spnAmphur.setSelection(selectIndex);

        spnAmphur.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                getListTambol(amphur.get(position).getAmphur_ID());
                mFarmer.setAmphur_ID(amphur.get(position).getAmphur_Code());
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    public void getListTambol(int position) {
        int selectIndex = -1;

        listTambol.clear();
        tambol = DBHelper.getTambol(position);

        for (int i = 0; i < tambol.size(); i++) {
            listTambol.add(tambol.get(i).getTambol_NameTh());
            if (tambol.get(i).getTambol_Code() == mFarmer.getTambol_ID()) {
                selectIndex = i;
            }
        }

        MySpinnerAdapter adapter = new MySpinnerAdapter(
                getContext(),
                android.R.layout.simple_dropdown_item_1line,
                listTambol);
        spnTambol.setAdapter(adapter);

        spnTambol.setSelection(selectIndex);

        spnTambol.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                getListVillage(tambol.get(position).getTambol_ID());

                mFarmer.setTambol_ID(tambol.get(position).getTambol_Code());
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    public void getListVillage(int position) {

        int selectIndex = -1;

        listVillage.clear();
        village = DBHelper.getVillage(position);

        for (int i = 0; i < village.size(); i++) {
            listVillage.add(village.get(i).getVillage_Name());
            if (village.get(i).getVilage_Code() == mFarmer.getVillage_ID()) {
                selectIndex = i;
            }
        }

        MySpinnerAdapter adapter = new MySpinnerAdapter(
                getContext(),
                android.R.layout.simple_dropdown_item_1line,
                listVillage);
        spnVillage.setAdapter(adapter);

        spnVillage.setSelection(selectIndex);

        spnVillage.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                    mFarmer.setVillage_ID(village.get(position).getVillage_ID());
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

}
