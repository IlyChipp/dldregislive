package com.android.fanqfang.dldregist.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Spinner;
import android.widget.Toast;

import com.android.fanqfang.dldregist.R;
import com.android.fanqfang.dldregist.activity.MainActivity;
import com.android.fanqfang.dldregist.config.Api;
import com.android.fanqfang.dldregist.database.DBHelper;
import com.android.fanqfang.dldregist.manager.Contextor;
import com.android.fanqfang.dldregist.model.AmphurMaster;
import com.android.fanqfang.dldregist.model.FarmerModel;
import com.android.fanqfang.dldregist.model.ProvinceMaster;
import com.android.fanqfang.dldregist.model.TambolMaster;
import com.android.fanqfang.dldregist.util.AppUtils;
import com.android.fanqfang.dldregist.widget.MyEditNoHLTextView;
import com.android.fanqfang.dldregist.widget.MyNoHLTextView;
import com.android.fanqfang.dldregist.widget.MySpinnerAdapter;

import java.util.ArrayList;

import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class FarmerGeneralFragment extends Fragment {

    DBHelper DBHelper ;
    private MyEditNoHLTextView txtPID;
    private MyNoHLTextView txtIncorrect;
    private Spinner spnProvince;
    private Spinner spnAmphur;
    private Spinner spnTambol;


    ArrayList<String> listProvince = new ArrayList<>();
    ArrayList<String> listTambol = new ArrayList<>();
    ArrayList<String> listAmphur = new ArrayList<>();

    ArrayList<ProvinceMaster.ProvinceEntity> province = new ArrayList<>();
    ArrayList<AmphurMaster.AmphurEntity> amphur = new ArrayList<>();
    ArrayList<TambolMaster.TambolEntity> tambol = new ArrayList<>();


    public FarmerGeneralFragment() {
        super();
    }

    public static FarmerGeneralFragment newInstance() {
        FarmerGeneralFragment fragment = new FarmerGeneralFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_farmer_general, container, false);

        ButterKnife.inject(this,rootView);
        initInstances(rootView);
        DBHelper = new DBHelper(getContext());
        txtPID.setText("3160200044480");
        getListProvince();

        return rootView;
    }

    private void initInstances(View rootView) {
        // Init 'View' instance(s) with rootView.findViewById here
        txtPID = (MyEditNoHLTextView) rootView.findViewById(R.id.txtPID);
        txtIncorrect = (MyNoHLTextView) rootView.findViewById(R.id.txtIncorrect);
        spnProvince = (Spinner) rootView.findViewById(R.id.spnProvince);
        spnAmphur = (Spinner) rootView.findViewById(R.id.spnAmphur);
        spnTambol = (Spinner) rootView.findViewById(R.id.spnTambol);

    }

    @Override
    public void onStart() {
        super.onStart();
    }

    @Override
    public void onStop() {
        super.onStop();
    }

    /*
     * Save Instance State Here
     */
    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        // Save Instance State here
    }

    /*
     * Restore Instance State Here
     */
    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        if (savedInstanceState != null) {
            // Restore Instance State here
        }
    }

    @OnClick(R.id.btnSubmit)
    public void gotoMain() {
        if (txtPID.getText().length() != 0) {
            loginPID(txtPID.getText().toString());

        } else {
            Toast.makeText(Contextor.getInstance().getContext(), getString(R.string.field_pid), Toast.LENGTH_LONG).show();
        }
    }

    private void loginPID(String idcard) {
        Api api = AppUtils.getApiService();
        Call<FarmerModel> call = api.loginPID(idcard);
        call.enqueue(new Callback<FarmerModel>() {
            @Override
            public void onResponse(Call<FarmerModel> call, Response<FarmerModel> response) {

                if (response.body() != null) {
                    if (response.body().getResponseMessage().equals(getString(R.string.success))) {

                        Log.d("TEST FIND PID 1", String.valueOf(response.body().getData().getPid()));

                        DBHelper.addFarmer(response.body().getData());



                        Intent intent = new Intent(getContext(), MainActivity.class);
                        startActivity(intent);
                    }else {
                        txtPID.setBackgroundResource(R.drawable.shape_edit_text_incorrect);
                        txtIncorrect.setVisibility(View.VISIBLE);
                    }
                } else {
                    Toast.makeText(Contextor.getInstance().getContext(), getString(R.string.alert_failconnect), Toast.LENGTH_SHORT).show();
                }

            }


            @Override
            public void onFailure(Call<FarmerModel> call, Throwable t) {
                Toast.makeText(Contextor.getInstance().getContext(), t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }


    public void getListProvince() {
        province = DBHelper.getProvinceList();

        for (int i = 0; i < province.size(); i++) {
            listProvince.add(province.get(i).getProvince_NameTh());
        }
        MySpinnerAdapter adapter = new MySpinnerAdapter(
                getContext(),
                android.R.layout.simple_dropdown_item_1line,
                listProvince);

        spnProvince.setAdapter(adapter);

        spnProvince.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                Log.d("Province Position .. : ", String.valueOf(position));
                getListAmphur(province.get(position).getProvince_ID());
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

    }

    public void getListAmphur(int position) {
        listAmphur.clear();
        amphur = DBHelper.getAmphur(position);

        for (int i = 0; i < amphur.size(); i++) {
            listAmphur.add(amphur.get(i).getAmphur_nameTh());
        }

        MySpinnerAdapter adapter = new MySpinnerAdapter(
                getContext(),
                android.R.layout.simple_dropdown_item_1line,
                listAmphur);
        spnAmphur.setAdapter(adapter);

        spnAmphur.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                getListTambol(amphur.get(position).getAmphur_ID());
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    public void getListTambol(int position) {
        listTambol.clear();
        tambol = DBHelper.getTambol(position);

        for (int i = 0; i < tambol.size(); i++) {
            listTambol.add(tambol.get(i).getTambol_NameTh());
        }

        MySpinnerAdapter adapter = new MySpinnerAdapter(
                getContext(),
                android.R.layout.simple_dropdown_item_1line,
                listTambol);
        spnTambol.setAdapter(adapter);


        spnTambol.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

}
