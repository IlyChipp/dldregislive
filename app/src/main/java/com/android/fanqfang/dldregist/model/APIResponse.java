package com.android.fanqfang.dldregist.model;

/**
 * Created by fanqfang on 10/8/2017 AD.
 */

public interface APIResponse {
    String getResponseMessage();
}
