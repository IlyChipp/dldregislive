package com.android.fanqfang.dldregist.model;

import android.provider.BaseColumns;

import org.parceler.Parcel;

import java.util.ArrayList;

/**
 * Created by fanqfang on 10/3/2017 AD.
 */
@Parcel
public class LivestockModel {

    //DBHelper
    public static final String TABLE = "livestock";

    String responseCode ;
    String responseMessage;
    ArrayList<Livestocks> data;

    public LivestockModel(){ }

    public String getResponseCode() {
        return responseCode;
    }

    public void setResponseCode(String responseCode) {
        this.responseCode = responseCode;
    }

    public String getResponseMessage() {
        return responseMessage;
    }

    public void setResponseMessage(String responseMessage) {
        this.responseMessage = responseMessage;
    }

    public ArrayList<Livestocks> getData() {
        return data;
    }

    public void setData(ArrayList<Livestocks> data) {
        this.data = data;
    }

    @Parcel
    public static class Livestocks{
        int livestock_ID;
        String createBy;
        String createDate;
        int farm_ID;
        int livestock_Amount;
        int livestock_Type_ID;
        int livestock_Values;
        String updateBy;
        String updateDate;

        public Livestocks() { }

        public int getLivestock_ID() {
            return livestock_ID;
        }

        public void setLivestock_ID(int livestock_ID) {
            this.livestock_ID = livestock_ID;
        }

        public String getCreateBy() {
            return createBy;
        }

        public void setCreateBy(String createBy) {
            this.createBy = createBy;
        }

        public String getCreateDate() {
            return createDate;
        }

        public void setCreateDate(String createDate) {
            this.createDate = createDate;
        }

        public int getFarm_ID() {
            return farm_ID;
        }

        public void setFarm_ID(int farm_ID) {
            this.farm_ID = farm_ID;
        }

        public int getLivestock_Amount() {
            return livestock_Amount;
        }

        public void setLivestock_Amount(int livestock_Amount) {
            this.livestock_Amount = livestock_Amount;
        }

        public int getLivestock_Type_ID() {
            return livestock_Type_ID;
        }

        public void setLivestock_Type_ID(int livestock_Type_ID) {
            this.livestock_Type_ID = livestock_Type_ID;
        }

        public int getLivestock_Values() {
            return livestock_Values;
        }

        public void setLivestock_Values(int livestock_Values) {
            this.livestock_Values = livestock_Values;
        }

        public String getUpdateBy() {
            return updateBy;
        }

        public void setUpdateBy(String updateBy) {
            this.updateBy = updateBy;
        }

        public String getUpdateDate() {
            return updateDate;
        }

        public void setUpdateDate(String updateDate) {
            this.updateDate = updateDate;
        }
    }

    public class Column{
        public static final String ID = BaseColumns._ID;
        public static final String LIVESTOCK_ID = "livestock_ID";
        public static final String CREATE_BY ="createBy";
        public static final String CREATE_DATE = "createDate";
        public static final String FARM_ID ="farm_ID";
        public static final String LIVESTOCK_AMOUNT = "livestock_Amount";
        public static final String LIVESTOCK_TYPE_ID ="livestock_Type_ID";
        public static final String LIVESTOCK_VALUES = "livestock_Values";
        public static final String UPDATE_BY = "updateBy";
        public static final String UPDATE_DATE = "updateDate";
    }
}
