package com.android.fanqfang.dldregist.fragment;

import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.media.MediaScannerConnection;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.Toast;

import com.android.fanqfang.dldregist.R;
import com.android.fanqfang.dldregist.activity.FarmActivity;
import com.android.fanqfang.dldregist.database.DBHelper;
import com.android.fanqfang.dldregist.model.AmphurMaster.AmphurEntity;
import com.android.fanqfang.dldregist.model.FarmModel.FarmEntity;
import com.android.fanqfang.dldregist.model.ProvinceMaster.ProvinceEntity;
import com.android.fanqfang.dldregist.model.StandardMaster.StandardEntity;
import com.android.fanqfang.dldregist.model.SupportStandardMaster.SupportEntity;
import com.android.fanqfang.dldregist.model.TambolMaster.TambolEntity;
import com.android.fanqfang.dldregist.model.VillageMaster.VillageEntity;
import com.android.fanqfang.dldregist.widget.MyEditNoHLTextView;
import com.android.fanqfang.dldregist.widget.MyNoHBTextView;
import com.android.fanqfang.dldregist.widget.MySpinnerAdapter;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;

import butterknife.ButterKnife;
import butterknife.OnClick;


public class FarmFragment extends Fragment {

    private static final int RESULT_CANCELED = 0;
    private static final String IMAGE_DIRECTORY = "/demonuts";
    private int GALLERY = 1, CAMERA = 2;

    ArrayList<Bitmap> bitmaps = new ArrayList<>();
    ArrayList<String> listVillage = new ArrayList<>();
    ArrayList<String> listTambol = new ArrayList<>();
    ArrayList<String> listAmphur = new ArrayList<>();
    ArrayList<String> listProvince = new ArrayList<>();
    ArrayList<String> listStandard = new ArrayList<>();
    ArrayList<String> listSupport = new ArrayList<>();

    ArrayList<VillageEntity> village = new ArrayList<>();
    ArrayList<TambolEntity> tambol = new ArrayList<>();
    ArrayList<AmphurEntity> amphur = new ArrayList<>();
    ArrayList<ProvinceEntity> province = new ArrayList<>();
    ArrayList<StandardEntity> standard = new ArrayList<>();
    ArrayList<SupportEntity> support = new ArrayList<>();

    ImageView imageView;
    int click = 0;

    private ImageView btnAddPhoto;
    private LinearLayout llAddphoto;
    private LinearLayout llFarmAddress;
    private MyNoHBTextView txtTopicAddress;
    private View view;
    private Spinner spnSupport;
    private Spinner spnStandard;
    private Spinner spnProvince;
    private Spinner spnAmphur;
    private Spinner spnTambol;
    private Spinner spnVillage;
    private MyEditNoHLTextView txtFarmName;
    private MyEditNoHLTextView txtPhone;
    private MyEditNoHLTextView txtSalary;
    private MyEditNoHLTextView txtProblem;
    private MyEditNoHLTextView txtMobile;
    private MyEditNoHLTextView txtNgan;
    private MyEditNoHLTextView txtRai;
    private MyEditNoHLTextView txtLongitude;
    private MyEditNoHLTextView txtLatitude;
    private MyEditNoHLTextView txtPostCode;
    private MyEditNoHLTextView txtStreet;
    private MyEditNoHLTextView txtSoi;
    private MyEditNoHLTextView txtHomeNo;
    private MyEditNoHLTextView txtVillage;
    private MyEditNoHLTextView txtEmail;
    FarmEntity farm;
    DBHelper dbHelper;
    Boolean isEmpty = false;


    public FarmFragment() {
        super();
    }


    public static FarmFragment newInstance() {
        FarmFragment fragment = new FarmFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_farm, container, false);
        View v = inflater.inflate(R.layout.item_farm_address, container, false);

        view = inflater.inflate(R.layout.item_pic, container, false);
        ButterKnife.inject(this, rootView);
        initInstances(rootView, v);
        farm = ((FarmActivity) getActivity()).getFarm();
        dbHelper = new DBHelper(getContext());
//        if (farm != null) {
//            setCategory();
//            setInfo(farm);
//            dbHelper.addFarm(farm);
//        } else {
//            farm = new FarmEntity();
//            isEmpty = true;
//        }
        llFarmAddress.addView(v);
        getListProvince();
        getStandard();
//        getSupport();


        return rootView;
    }

    private void initInstances(View rootView, View v) {
        // Init 'View' instance(s) with rootView.findViewById here

        llAddphoto = (LinearLayout) rootView.findViewById(R.id.llAddphoto);
        btnAddPhoto = (ImageView) rootView.findViewById(R.id.btnAddPhoto);
        llFarmAddress = (LinearLayout) rootView.findViewById(R.id.llFarmAddress);
        txtTopicAddress = (MyNoHBTextView) rootView.findViewById(R.id.txtTopicAddress);
        txtFarmName = (MyEditNoHLTextView) v.findViewById(R.id.txtFarmName);
        txtHomeNo = (MyEditNoHLTextView) v.findViewById(R.id.txtFarmNo);
        txtSoi = (MyEditNoHLTextView) v.findViewById(R.id.txtSoi);
        txtVillage = (MyEditNoHLTextView) v.findViewById(R.id.txtVillage);
        txtStreet = (MyEditNoHLTextView) v.findViewById(R.id.txtStreet);
        txtPostCode = (MyEditNoHLTextView) v.findViewById(R.id.txtPostCode);
        txtLatitude = (MyEditNoHLTextView) v.findViewById(R.id.txtLatitude);
        txtLongitude = (MyEditNoHLTextView) v.findViewById(R.id.txtLongitude);
        txtRai = (MyEditNoHLTextView) v.findViewById(R.id.txtRai);
        txtNgan = (MyEditNoHLTextView) v.findViewById(R.id.txtNgan);
        txtMobile = (MyEditNoHLTextView) v.findViewById(R.id.txtMobile);
        txtPhone = (MyEditNoHLTextView) v.findViewById(R.id.txtPhone);
        txtProblem = (MyEditNoHLTextView) rootView.findViewById(R.id.txtProblem);
        txtSalary = (MyEditNoHLTextView) rootView.findViewById(R.id.txtSalary);
        txtEmail = (MyEditNoHLTextView) v.findViewById(R.id.txtEmail);
        spnVillage = (Spinner) v.findViewById(R.id.spnVillage);
        spnTambol = (Spinner) v.findViewById(R.id.spnTambol);
        spnAmphur = (Spinner) v.findViewById(R.id.spnAmphur);
        spnProvince = (Spinner) v.findViewById(R.id.spnProvince);
        spnStandard = (Spinner) rootView.findViewById(R.id.spnStandard);
        spnSupport = (Spinner) rootView.findViewById(R.id.spnStandSup);

    }

    @OnClick(R.id.btnAddPhoto)
    public void addPhoto() {
        imageView = new ImageView(getContext());
        showPictureDialog();
    }

    public void setCategory() {

        txtFarmName.setText(farm.getFarm_Name());
        txtHomeNo.setText(farm.getHomeNo());
        txtVillage.setText(farm.getVillage());
        txtSoi.setText(farm.getSoi());
        txtPostCode.setText(farm.getPostCode());
        txtLatitude.setText(farm.getLatitude());
        txtLongitude.setText(farm.getLongitude());
        txtRai.setText(farm.getArea_Rai());
        txtNgan.setText(farm.getArea_Ngan());
        txtMobile.setText(farm.getMobile());
        txtPhone.setText(farm.getPhone());
        txtEmail.setText(farm.getEmail());

    }

    @OnClick(R.id.btnSubmit)
    public void clickBtnSubmit() {

        setInfo(farm);
        dbHelper.updateFarm(farm, farm.getFarm_ID());

    }

    public void setInfo(FarmEntity farm){
        farm.setFarm_Name(txtFarmName.getText().toString());
        farm.setHomeNo(txtHomeNo.getText().toString());
        farm.setVillage(txtVillage.getText().toString());
        farm.setSoi(txtSoi.getText().toString());
        farm.setPostCode(txtPostCode.getText().toString());
        farm.setLatitude(txtLatitude.getText().toString());
        farm.setLongitude(txtLongitude.getText().toString());
        farm.setArea_Rai(txtRai.getText().toString());
        farm.setArea_Ngan(txtNgan.getText().toString());
        farm.setMobile(txtMobile.getText().toString());
        farm.setPhone(txtPhone.getText().toString());

    }


    @Override
    public void onStart() {
        super.onStart();
    }

    @Override
    public void onStop() {
        super.onStop();
    }

    /*
     * Save Instance State Here
     */
    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        // Save Instance State here
    }

    /*
     * Restore Instance State Here
     */
    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        if (savedInstanceState != null) {
            // Restore Instance State here
        }
    }


    private void showPictureDialog() {
        AlertDialog.Builder pictureDialog = new AlertDialog.Builder(getContext());
        pictureDialog.setTitle("Select Action");
        String[] pictureDialogItems = {
                "Select photo from gallery",
                "Capture photo from camera"};
        pictureDialog.setItems(pictureDialogItems,
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        switch (which) {
                            case 0:
                                choosePhotoFromGallary();
                                break;
                            case 1:
                                takePhotoFromCamera();
                                break;
                        }
                    }
                });
        pictureDialog.show();
    }

    public void choosePhotoFromGallary() {
        Intent galleryIntent = new Intent(Intent.ACTION_PICK,
                android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);

        startActivityForResult(galleryIntent, GALLERY);
    }

    private void takePhotoFromCamera() {
        Intent intent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
        startActivityForResult(intent, CAMERA);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {

        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == this.RESULT_CANCELED) {
            return;
        }
        if (requestCode == GALLERY) {
            if (data != null) {
                Uri contentURI = data.getData();
                try {
                    Bitmap bitmap = MediaStore.Images.Media.getBitmap(getContext().getContentResolver(), contentURI);
                    bitmaps.add(bitmap);
//                    String path = saveImage(bitmap);
                    Toast.makeText(getContext(), "Image Saved!", Toast.LENGTH_SHORT).show();
                    imageView.setLayoutParams(new ViewGroup.LayoutParams((int) getResources().getDimension(R.dimen.picture_width)
                            , (int) getResources().getDimension(R.dimen.picture_width)));

                    imageView.setScaleType(ImageView.ScaleType.CENTER_CROP);
                    imageView.setImageBitmap(bitmaps.get(click));
                    llAddphoto.addView(imageView);
                    click++;

                } catch (IOException e) {
                    e.printStackTrace();
                    Toast.makeText(getContext(), "Failed!", Toast.LENGTH_SHORT).show();
                }
            }

        } else if (requestCode == CAMERA) {
            Bitmap thumbnail = (Bitmap) data.getExtras().get("data");
            bitmaps.add(thumbnail);
            Toast.makeText(getContext(), "Image Saved!", Toast.LENGTH_SHORT).show();

            imageView.setLayoutParams(new ViewGroup.LayoutParams((int) getResources().getDimension(R.dimen.picture_width)
                    , (int) getResources().getDimension(R.dimen.picture_width)));
            imageView.setScaleType(ImageView.ScaleType.CENTER_CROP);
            imageView.setImageBitmap(bitmaps.get(click));

            llAddphoto.addView(imageView);
            click++;
        }
    }

    public String saveImage(Bitmap myBitmap) {
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        myBitmap.compress(Bitmap.CompressFormat.JPEG, 90, bytes);
        File wallpaperDirectory = new File(
                Environment.getExternalStorageDirectory() + IMAGE_DIRECTORY);
        // have the object build the directory structure, if needed.
        if (!wallpaperDirectory.exists()) {
            wallpaperDirectory.mkdirs();
        }

        try {
            File f = new File(wallpaperDirectory, Calendar.getInstance()
                    .getTimeInMillis() + ".jpg");
            f.createNewFile();
            FileOutputStream fo = new FileOutputStream(f);
            fo.write(bytes.toByteArray());
            MediaScannerConnection.scanFile(getContext(),
                    new String[]{f.getPath()},
                    new String[]{"image/jpeg"}, null);
            fo.close();
            Log.d("TAG", "File Saved::--->" + f.getAbsolutePath());

            return f.getAbsolutePath();
        } catch (IOException e1) {
            e1.printStackTrace();
        }
        return "";
    }


    public void getListProvince() {
        int selectIndex = -1;
        province = dbHelper.getProvinceList();

        for (int i = 0; i < province.size(); i++) {
            listProvince.add(province.get(i).getProvince_NameTh());
            if (farm != null) {
                if (province.get(i).getProvince_Code() == farm.getProvince_ID()) {
                    selectIndex = i;
                }
            }
        }
        MySpinnerAdapter adapter = new MySpinnerAdapter(
                getContext(),
                android.R.layout.simple_dropdown_item_1line,
                listProvince);

        spnProvince.setAdapter(adapter);

        spnProvince.setSelection(selectIndex);

        spnProvince.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                getListAmphur(province.get(position).getProvince_ID());
                farm.setProvince_ID(province.get(position).getProvince_Code());
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

    }

    public void getListAmphur(int position) {
        int selectIndex = -1;

        listAmphur.clear();
        amphur = dbHelper.getAmphur(position);

        for (int i = 0; i < amphur.size(); i++) {
            listAmphur.add(amphur.get(i).getAmphur_nameTh());
            if (farm != null) {
                if (amphur.get(i).getAmphur_Code() == farm.getAmphur_ID()) {
                    selectIndex = i;
                }
            }
        }

        MySpinnerAdapter adapter = new MySpinnerAdapter(
                getContext(),
                android.R.layout.simple_dropdown_item_1line,
                listAmphur);
        spnAmphur.setAdapter(adapter);

        spnAmphur.setSelection(selectIndex);

        spnAmphur.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                getListTambol(amphur.get(position).getAmphur_ID());
                farm.setAmphur_ID(amphur.get(position).getAmphur_Code());
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    public void getListTambol(int position) {
        int selectIndex = -1;

        listTambol.clear();
        tambol = dbHelper.getTambol(position);

        for (int i = 0; i < tambol.size(); i++) {
            listTambol.add(tambol.get(i).getTambol_NameTh());
            if (farm != null) {
                if (tambol.get(i).getTambol_Code() == farm.getTambol_ID()) {
                    selectIndex = i;
                }
            }
        }

        MySpinnerAdapter adapter = new MySpinnerAdapter(
                getContext(),
                android.R.layout.simple_dropdown_item_1line,
                listTambol);
        spnTambol.setAdapter(adapter);

        spnTambol.setSelection(selectIndex);

        spnTambol.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                getListVillage(tambol.get(position).getTambol_ID());
                farm.setTambol_ID(tambol.get(position).getTambol_Code());
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    public void getListVillage(int position) {

        int selectIndex = -1;

        listVillage.clear();
        village = dbHelper.getVillage(position);

        for (int i = 0; i < village.size(); i++) {
            listVillage.add(village.get(i).getVillage_Name());
            if (farm != null) {
                if (village.get(i).getVilage_Code() == farm.getVillage_ID()) {
                    selectIndex = i;
                }
            }
        }

        MySpinnerAdapter adapter = new MySpinnerAdapter(
                getContext(),
                android.R.layout.simple_dropdown_item_1line,
                listVillage);
        spnVillage.setAdapter(adapter);

        spnVillage.setSelection(selectIndex);

        spnVillage.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                farm.setVillage(village.get(position).getVilage_Code() + "");
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    public void getStandard() {

        int selectIndex = -1;
        standard = dbHelper.getStandard();


        for (int i = 0; i < standard.size(); i++) {
            listStandard.add(standard.get(i).getStandard_Name());

            if (farm != null) {
                if (standard.get(i).getStandard_ID() == farm.getStandard_ID()) {
                    selectIndex = i;
                }
            }
        }

        MySpinnerAdapter adapter = new MySpinnerAdapter(
                getContext(),
                android.R.layout.simple_dropdown_item_1line,
                listStandard);
        spnStandard.setAdapter(adapter);

        spnStandard.setSelection(selectIndex);

        spnStandard.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    public void getSupport() {
        int selectIndex = -1;
        support = dbHelper.getSupport();

        for (int i = 0; i < support.size(); i++) {
            listSupport.add(support.get(i).getSupport_Standard_Name());
            Log.d("STANDARD LIST -->> ", String.valueOf(support.get(i).getSupport_Standard_Name()));
            if (support.get(i).getSupport_Standard_ID() == farm.getSupport_Standard_ID()) {
                selectIndex = i;
            }
        }

        MySpinnerAdapter adapter = new MySpinnerAdapter(
                getContext(),
                android.R.layout.simple_dropdown_item_1line,
                listSupport);
        spnSupport.setAdapter(adapter);

        spnSupport.setSelection(selectIndex);

        spnSupport.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }


}
