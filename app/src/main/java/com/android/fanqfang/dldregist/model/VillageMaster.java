package com.android.fanqfang.dldregist.model;

import android.provider.BaseColumns;

import org.parceler.Parcel;

import java.util.ArrayList;

/**
 * Created by fanqfang on 9/26/2017 AD.
 */
@Parcel
public class VillageMaster implements APIResponse{

    //DBHelper
    public static final String TABLE = "village";

    String responseCode;
    String responseMessage;
    ArrayList<VillageEntity> data;

    public VillageMaster(){ }

    public String getResponseCode() {
        return responseCode;
    }

    public void setResponseCode(String responseCode) {
        this.responseCode = responseCode;
    }

    public String getResponseMessage() {
        return responseMessage;
    }

    public void setResponseMessage(String responseMessage) {
        this.responseMessage = responseMessage;
    }

    public ArrayList<VillageEntity> getData() {
        return data;
    }

    public void setData(ArrayList<VillageEntity> data) {
        this.data = data;
    }

    @Parcel
    public static class VillageEntity {
        int village_ID;
        int moo;
        int orderIndex;
        int status_ID;
        int tambol_ID;
        int vilage_Code;
        String village_Name;

        public VillageEntity(){ }

        public int getVillage_ID() {
            return village_ID;
        }

        public void setVillage_ID(int village_ID) {
            this.village_ID = village_ID;
        }

        public int getMoo() {
            return moo;
        }

        public void setMoo(int moo) {
            this.moo = moo;
        }

        public int getOrderIndex() {
            return orderIndex;
        }

        public void setOrderIndex(int orderIndex) {
            this.orderIndex = orderIndex;
        }

        public int getStatus_ID() {
            return status_ID;
        }

        public void setStatus_ID(int status_ID) {
            this.status_ID = status_ID;
        }

        public int getTambol_ID() {
            return tambol_ID;
        }

        public void setTambol_ID(int tambol_ID) {
            this.tambol_ID = tambol_ID;
        }

        public int getVilage_Code() {
            return vilage_Code;
        }

        public void setVilage_Code(int vilage_Code) {
            this.vilage_Code = vilage_Code;
        }

        public String getVillage_Name() {
            return village_Name;
        }

        public void setVillage_Name(String village_Name) {
            this.village_Name = village_Name;
        }
    }
    public class Column{
        public static final String ID = BaseColumns._ID;
        public static final String VILLAGE_ID = "village_ID";
        public static final String MOO = "moo";
        public static final String ORDER_INDEX = "orderIndex";
        public static final String STATUS_ID = "status_ID";
        public static final String TAMBOL_ID = "tambol_ID";
        public static final String VILLAGE_CODE ="village_Code";
        public static final String VILLAGE_NAME = "village_name";
    }

}
